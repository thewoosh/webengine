# Conventions
This program makes use of a bunch of conventions, which can be confusing. This
file contains some of those and tries to describe them.

## std::optional<Unicode::UString\>
An optional wrapper for a string gives the string the possibility to be null or
missing. We can't just signal this with the string being empty, because in a lot
of parsers those are separate states.
