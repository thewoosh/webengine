/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Layout/Box.hpp"

#include <iostream>

#include "Source/Paint/Painter.hpp"

namespace Layout {

    bool Box::IsTextNode() { return false; }
    bool Box::IsBlockContainer() { return false; }

    void Box::Paint(Paint::PaintingStage stage, Paint::Painter *painter) {
        if (width == 0 || height == 0)
            return;

        switch (stage) {
            case Paint::PaintingStage::BACKGROUND:
                if (computedStyle.backgroundColor.alpha == 0)
                    return;
                painter->PaintRect({
                    {
                        position.x + border.left + margin.left,
                        position.y + border.top + margin.top
                    },
                    {
                        padding.left + width + padding.right,
                        padding.top + height + padding.bottom,
                    }
                }, computedStyle.backgroundColor);
                break;
            case Paint::PaintingStage::BORDER:
                PaintBorder(painter);
                break;
            default:
                break;
        }
    }

    void Box::PaintBorder(Paint::Painter *painter) const {
        // Currently, the corners of the borders are drawn as two rectangles.
        // We could optimize it in the case the border corners don't differ in
        // color, but I don't know if that's worth it.

        const auto verticalHeights = padding.top + height + padding.bottom;
        const auto horizontalWidths = padding.left + width + padding.right;

        // TOP
        painter->PaintRect({{position.x + margin.left + border.left, position.y + margin.top},
                            {horizontalWidths, border.top}},
                           computedStyle.borderTopColor);
        // BOTTOM
        painter->PaintRect({{position.x + margin.left + border.left, position.y + margin.top + border.top + verticalHeights},
                            {horizontalWidths, border.bottom}},
                           computedStyle.borderBottomColor);
        // LEFT
        painter->PaintRect({{position.x + margin.left, position.y + margin.top + border.top},
                            {border.left, verticalHeights}},
                           computedStyle.borderLeftColor);
        // RIGHT
        painter->PaintRect({{position.x + margin.left + border.left + horizontalWidths, position.y + margin.top + border.top},
                            {border.right, verticalHeights}},
                           computedStyle.borderRightColor);

        // CORNER LEFT TOP
        PaintBorderCorner(painter, {position, {border.left, border.top}},
                          computedStyle.borderLeftColor, computedStyle.borderTopColor);

        // CORNER RIGHT TOP
        PaintReversedBorderCorner(painter, {{position.x + border.left + horizontalWidths, position.y}, {border.right, border.top}},
                               computedStyle.borderRightColor, computedStyle.borderTopColor);

        // CORNER LEFT BOTTOM
        PaintReversedBorderCorner(painter, {{position.x, position.y + border.top + verticalHeights}, {border.left, border.bottom}},
                              computedStyle.borderBottomColor, computedStyle.borderLeftColor);

        // CORNER RIGHT BOTTOM
        PaintBorderCorner(painter, {{position.x + border.left + horizontalWidths, position.y + border.top + verticalHeights}, {border.right, border.bottom}},
                              computedStyle.borderBottomColor, computedStyle.borderRightColor);
    }

    void Box::PaintBorderCorner(Paint::Painter *painter, Paint::Rect rect, Paint::Color colorA, Paint::Color colorB) const {
        painter->GetRectPainter().PaintTriangle(rect.position,
                                                {rect.position.x + rect.size.width, rect.position.y + rect.size.height},
                                                {rect.position.x,                   rect.position.y + rect.size.height},
                                                colorA);
        painter->GetRectPainter().PaintTriangle(rect.position,
                                                {rect.position.x + rect.size.width, rect.position.y},
                                                {rect.position.x + rect.size.width, rect.position.y + rect.size.height},
                                                colorB);
    }

    void Box::PaintReversedBorderCorner(Paint::Painter *painter, Paint::Rect rect, Paint::Color colorA, Paint::Color colorB) const {
        painter->GetRectPainter().PaintTriangle({rect.position.x + rect.size.width, rect.position.y},
                                                {rect.position.x + rect.size.width, rect.position.y + rect.size.height},
                                                {rect.position.x, rect.position.y + rect.size.height},
                                                colorA);
        painter->GetRectPainter().PaintTriangle(rect.position,
                                                {rect.position.x + rect.size.width, rect.position.y},
                                                {rect.position.x, rect.position.y + rect.size.height},
                                                colorB);
    }

}
