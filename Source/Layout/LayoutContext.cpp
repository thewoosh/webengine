/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Layout/LayoutContext.hpp"

#include <cassert>

#include "Source/DOM/CharacterData.hpp"
#include "Source/DOM/Document.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/DOM/Helpers/Strings.hpp"
#include "Source/DOM/Node.hpp"
#include "Source/Layout/BlockBox.hpp"
#include "Source/Layout/TextBox.hpp"
#include "Source/Paint/Painter.hpp"
#include "Source/Text/FastUStrings.hpp"

[[nodiscard]] inline bool
IsStringOnlyWhitespace(const Unicode::UString &string) noexcept {
    for (Unicode::CodePoint codePoint : string) {
        if (codePoint != Unicode::SPACE &&
            codePoint != Unicode::CARRIAGE_RETURN &&
            codePoint != Unicode::LINE_FEED &&
            codePoint != Unicode::CHARACTER_TABULATION) {
            return false;
        }
    }

    return true;
}

namespace Layout {

    void LayoutContext::CreateFromDocument(Paint::Painter *painter,
                                           const DOM::Document *document) {
        assert(rootBox.children.empty());

        rootBox.computedStyle.display = CSS::Types::Display::BLOCK;
        TraverseDOM(&rootBox, painter, document, 0);
    }

    std::unique_ptr<Layout::Box>
    LayoutContext::CreateBoxForDocumentNode(Layout::Box *parent) {
        parent->lineY += 8; // DEBUG simulated margin-top of 8
        return std::make_unique<Layout::Box>();
    }

    std::unique_ptr<Layout::Box>
    LayoutContext::CreateBoxForElementNode(const DOM::Node *node) {
        const DOM::Element *element = static_cast<const DOM::Element *>(node);
        bool isBlockContainer = element->computedStyle.display == CSS::Types::Display::BLOCK ||
                                element->computedStyle.display == CSS::Types::Display::INLINE_BLOCK;

        // FIXME hack
        if (Unicode::FastUStrings::EqualsASCII(element->localName, "br"))
            isBlockContainer = true;

        if (isBlockContainer) {
            return std::make_unique<Layout::BlockBox>();
        }

        return std::make_unique<Layout::Box>();
    }

    std::unique_ptr<Layout::Box>
    LayoutContext::CreateBoxForTextNode(Layout::Box *parent, Paint::Painter *painter, const DOM::Node *node) {
        const auto *string = &static_cast<const DOM::CharacterData *>(node)->data;

        std::cout << "CreateBoxForTextNode>\n"
                     "\tText: \"" << *string << "\"\n"
                     "\tParent.margin-top: " << parent->margin.top << "\n"
                     "\tParent.padding-top: " << parent->padding.top << "\n"
                     "\tParent.lineY: " << parent->lineY << "\n";

        if (IsStringOnlyWhitespace(*string)) {
            if (std::empty(parent->lineStack)) {
                return nullptr;
            }

            string = &Unicode::SingleSpaceString;
        }

        auto box = std::make_unique<Layout::TextBox>(*string);

        auto dim = painter->Text().CalculateTextSize(*string);
        box->width = dim.width;
        box->height = dim.height;

        return box;
    }

    void LayoutContext::ApplyBoxProperties(Layout::Box *box, Layout::Box *parent, const DOM::Node *node) {
        box->parent = parent;

        box->computedStyle  = node->computedStyle;
        box->padding.left   = box->computedStyle.paddingLeft;
        box->padding.right  = box->computedStyle.paddingRight;
        box->padding.top    = box->computedStyle.paddingTop;
        box->padding.bottom = box->computedStyle.paddingBottom;

    }

    void LayoutContext::ProcessBlockBox(Layout::Box *parent) {
        // A block box in this function will break the current line, and start a
        // new one. This involves increasing the parent's lineY, so the line
        // starts under the previous one.

        float maxHeight = 0;
        // maxHeight should also depend on the line-height, and the font-size
        // when the line stack is empty.

        for (const auto *member : parent->lineStack) {
            const auto memberHeight =
                    member->margin.top +
                    member->border.top +
                    member->padding.top +
                    member->height +
                    member->padding.top +
                    member->border.top +
                    member->margin.top;

            if (memberHeight > maxHeight)
                maxHeight = memberHeight;
        }

        std::cout << "[DEBUG] Increase line with " << maxHeight << "\n";

        parent->lineY += maxHeight;

        parent->lineStack.clear();
    }

    void LayoutContext::ProcessInlineBox(Layout::Box *box, Layout::Box *parent) {
        if (std::empty(parent->lineStack)) {
            box->position.x = parent->position.x
                    + parent->margin.left
                    + parent->border.left
                    + parent->padding.left;
        } else {
            auto *previousBox = parent->lineStack.back();

            box->position.x = previousBox->position.x +
                              previousBox->margin.left +
                              previousBox->border.left +
                              previousBox->padding.left +
                              previousBox->width +
                              previousBox->padding.right +
                              previousBox->border.right +
                              previousBox->margin.right;
        }

        parent->lineStack.push_back(box);
    }

    void
    LayoutContext::CalculateBoxDimensions(Layout::Box *box) {
        float maxX = box->position.x + box->width;
        float maxY = box->position.y + box->height;

        for (const auto &child : box->children) {
//            box->width += CalculateBoxWidth(child.get());
//            box->height += CalculateBoxHeight(child.get());


            const auto childMaxX = child->position.x + CalculateBoxWidth(child.get());
            const auto childMaxY = child->position.y + CalculateBoxHeight(child.get());

            if (box->padding.bottom == 5) {
                std::cout << "childMaxX: " << childMaxX << " width=" << (childMaxX - child->position.x);
                std::cout << ' ' << child->padding.left << ' ' << child->padding.right;
                if (child->IsTextNode())
                    std::cout << ' ' << static_cast<const Layout::TextBox *>(child.get())->textContent;
                std::cout << '\n';
            }

            if (maxX < childMaxX)
                maxX = childMaxX;
            if (maxY < childMaxY)
                maxY = childMaxY;
        }

        std::cout << "CalculateBoxDimensions() p{" << box->width << ", " << box->height << "} calc={" << (maxX - box->position.x) << ", " << (maxY - box->position.y) << "}\n";

        box->width = maxX - box->position.x - box->margin.left - box->border.left - box->padding.left;
        box->height = maxY - box->position.y - box->margin.top - box->border.top - box->padding.top;
    }

    void LayoutContext::TraverseDOM(Layout::Box *parent, Paint::Painter *painter,
                                    const DOM::Node *node, std::size_t depth) {
        std::unique_ptr<Layout::Box> box;

        if (node->nodeType == DOM::NodeType::TEXT_NODE) {
            box = CreateBoxForTextNode(parent, painter, node);
        } else if (node->nodeType == DOM::NodeType::ELEMENT_NODE) {
            box = CreateBoxForElementNode(node);
        } else if (node->nodeType == DOM::NodeType::DOCUMENT_NODE) {
            box = CreateBoxForDocumentNode(parent);
        } else {
            std::cout << "[LayoutContext] Ignored node with type " << node->nodeType << '\n';
            return;
        }

        if (!box)
            return;

        ApplyBoxProperties(box.get(), parent, node);

        if (box->computedStyle.display == CSS::Types::Display::NONE) {
            return;
        }

        // TODO other displays than inline are inline-displays as well; add
        //      them here.
        if (box->IsBlockContainer()) {
            ProcessBlockBox(parent);
        } else if (box->computedStyle.display == CSS::Types::Display::INLINE) {
            ProcessInlineBox(box.get(), parent);
        } else {
            parent->lineStack.clear();
        }

        box->position.y = parent->lineY
                + parent->margin.top
                + parent->padding.top
                + parent->border.top;

        auto &ref = parent->children.emplace_back(std::move(box));

        ref->lineY = ref->position.y;
        for (auto &child : node->childNodes) {
            TraverseDOM(ref.get(), painter, child.get(), depth + 1);
        }

        RemoveTrailingSpaceTextBoxChildren(ref.get());

        CalculateBoxDimensions(ref.get());

        const auto &boxReport = [=, this](const auto &ref) {
            std::cout << "==BoxReport of " << ref
                      << "\nIsText=" << ref->IsTextNode()
                      << "\nIsBlockContainer=" << ref->IsBlockContainer()
                      << "\nPadding.right=" << ref->padding.right
                      << "\nMargin.right=" << ref->margin.right
                      << "\nBorder.right=" << ref->border.right
                      << "\nPosition.x=" << ref->position.x
                      << "\nWidth=" << ref->width
                      << "\nMax X=" << (ref->position.x + ref->width)
                      << "\nCalculated Width=" << CalculateBoxWidth(ref)
                      << "\nHeight=" << ref->height
                      << "\nCalculated Height=" << CalculateBoxHeight(ref)
                      << "\nMax Y=" << (ref->position.y + ref->height)
                      << "\nlineY=" << ref->lineY
                      << "\n";
        };


        if (ref->padding.bottom == 5) {
            boxReport(ref.get());
            if (!ref->lineStack.empty()) {
                std::cout << "LastLineElement Report coming up";
                boxReport(ref->lineStack.back());
            }
        }
    }
//    << "\nLastLineElement Max X=" << (ref->lineStack.empty() ? 0 :
//    ref->lineStack.back()->position.y +
//    CalculateBoxWidth(ref->lineStack.back()))

    float LayoutContext::CalculateBoxHeight(Layout::Box *box) {
        return box->margin.top +
               box->border.top +
               box->padding.top +
               box->height +
               box->padding.top +
               box->border.top +
               box->margin.top;
    }

    float LayoutContext::CalculateBoxWidth(Layout::Box *box) {
        return box->margin.left +
               box->border.left +
               box->padding.left +
               box->width +
               box->padding.right +
               box->border.right +
               box->margin.right;
    }

    void LayoutContext::RemoveTrailingSpaceTextBoxChildren(Layout::Box *box) {
        // Remove all trailing TextBoxes that are just whitespace.
        // FYI The leading one(s) is/are omitted before box generation.

        while (!std::empty(box->children)) {
            if (!box->children.back()->IsTextNode())
                return;

            const auto *textBox = static_cast<const TextBox *>(box->children.back().get());

            if (!IsStringOnlyWhitespace(textBox->textContent))
                return;

            box->children.pop_back();

            if (box->lineStack.back() == textBox)
                box->lineStack.pop_back();
        }
    }

} // namespace Layout
