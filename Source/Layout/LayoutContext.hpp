/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Layout/Box.hpp"

namespace DOM { struct Document; struct Node; }
namespace Paint { class Painter; }

namespace Layout {

    class LayoutContext {
        Layout::Box rootBox;

        void TraverseDOM(Layout::Box *parent, Paint::Painter *, const DOM::Node *, std::size_t depth);

        [[nodiscard]] std::unique_ptr<Layout::Box>
        CreateBoxForDocumentNode(Layout::Box *parent);

        [[nodiscard]] std::unique_ptr<Layout::Box>
        CreateBoxForElementNode(const DOM::Node *node);

        [[nodiscard]] std::unique_ptr<Layout::Box>
        CreateBoxForTextNode(Layout::Box *parent, Paint::Painter *painter, const DOM::Node *node);

        void ApplyBoxProperties(Layout::Box *box, Layout::Box *parent, const DOM::Node *node);
        void ProcessBlockBox(Layout::Box *parent);
        void ProcessInlineBox(Layout::Box *box, Layout::Box *parent);
        void CalculateBoxDimensions(Layout::Box *);
        void RemoveTrailingSpaceTextBoxChildren(Layout::Box *);

        [[nodiscard]] float CalculateBoxWidth(Layout::Box *);
        [[nodiscard]] float CalculateBoxHeight(Layout::Box *);
    public:
        void CreateFromDocument(Paint::Painter *, const DOM::Document *);

        [[nodiscard]] inline constexpr Layout::Box &
        Root() noexcept { return rootBox; }

        [[nodiscard]] inline constexpr const Layout::Box &
        Root() const noexcept { return rootBox; }
    };


} // namespace Layout
