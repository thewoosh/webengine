/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Layout/TextBox.hpp"

#include <iostream>

#include "Source/Paint/Painter.hpp"

namespace Layout {

    void TextBox::Paint(Paint::PaintingStage stage, Paint::Painter *painter) {
        Box::Paint(stage, painter);

        if (stage != Paint::PaintingStage::CONTENT)
            return;

        painter->Text().Paint({
            position.x + margin.left + border.left + padding.left,
            position.y + margin.top + border.top + padding.top
        }, textContent, computedStyle.textColor);
    }

}
