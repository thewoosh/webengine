/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <vector>

#include "Source/CSS/ComputedStyle.hpp"
#include "Source/Graphics/Vector2.hpp"
#include "Source/Paint/Rect.hpp"
#include "Source/Paint/Stage.hpp"

namespace Paint { class Painter; }

namespace Layout {

    struct Edge {
        float left{};
        float right{};
        float top{};
        float bottom{};
    };

    struct Box {

        Box *parent{};
        std::vector<std::unique_ptr<Box>> children{};

        CSS::ComputedStyle computedStyle{};

        Edge margin{};
        Edge border{};
        Edge padding{};

        Graphics::Vector2f position{};

        float width{};
        float height{};

        virtual ~Box() = default;

        [[nodiscard]] virtual bool IsTextNode();
        [[nodiscard]] virtual bool IsBlockContainer();

        virtual void
        Paint(Paint::PaintingStage, Paint::Painter *);

        //
        // Used in layout computation
        //

        /**
         * The line stack holds the display: inline boxes in the current line.
         */
        std::vector<Layout::Box *> lineStack{};

        float lineY{};

    private:
        void PaintBorder(Paint::Painter *) const;
        void PaintBorderCorner(Paint::Painter *, Paint::Rect, Paint::Color, Paint::Color) const;
        void PaintReversedBorderCorner(Paint::Painter *, Paint::Rect, Paint::Color, Paint::Color) const;
    };

} // namespace Layout
