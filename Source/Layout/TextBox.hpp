/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Layout/Box.hpp"
#include "Source/Text/UString.hpp"

namespace Layout {

    struct TextBox : public Box {

        const Unicode::UString &textContent;

        [[nodiscard]] inline explicit
        TextBox(const Unicode::UString &textContent) noexcept
                : textContent(textContent) {
        }

        TextBox(const TextBox &) = default;
        TextBox(TextBox &&) = default;

        [[nodiscard]] inline bool
        IsTextNode() override {
            return true;
        }

        void Paint(Paint::PaintingStage, Paint::Painter *) override;

    };

} // namespace Layout
