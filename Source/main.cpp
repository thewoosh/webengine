/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 - 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * This file contains the basic Unicode symbols for use throughout the software.
 */

#include <array>
#include <chrono>
#include <iostream>
#include <cmath>

#include "Source/CSS/Context.hpp"
#include "Source/Data/IteratorStream.hpp"
#include "Source/Graphics/Window.hpp"
#include "Source/HTML/Tokenizer.hpp"
#include "Source/Layout/LayoutContext.hpp"
#include "Source/Layout/TextBox.hpp"
#include "Source/Paint/Painter.hpp"
#include "Source/Text/Encoding/Type/UTF8.hpp"
#include "Source/Text/FastUStrings.hpp"
#include "Source/Web/URLParser.hpp"

#include "Testing/PrintDOMNode.hpp"

// TODO put in Data/StreamHelpers.hpp or something like that
template<typename T>
constexpr auto &
operator<<(auto &stream, const std::optional<T> &opt) {
    if (!opt) {
        stream << "optional(empty)";
    } else {
        stream << "optional(non-empty: " << *opt << ")";
    }

    return stream;
}

template<typename Container>
constexpr auto &
operator<<(auto &stream, const Container &container) {
    stream << "container[" << std::distance(std::cbegin(container), std::cend(container)) << "]{";

    bool first = true;
    for (const auto &element : container) {
        if (!first) {
            stream << ", ";
        }

        stream << ' ' << element;
        first = false;
    }

    if (first) {
        stream << "}";
    } else {
        stream << " }";
    }

    return stream;
}

namespace Debug {
    struct TimePerfEntry {
        using Clock = std::chrono::high_resolution_clock;
        using TimePoint = std::chrono::time_point<Clock>;

        TimePoint begin{};
        TimePoint end{};

        inline void start() {
            begin = Clock::now();
        }

        inline void stop() {
            end = Clock::now();
        }
    };

    template<typename Unit, typename DurationType>
    inline void
    append(std::ostream &stream, DurationType &inDuration, bool &flag, const char *literal) {
        const auto duration = std::chrono::duration_cast<Unit>(inDuration);
        if (duration.count() > 0) {
            if (flag) stream << ", ";
            stream << duration.count() << ' ' << literal;
            flag = true;

            inDuration -= duration;
        }
    }

    std::ostream &
    operator<<(std::ostream &stream, TimePerfEntry entry) {
        auto duration = entry.end - entry.begin;
        bool flag = false;

        append<std::chrono::seconds>(stream, duration, flag, "s");
        append<std::chrono::milliseconds>(stream, duration, flag, "ms");
        append<std::chrono::microseconds>(stream, duration, flag, "μs");
        append<std::chrono::nanoseconds>(stream, duration, flag, "ns");

        return stream;
    }

    struct TimePerfList {
        TimePerfEntry utf8Decode{};
        TimePerfEntry htmlParser{};
    };
}

void PrintLayoutBox(Layout::Box *box, std::size_t depth) {
    std::cout << std::string(depth * 2, ' ');

    if (box->IsTextNode())
        std::cout << "TextBox => " << static_cast<const Layout::TextBox *>(box)->textContent;
    else
        std::cout << "Box";

    if (box->computedStyle.display == CSS::Types::Display::BLOCK)
        std::cout << " => block";
    else if (box->computedStyle.display == CSS::Types::Display::INLINE)
        std::cout << " => inline";
    else
        std::cout << " => display?";


    std::cout << '\n';

    for (auto &child : box->children) {
        PrintLayoutBox(child.get(), depth + 1);
    }
}

void PaintBox(Layout::Box &box, Paint::PaintingStage stage, Paint::Painter *painter) {
    box.Paint(stage, painter);

    for (auto &child : box.children) {
        PaintBox(*child, stage, painter);
    }
}

void PaintLayout(Layout::LayoutContext &context, Paint::Painter *painter) {
    painter->Begin();
    PaintBox(context.Root(), Paint::PaintingStage::BACKGROUND, painter);
    PaintBox(context.Root(), Paint::PaintingStage::BORDER, painter);
    PaintBox(context.Root(), Paint::PaintingStage::CONTENT, painter);
    painter->End();
}

int main() {
    Graphics::Window window{};
    Paint::Painter painter{};
    painter.SetViewPort(window.Size());

    using CharType = std::uint8_t;

    Debug::TimePerfList timePerfList{};

    timePerfList.utf8Decode.start();

    Data::StringStream rawTextStream(R"(
<!doctype html>
<html>
    <head>
        <title>Test Page</title>
        <style>
            * {
                color: lime;
            }
        </style>
    </head>
    <body>
        <span hidden>Some </span> <span style="color: red">red</span> <span style="color: blue">text</span>
        <br>
        <span>abcdefg ÅḀ</span>
        <br>
        <span style="background-color: yellow">yellow background</span>
        <div style="padding: 5px; background-color: aqua">
            <span style="background-color: fuchsia">Fuchsia</span>
            <span style="background-color: green; color: white">Green</span>
            <span style="background-color: blue; color: red">Blue</span>
        </div>
    </body>
</html>
)");

    Encoding::UTF8<CharType> utf{};
    auto vec = utf.DecodeStream(&rawTextStream);
    std::shared_ptr<Data::Stream<Unicode::CodePoint>> tokenizerInputStream = std::make_shared<Data::IteratorStream<Unicode::CodePoint, decltype(vec)::const_iterator>>(
        std::cbegin(vec), std::cend(vec)
    );

    timePerfList.utf8Decode.stop();
    timePerfList.htmlParser.start();

    HTML::Tokenizer tokenizer(tokenizerInputStream, HTML::HTMLLogger(std::cout));
    tokenizer.Run();

    timePerfList.htmlParser.stop();

    std::cout << "\n\n==================\nTokenizer finished\n==================\n\n";

    std::cout << "Token list size: " << tokenizer.Context().tokenList.size() << '\n';
    for (std::size_t i = 0; i < tokenizer.Context().tokenList.size(); i++) {
        const auto &token = tokenizer.Context().tokenList[i];
        std::cout << '(' << (i+1) << ") " << token->type;

        auto separator = "\n" + std::string((4 + std::log(i+1) / std::log(10)), ' ');
        if (token->type == HTML::TokenType::DOCTYPE) {
            const auto *instance = reinterpret_cast<HTML::DoctypeToken *>(token.get());
            std::cout << separator << "Name: " << instance->name;
            std::cout << separator << "Public Identifier: " << instance->publicIdentifier;
            std::cout << separator << "System Identifier: " << instance->systemIdentifier;
        } else if (token->type == HTML::TokenType::CHARACTER) {
            HTML::CharacterToken *characterToken = static_cast<HTML::CharacterToken *>(token.get());
            std::cout << separator << "Data: " << characterToken->data << separator << "ASCII Representation: ";
            if (characterToken->data >= 0x20 && characterToken->data <= 0x7E) {
                std::cout << '\'' << static_cast<char>(characterToken->data) << '\'';
            } else {
                switch (characterToken->data) {
                    case Unicode::NULL_CHARACTER:
                        std::cout << "NUL";
                        break;
                    case Unicode::LINE_FEED:
                        std::cout << "LINE FEED";
                        break;
                    case Unicode::FORM_FEED:
                        std::cout << "FORM FEED";
                        break;
                    case Unicode::CARRIAGE_RETURN:
                        std::cout << "CARRIAGE RETURN";
                        break;
                    case Unicode::CHARACTER_TABULATION:
                        std::cout << "CHARACTER TABULATION";
                        break;
                    case Unicode::REPLACEMENT_CHARACTER:
                        std::cout << "REPLACEMENT CHARACTER";
                        break;
                    default:
                        std::cout << "not in ASCII visible character scope.";
                        break;
                }
            }

        } else if (token->type == HTML::TokenType::END_TAG || token->type == HTML::TokenType::START_TAG) {
            const auto *instance = reinterpret_cast<HTML::TagToken *>(token.get());
            std::cout << separator << "Name: " << instance->name;
            std::cout << separator << "Self Closing: " << instance->selfClosing;
        } else if (token->type == HTML::TokenType::COMMENT) {
            std::cout << separator << "Data: " << reinterpret_cast<HTML::CommentToken *>(token.get())->data;
        }

        std::cout << '\n';
    }

    if (tokenizer.Context().status != HTML::TokenizerStatus::SUCCESS) {
        std::cout << "\nTokenizer failed with status: " << tokenizer.Context().status << "\n";
        return EXIT_FAILURE;
    }

    // Printing the DOM
    const auto &parserContext = tokenizer.Context().treeConstructor.Context();
    std::cout << "\n\nTree constructor output\n\n";


    std::cout << "=======================\nDocument Type\n=======================\n";
    if (parserContext.document->doctype) {
        std::cout << "Name: " << parserContext.document->doctype->name << "\n";
        std::cout << "PublicId: " << parserContext.document->doctype->publicId << "\n";
        std::cout << "SystemId: " << parserContext.document->doctype->systemId << "\n";
    } else {
        std::cout << "No information about the document type is available,\n"
                     "since the Document.doctype is null.\n";
    }

    std::cout << "\n\n=======================\n          Tree\n=======================\n";
    Testing::PrintDOMNode(parserContext.document, 0);

    std::cout << "\n===========\nPerformance\n===========\n";
    std::cout << "\nUTF-8 Decode: " << timePerfList.utf8Decode;
    std::cout << "\nHTML Parsing: " << timePerfList.htmlParser;

    std::cout << "\n\n\n";

    DOM::Document *document = parserContext.document.get();

    CSS::Context context{};
    if (!context.applyStyle(document)) {
        std::cout << "Failed to apply CSS!\n";
        return 1;
    }

    Layout::LayoutContext layoutContext{};
    layoutContext.CreateFromDocument(&painter, document);

    PrintLayoutBox(&layoutContext.Root(), 0);

    while (!window.IsCloseRequested()) {
        PaintLayout(layoutContext, &painter);

        window.SwapBuffers();
    }

    return EXIT_SUCCESS;
}
