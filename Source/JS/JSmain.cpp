/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

// C++ std libraries
#include <iostream>
#include <string>

// C std libraries
#include <cstdlib>
#include <cstring>
#include <malloc.h>

// POSIX libraries
#include <unistd.h>

// System libraries
#include <sys/mman.h>

// Project libraries
#include "Architecture/X86_64/Register.hpp"
using namespace Architecture::X86_64;

static long pageSize = 0;

[[noreturn]] void
TerminateWithError(const std::string &);

template <typename Type>
struct DataWrapper {

    Type *const ptr;
    const std::size_t size;

    DataWrapper(Type *ptr, std::size_t size) noexcept : ptr(ptr), size(size) {}
    DataWrapper(const DataWrapper &) = delete;
    DataWrapper(DataWrapper &&instance)
            : ptr(std::move(instance.ptr)), size(instance.size) {
        const_cast<Type *&>(instance.ptr) = nullptr;
    }

    ~DataWrapper() noexcept {
        if (ptr) {
            if (mprotect(ptr, size, PROT_WRITE) == -1)
                TerminateWithError("Failed to set ptr to PROT_WRITE for dtor");
            std::free(ptr);
        }
    }

    [[nodiscard]] inline const Type &
    operator[](std::size_t index) const {
        if (index >= size) {
            throw std::out_of_range("index=" + std::to_string(index) + " >= size=" + std::to_string(size));
        }

        return ptr[index];
    }

    [[nodiscard]] inline Type &
    operator[](std::size_t index) {
        if (index >= size) {
            throw std::out_of_range("index=" + std::to_string(index) + " >= size=" + std::to_string(size));
        }

        return ptr[index];
    }

    void
    zero() noexcept {
        std::memset(ptr, 0, size);
    }

};

class InstructionsBuilder {
    std::size_t pos{};

public:
    using UnitType = std::uint8_t;
    DataWrapper<UnitType> data;

    explicit
    InstructionsBuilder(DataWrapper<UnitType> &&data)
        : data(std::move(data)) {
        this->data.zero();
    }

    InstructionsBuilder(const InstructionsBuilder &) = delete;

    InstructionsBuilder &
    AddRet() {
        data[pos++] = 0xC3;
        return *this;
    }

    template <Register reg, typename IntType>
    InstructionsBuilder &
    AddMov(IntType value) {
        if constexpr (IsRegister32(reg)) {
            static_assert(sizeof(IntType) == 4);
            data[pos++] = 0xb8 + static_cast<UnitType>(reg);
            memcpy(&data.ptr[pos], &value, 4);
            pos += 4;
        } else if constexpr (IsRegister64(reg)) {
            static_assert(sizeof(IntType) == 8);
            data[pos++] = 0x48;
            data[pos++] = 0xc7;
            data[pos++] = 0xc0 + static_cast<UnitType>(reg) - 8;
            memcpy(&data.ptr[pos], &value, 4);
            pos += 4;
        }

        return *this;
    }

    void
    Finalize() {
        if (mprotect(data.ptr, data.size, PROT_EXEC) == -1)
            TerminateWithError("Failed to set ptr to PROT_EXEC");
    }

    void
    Execute() {
        void *ptr = (void *)data.ptr;

        std::cout << "Result is: " << ((int (*)())ptr)() << '\n';
    }

};

template<typename PtrType>
[[nodiscard]] inline DataWrapper<PtrType>
AllocatePages(std::size_t pageCount) {
    void *data = memalign(pageSize, pageSize * pageCount);

    if (!data) {
        TerminateWithError("Failed to allocate pages");
    }

    printf("memalign~%p\n", data);

    return {static_cast<PtrType *>(data), pageSize * pageCount};
}

int main() {
    pageSize = sysconf(_SC_PAGE_SIZE);
    if (pageSize == -1) {
        TerminateWithError("Failed to get page size from sysconf");
    }

    InstructionsBuilder builder(AllocatePages<InstructionsBuilder::UnitType>(4));

    builder.AddMov<Register::EAX>(4);
    builder.AddRet();

    builder.Finalize();
    builder.Execute();

    return EXIT_SUCCESS;
}

[[noreturn]] void
TerminateWithError(const std::string &message) {
    perror("Errno is set to");
    std::cerr << "Terminate called on error: " << message << '\n';
    std::exit(EXIT_FAILURE);
}
