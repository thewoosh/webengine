/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

namespace Architecture::X86_64 {

    enum class Register {
        EAX,
        ECX,
        EDX,
        EBX,
        ESP,
        EBP,
        ESI,
        EDI,


        RAX,
        RCX,
        RDX,
        RBX,
        RSP,
        RBP,
        RSI,
        RDI,
    };

    [[nodiscard]] constexpr inline bool
    IsRegister32(Register reg) {
        auto a = static_cast<std::size_t>(reg);
        return a >= static_cast<std::size_t>(Register::EAX) &&
               a <= static_cast<std::size_t>(Register::EDI);
    }

    [[nodiscard]] constexpr inline bool
    IsRegister64(Register reg) {
        auto a = static_cast<std::size_t>(reg);
        return a >= static_cast<std::size_t>(Register::RAX) &&
               a <= static_cast<std::size_t>(Register::RDI);
    }

    [[nodiscard]] constexpr inline Register
    UpgradeRegister32to64(Register reg) {
        if (!IsRegister32(reg))
            return reg;
        return static_cast<Register>(static_cast<std::size_t>(reg) + 8);
    }

} // namespace Architecture::X86_64
