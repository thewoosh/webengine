/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * This file contains tools that can be used in conjunction with the
 * Unicode::CodePoint environment.
 */

#pragma once

#include <algorithm>
#include <array>
#include <compare>
#include <iterator>
#include <string_view>
#include <vector>

#include "Unicode.hpp"

#include "Source/Data/StringStream.hpp"
#include "Source/Text/Encoding/Type/UTF8.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace Unicode {

    /**
     * This class is a string that holds code points, instead of std::string,
     * which holds char. The UString class simplifies working with Unicode.
     */
    class UString {
    public:
        /**
         * Calling this constructor (i.e. without arguments) will represent an
         * empty string.
         */
        UString() = default;

        /**
         * This constructor takes in a std::string_view and will decode it from
         * UTF-8.
         */
        explicit UString(const std::string_view &sv) {
            auto stream = Data::StringStream<std::uint8_t>(sv);
            data = Encoding::UTF8<std::uint8_t>().DecodeStream(&stream);
        }

        template<std::size_t ArraySize>
        explicit UString(const std::array<char, ArraySize> &array) {
            data.reserve(ArraySize);
            for (char c : array) {
                data.push_back(c);
            }
        }

        template<std::size_t ArraySize>
        explicit UString(const std::array<Unicode::CodePoint, ArraySize> &array)
            : data(array) {
        }

        /**
         * This constructor takes in a single code point.
         */
        explicit UString(Unicode::CodePoint codePoint)
                : data({codePoint}) {
        }

        /**
         * This constructor takes in a vector which will be copied.
         */
        explicit UString(const std::vector<Unicode::CodePoint> &data)
                : data(data) {
        }

        /**
         * This constructor takes in a vector which won't be copied but moved.
         */
        explicit UString(std::vector<Unicode::CodePoint> &&data)
                : data(std::move(data)) {
        }

        /**
         * This constructor takes in two iterators which will be copied into the
         * data vector.
         */
        template<typename IteratorType>
        explicit UString(const IteratorType &begin,
                         const IteratorType &end)
                : data(begin, end) {
        }

        [[nodiscard]] inline UString
        clone() const {
            return UString(data);
        }

        [[nodiscard]] inline Unicode::CodePoint
        operator[](std::size_t index) const {
            return data.at(index);
        }

        /**
         * Returns the length of this string.
         *
         * Unicode::UString({"Test"})::length() => 4
         */
        [[nodiscard]] inline std::size_t
        length() const {
            return data.size();
        }

        /**
         * Checks whether or not the string is equal to the given code point.
         */
        [[nodiscard]] inline bool
        operator==(Unicode::CodePoint codePoint) const {
            return length() == 1 && operator[](0) == codePoint;
        }

        /**
         * Checks whether or not the string is equal to the string.
         */
        [[nodiscard]] inline bool
        operator==(const UString &otherString) const {
            return data == otherString.data;
        }

        /**
         * Checks whether or not the string is equal to the given vector.
         */
        [[nodiscard]] inline bool
        operator==(const std::vector<Unicode::CodePoint> &inData) const {
            return this->data == inData;
        }

        [[nodiscard]] inline bool
        operator<(const std::vector<Unicode::CodePoint> &inData) const noexcept {
            return this->data < inData;
        }

        [[nodiscard]] inline bool
        operator<(const UString &uString) const noexcept {
            return operator<(uString.data);
        }

//        auto operator<=>(const UString &) const = default;

        /**
         * Checks whether or not the string starts with the given vector.
         */
        [[nodiscard]] inline bool
        startsWith(const std::vector<Unicode::CodePoint> &prefix) const {
            return prefix.size() <= data.size() &&
                   std::equal(std::cbegin(prefix), std::cend(prefix), std::cbegin(data));
        }

        /**
         * Checks whether or not the string starts with the given string.
         */
        [[nodiscard]] inline bool
        startsWith(const UString &otherString) const {
            return startsWith(otherString.data);
        }

        /**
         * Checks whether or not the string starts with the given code point.
         */
        [[nodiscard]] inline bool
        startsWith(Unicode::CodePoint codePoint) const {
            return !data.empty() && data[0] == codePoint;
        }

        /**
         * Checks whether or not the string ends with the given vector.
         */
        [[nodiscard]] inline bool
        endsWith(const std::vector<Unicode::CodePoint> &prefix) const {
            return prefix.size() <= data.size() &&
                   std::equal(std::crbegin(prefix), std::crend(prefix), std::crbegin(data));
        }

        /**
         * Checks whether or not the string ends with the given string.
         */
        [[nodiscard]] inline bool
        endsWith(const UString &otherString) const {
            return endsWith(otherString.data);
        }

        /**
         * Checks whether or not the string ends with the given code point.
         */
        [[nodiscard]] inline bool
        endsWith(Unicode::CodePoint codePoint) const {
            return !data.empty() && data.back() == codePoint;
        }

        /**
         * Checks whether or not the string is equal to the other string, with
         * comparing ASCII letters case-insensitively.
         */
        [[nodiscard]] inline bool
        equalsIgnoreCase(const Unicode::UString &other) const {
            if (other.length() != length()) {
                return false;
            }

            for (std::size_t i = 0; i < length(); i++) {
                if (Unicode::ToLowerASCII(data[i]) != other.data[i]) {
                    return false;
                }
            }

            return true;
        }

        //
        // STRING MANIPULATION
        //

        /**
         * Append the other string to this string.
         */
        inline UString &
        operator+=(const Unicode::UString &s) {
            data.reserve(data.size() + s.data.size());
            std::copy(std::cbegin(s.data), std::cend(s.data), std::back_inserter(data));
            return *this;
        }

        /**
         * Append a character to this string.
         */
        inline UString &
        operator+=(Unicode::CodePoint character) {
            data.push_back(character);
            return *this;
        }

        /**
         * Append a character to this string.
         */
        inline UString &
        operator+=(char character) {
            data.push_back(character);
            return *this;
        }

        //
        // ITERATOR SUPPORT
        //

        [[nodiscard]] inline auto
        begin() const {
            return data.begin();
        }

        [[nodiscard]] inline auto
        end() const {
            return data.end();
        }

        [[nodiscard]] inline auto
        cbegin() const {
            return data.cbegin();
        }

        [[nodiscard]] inline auto
        cend() const {
            return data.cend();
        }

        [[nodiscard]] inline auto
        rbegin() const {
            return data.rbegin();
        }

        [[nodiscard]] inline auto
        rend() const {
            return data.rend();
        }

        [[nodiscard]] inline auto
        crbegin() const {
            return data.crbegin();
        }

        [[nodiscard]] inline auto
        crend() const {
            return data.crend();
        }

    private:
        std::vector<Unicode::CodePoint> data;
    };

    static const Unicode::UString EmptyString{};
    static const Unicode::UString SingleSpaceString{' '};

    inline std::ostream &
    operator<<(std::ostream &stream, const Unicode::UString &string) {
        // TODO run actual UTF-8 encoder on the data, and append that.
        for (Unicode::CodePoint character : string) {
            stream << static_cast<char>(character);
        }
        return stream;
    }

} // namespace Unicode
