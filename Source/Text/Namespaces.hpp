/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

namespace Text::Namespaces {

    const char *const HTML = "http://www.w3.org/1999/xhtml";
    const char *const MathML = "http://www.w3.org/1998/Math/MathML";
    const char *const SVG = "http://www.w3.org/2000/svg";
    const char *const XLink = "http://www.w3.org/1999/xlink";
    const char *const XML = "http://www.w3.org/XML/1998/namespace";
    const char *const XMLNS = "http://www.w3.org/2000/xmlns/";

} // namespace Text::Namespaces
