/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * This file contains tools that can be used in conjunction with the
 * Unicode::CodePoint environment.
 */

#pragma once

#include <vector>
#include <Source/Text/UnicodeCommons.hpp>

#include "Source/Data/Stream.hpp"
#include "Source/Text/Unicode.hpp"

#include "EncoderException.hpp"

namespace Encoding {

    template<typename UnitType>
    class Encoder {
    public:

        /**
         * https://encoding.spec.whatwg.org/#continue
         */
        constexpr static Unicode::CodePoint CONTINUE = Unicode::LAST_ALLOWED_CODE_POINT + 1;

        /**
         * Decodes and consumes the whole stream.
         *
         * May throw Encoding::EncoderException
         *
         * https://encoding.spec.whatwg.org/#concept-encoding-run
         */
        [[nodiscard]] std::vector<Unicode::CodePoint>
        DecodeStream(Data::Stream<UnitType> *stream) {
            std::vector<Unicode::CodePoint> output;
            output.reserve(CalculateDecodeSize(stream));

            bool requireNotEOS = false;

            while (stream->IsReady()) {
                auto ret = Decode(stream);

                if (ret == CONTINUE) {
                    requireNotEOS = true;
                    continue;
                }

                requireNotEOS = false;
                output.push_back(ret);
            }

            if (requireNotEOS) {
                throw EncoderException(ErrorType::UNEXPECTED_END_OF_STREAM);
            }

            return output;
        }

        /**
         * Encodes and consumes the whole stream.
         *
         * May throw Encoding::EncoderException
         *
         * https://encoding.spec.whatwg.org/#concept-encoding-run
         */
        [[nodiscard]] std::vector<UnitType>
        EncodeStream(Data::Stream<Unicode::CodePoint> *stream) {
            std::vector<UnitType> output;
            output.reserve(CalculateEncodeSize(stream));

            while (stream->IsReady()) {
                Encode(stream, &output);
            }

            return output;
        }

    private:
        /**
         * Decodes an unit of data from the stream.
         *
         * May throw Encoding::EncoderException
         */
        [[nodiscard]] virtual Unicode::CodePoint
        Decode(Data::Stream<UnitType> *) = 0;

        /**
         * Encodes an unit of data from the stream.
         *
         * May throw Encoding::EncoderException
         */
        virtual void
        Encode(Data::Stream<Unicode::CodePoint> *, std::vector<UnitType> *) = 0;

        /**
         * Calculates how many 'Unicode::CodePoint's the decode process will
         * generate.
         *
         * This function is used, possibly as an estimate, for the superclass
         * decode process. The return value is used as the initial capacity,
         * which will, when implemented correctly, reduce the memory footprint
         * and the amount of calls to realloc, which in turn leads to better a
         * performance.
         */
        [[nodiscard]] virtual std::size_t
        CalculateDecodeSize(const Data::Stream<UnitType> *) = 0;

        /**
         * Same thing as with the CalculateDecodeSize(), but just for encoding.
         */
        [[nodiscard]] virtual std::size_t
        CalculateEncodeSize(const Data::Stream<Unicode::CodePoint> *) = 0;

    };

} // namespace Encoding
