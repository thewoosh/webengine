/*
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "UTF8.hpp"

#include "Source/Text/Encoding/EncoderException.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace Encoding {

    template<typename IntegerType>
    [[nodiscard]] constexpr bool
    IsInsideBoundaries(const IntegerType &input,
                        const IntegerType &lower,
                        const IntegerType &upper) noexcept {
        return input >= lower && input <= upper;
    }

    template<>
    Unicode::CodePoint
    UTF8<std::uint8_t>::Decode(Data::Stream<std::uint8_t> *stream) {
        auto currentByte = stream->Read();

        if (BytesNeeded == 0) {
            if (currentByte <= 0x7F) {
                return currentByte;
            } else if (currentByte >= 0xC2 && currentByte <= 0xDF) {
                BytesNeeded = 1;
                CodePoint = currentByte & 0x1F;
            } else if (currentByte >= 0xE0 && currentByte <= 0xEF) {
                if (currentByte == 0xE0)
                    LowerBoundary = 0xA0;
                else if (currentByte == 0xED)
                    UpperBoundary = 0x9F;
                BytesNeeded = 2;
                CodePoint = currentByte & 0xF;
            } else if (currentByte >= 0xF0 && currentByte <= 0xF4) {
                if (currentByte == 0xF0)
                    LowerBoundary = 0x90;
                else if (currentByte == 0xF4)
                    UpperBoundary = 0x8F;
                BytesNeeded = 3;
                CodePoint = currentByte & 0x7;
            } else {
                throw EncoderException(ErrorType::UTF8_OUT_OF_RANGE);
            }

            return CONTINUE;
        }

        if (!IsInsideBoundaries<std::size_t>(currentByte, LowerBoundary, UpperBoundary)) {
            ResetUTF8Parameters();

            stream->Reconsume();

            throw EncoderException(ErrorType::UTF8_OUT_OF_BOUNDARIES);
        }

        ResetUTF8Boundaries();
        CodePoint = (CodePoint << 6) | (currentByte & 0x3F);

        BytesSeen++;
        if (BytesSeen != BytesNeeded) {
            return CONTINUE;
        }

        const auto returnValue = CodePoint;

        ResetUTF8Parameters();

        return returnValue;
    }

    /**
     * Implements the UTF-8 encoder algorithm as defined by the WHATWG.
     *
     * https://encoding.spec.whatwg.org/#utf-8-encoder
     */
    template<>
    void
    UTF8<std::uint8_t>::Encode(Data::Stream<Unicode::CodePoint> *stream, std::vector<std::uint8_t> *output) {
        while (stream->IsReady()) {
            Unicode::CodePoint codePoint = stream->Read();

            // step 2
            if (Unicode::IsASCIICodePoint(codePoint)) {
                output->push_back(static_cast<std::uint8_t>(codePoint));
                continue;
            }

            // step 3
            std::uint_fast8_t count;
            std::uint_fast8_t offset;

            if (codePoint <= 0x07FF) {
                count = 1;
                offset = 0xC0;
            } else if (codePoint <= 0xFFFF) {
                count = 2;
                offset = 0xE0;
            } else if (codePoint <= 0x10FFFF) {
                count = 3;
                offset = 0xE0;
            } else {
                throw EncoderException(ErrorType::NOT_AN_UNICODE_CODE_POINT);
            }

            // step 4
            output->push_back((codePoint >> (6 * count)) + offset);

            // step 5
            do {
                // step 5.1
                auto temp = codePoint >> (6 * (count - 1));

                // step 5.2
                output->push_back(0x80 | (temp & 0x3F));

                // step 5.3
                count--;
            } while (count > 0);
        }
    }

} // namespace Encoding
