/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * This file contains the basic Unicode symbols for use throughout the software.
 */

#pragma once

#include <cstdint>

namespace Unicode {

    /**
     * Code points are like characters in Unicode.
     */
    using CodePoint = std::uint32_t;

} // namespace Unicode
