/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <compare>
#include <ostream>

namespace Graphics {

    template<typename Type>
    struct Dimensions {
        Type width;
        Type height;

        Dimensions() = default;
        Dimensions(const Dimensions &) = default;
        Dimensions(Dimensions &&) = default;

        Dimensions &operator=(const Dimensions &) = default;
        Dimensions &operator=(Dimensions &&) = default;

        [[nodiscard]] constexpr inline
        Dimensions(Type width, Type height) noexcept
                : width(width)
                , height(height) {
        }

        template<typename OtherType>
        [[nodiscard]] inline constexpr auto
        operator<=>(const Dimensions<OtherType> &rhs) const noexcept {
            if (auto r = width <=> rhs.width; r != 0) return r;
            return height <=> rhs.height;
        }
    };

    template<typename Type>
    [[nodiscard]] inline std::ostream &
    operator<<(std::ostream &stream, Dimensions<Type> dimensions) {
        stream << "Dimensions{" << dimensions.width << 'x' << dimensions.height << '}';
        return stream;
    }

    using IntDimensions = Dimensions<int>;
    using FloatDimensions = Dimensions<float>;

} // namespace Graphics
