/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Graphics {

    template<typename Type>
    struct Vector3 {
        Type x;
        Type y;
        Type z;

        Vector3() = default;
        Vector3(const Vector3 &) = default;
        Vector3(Vector3 &&) = default;

        [[nodiscard]] constexpr inline
        Vector3(Type x, Type y, Type z) noexcept
                : x(x)
                , y(y)
                , z(z) {
        }
    };

    using Vector3f = Vector3<float>;
    using Vector3d = Vector3<double>;
    using Vector3i = Vector3<int>;

} // namespace Graphics
