/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Texture2D.hpp"

#include <array>
#include <iostream>
#include <stdexcept>
#include <string>
#include <type_traits>

#include <GL/glew.h>

#include "Shader.hpp"
#include "ShaderType.hpp"

namespace Graphics {

    [[nodiscard]] inline constexpr GLenum
    ConvertTextureFormatToGLEnum(TextureFormat format) noexcept {
        switch (format) {
            case TextureFormat::GRAYSCALE:
                return GL_RED;
            default:
#ifdef __clang__
                throw std::logic_error(std::string("Illegal value for texture format, or is not registered in ")
                                       + __PRETTY_FUNCTION__);
#else
                return GL_NONE;
#endif
        }
    }

    [[nodiscard]] inline constexpr GLenum
    ConvertIntegerTypeToGLEnum(Texture2D::IntegerType format) {
        switch (format) {
            case Texture2D::IntegerType::UNSIGNED_INT:
                return GL_UNSIGNED_INT;
            case Texture2D::IntegerType::UNSIGNED_CHAR:
                return GL_UNSIGNED_BYTE;
            default:
                throw std::logic_error(std::string("Illegal value for integer type, or is not registered in ")
                                       + __PRETTY_FUNCTION__);
        }
    }

    Texture2D::Texture2D(IntegerType integerType, TextureFormat textureFormat,
                         std::uint32_t width, std::uint32_t height,
                         const void *data) {
        // Apparently, this function always succeeds?
        glGenTextures(1, &textureID);
        Bind();

        auto textureType = ConvertTextureFormatToGLEnum(textureFormat);

        GLint lod{0}, border{0};

        glTexImage2D(GL_TEXTURE_2D,                           // target
                     lod,                                     // LOD
                     textureType,                             // internal format
                     width,                                   // width
                     height,                                  // height
                     border,                                  // border (must be 0)
                     textureType,                             // format (of incoming data)
                     ConvertIntegerTypeToGLEnum(integerType), // type
                     data                                     // data
        );
    }

    Texture2D::~Texture2D() noexcept {
        static_assert(std::is_same_v<GLuint, unsigned int>);

        if (textureID != 0) {
            glDeleteTextures(1, &textureID);
        }
    }

    Texture2D &
    Texture2D::Bind() noexcept {
        glBindTexture(GL_TEXTURE_2D, textureID);
        return *this;
    }

    Texture2D &
    Texture2D::WithMagFilter(TextureMagFilter filter) {
        GLenum type;

        Bind();

        switch (filter) {
            case TextureMagFilter::LINEAR:
                type = GL_LINEAR;
                break;
            case TextureMagFilter::NEAREST:
                type = GL_NEAREST;
                break;
            default:
                throw std::logic_error(std::string("Illegal value for TextureMagFilter, or is not registered in ")
                                       + __PRETTY_FUNCTION__);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, type);

        return *this;
    }

    Texture2D &
    Texture2D::WithMinFilter(TextureMinFilter filter) {
        GLenum type;

        Bind();

        switch (filter) {
            case TextureMinFilter::NEAREST:
                type = GL_NEAREST;
                break;
            case TextureMinFilter::LINEAR:
                type = GL_LINEAR;
                break;
            case TextureMinFilter::NEAREST_MIPMAP_NEAREST:
                type = GL_NEAREST_MIPMAP_NEAREST;
                break;
            case TextureMinFilter::LINEAR_MIPMAP_NEAREST:
                type = GL_LINEAR_MIPMAP_NEAREST;
                break;
            case TextureMinFilter::NEAREST_MIPMAP_LINEAR:
                type = GL_NEAREST_MIPMAP_LINEAR;
                break;
            case TextureMinFilter::LINEAR_MIPMAP_LINEAR:
                type = GL_LINEAR_MIPMAP_LINEAR;
                break;
            default:
                throw std::logic_error(std::string("Illegal value for TextureMinFilter, or is not registered in ")
                                       + __PRETTY_FUNCTION__);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, type);

        return *this;
    }



    Texture2D &
    Texture2D::WithWrap(TextureWrap wrap) {
        GLenum type;

        Bind();

        switch (wrap) {
            case TextureWrap::CLAMP_TO_EDGE:
                type = GL_CLAMP_TO_EDGE;
                break;
            case TextureWrap::CLAMP_TO_BORDER:
                type = GL_CLAMP_TO_BORDER;
                break;
            case TextureWrap::REPEAT:
                type = GL_REPEAT;
                break;
            case TextureWrap::MIRROR_CLAMP_TO_EDGE:
                type = GL_MIRROR_CLAMP_TO_EDGE;
                break;
            case TextureWrap::MIRRORED_REPEAT:
                type = GL_MIRRORED_REPEAT;
                break;
            default:
                throw std::logic_error(std::string("Illegal value for TextureMinFilter, or is not registered in ")
                                       + __PRETTY_FUNCTION__);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, type);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, type);

        return *this;
    }

} // namespace Graphics
