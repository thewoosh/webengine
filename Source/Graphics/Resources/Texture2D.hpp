/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Texture/TextureFormat.hpp"
#include "Source/Graphics/Texture/TextureMagFilter.hpp"
#include "Source/Graphics/Texture/TextureMinFilter.hpp"
#include "Source/Graphics/Texture/TextureSource.hpp"
#include "Source/Graphics/Texture/TextureWrap.hpp"

namespace Graphics {

    class Texture2D {
        unsigned int textureID;

    public:
        enum class IntegerType {
            UNSIGNED_INT,
            UNSIGNED_CHAR,
        };

    private:
        [[nodiscard]] explicit
        Texture2D(IntegerType, TextureFormat, std::uint32_t, std::uint32_t, const void *);

    public:
        [[nodiscard]] inline explicit
        Texture2D(const TextureSource<unsigned char> &source) noexcept
                : Texture2D(IntegerType::UNSIGNED_CHAR, source.format, source.width, source.height, source.data) {
        }

        [[nodiscard]] inline explicit
        Texture2D(const TextureSource<unsigned int> &source) noexcept
                : Texture2D(IntegerType::UNSIGNED_INT, source.format, source.width, source.height, source.data) {
        }

        // Don't allow copying of these objects
        Texture2D(const Texture2D &) = delete;

        inline constexpr
        Texture2D(Texture2D &&rvalue) noexcept
            : textureID(rvalue.textureID) {
            rvalue.textureID = 0;
        }

        ~Texture2D() noexcept;

        [[nodiscard]] constexpr inline int
        TextureID() const noexcept {
            return textureID;
        }

        /* discardable */ Texture2D &Bind() noexcept;
        /* discardable */ Texture2D &WithMagFilter(TextureMagFilter);
        /* discardable */ Texture2D &WithMinFilter(TextureMinFilter);
        /* discardable */ Texture2D &WithWrap(TextureWrap);

    };

} // namespace Graphics
