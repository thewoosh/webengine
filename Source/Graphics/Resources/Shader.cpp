/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Shader.hpp"

#include <array>
#include <iostream>
#include <stdexcept>
#include <type_traits>

#include <GL/glew.h>

namespace Graphics {

    [[nodiscard]] constexpr inline GLenum
    ConvertShaderTypeToGLEnum(ShaderType type) noexcept {
        switch (type) {
            case ShaderType::VERTEX:
                return GL_VERTEX_SHADER;
            case ShaderType::FRAGMENT:
                return GL_FRAGMENT_SHADER;
            default:
#ifdef __clang__
                throw std::runtime_error(std::string("Shader type isn't registered in ") + __PRETTY_FUNCTION__);
#else
                return GL_NONE;
#endif
        }
    }

    Shader::Shader(ConstructionMode constructionMode,
                   ShaderType shaderType,
                   std::string_view sv) {
        static_assert(std::is_same_v<GLuint, decltype(Shader::shaderID)>);
        static_assert(std::is_same_v<GLchar, char>);

        CreateShader(shaderType);
        LoadShaderData(constructionMode, sv);
        CompileShader();
    }

    Shader::~Shader() noexcept {
        if (shaderID != 0) {
            if (programID != 0) {
                glDetachShader(programID, shaderID);
            }

            glDeleteShader(shaderID);
        }
    }

    void Shader::CreateShader(ShaderType shaderType) {
        shaderID = glCreateShader(ConvertShaderTypeToGLEnum(shaderType));
        if (shaderID == 0) {
            throw std::runtime_error("Error (Resource): failed to create GL shader");
        }
    }

    void Shader::LoadShaderData(ConstructionMode constructionMode, std::string_view sv) {
        switch (constructionMode) {
            case ConstructionMode::GLSL_SOURCE:
                LoadGLSLSource(sv);
                break;
            default:
                throw std::logic_error("unimplemented");
        }
    }

    void Shader::CompileShader() {
        glCompileShader(shaderID);

        GLint success{};
        glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);

        if (success == GL_FALSE) {
            std::array<char, 512> buffer{};
            glGetShaderInfoLog(shaderID, std::size(buffer), nullptr, std::data(buffer));
            std::cerr << "[GL] Shader: " << std::data(buffer) << '\n';
            throw std::runtime_error("Shader: compilation failure");
        }
    }

    void Shader::LoadGLSLSource(std::string_view source) {
        const char *data = std::data(source);
        glShaderSource(shaderID, 1, std::addressof(data), nullptr);
    }

    void Shader::Attach(std::uint32_t program) noexcept {
        this->programID = program;
        glAttachShader(program, shaderID);
    }

    void Shader::Detach() noexcept {
        if (programID != 0) {
            glDetachShader(programID, shaderID);
            programID = 0;
        }
    }

} // namespace Graphics
