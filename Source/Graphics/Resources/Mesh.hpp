/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <array>
#include <stdexcept>

#include <cstdint>

namespace Graphics {

    class Mesh {
        unsigned int vao{};
        unsigned int vbo{};

        const std::uint8_t dimensions;
        const std::uint32_t vertexCount;
    public:
        [[nodiscard]]
        Mesh(std::uint8_t dimensionSize, std::uint32_t vertexCount) noexcept;

        Mesh(const Mesh &) = delete;
        Mesh(Mesh &&) = default;

        ~Mesh() noexcept;

        void Draw(const float *);

        template<std::size_t Dims, std::size_t Vertices>
        void
        Draw(const std::array<std::array<float, Dims>, Vertices> &array) {
            if (Dims != dimensions)
                throw std::runtime_error("Mesh: LoadData array dimensions mismatch!");

            if (Vertices != vertexCount)
                throw std::runtime_error("Mesh: LoadData array vertex count mismatch!");

            Draw(std::data(array[0]));
        }

    };

} // namespace Graphics
