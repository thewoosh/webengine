/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "ShaderProgram.hpp"

#include <array>
#include <iostream>
#include <stdexcept>
#include <type_traits>

#include <GL/glew.h>

#include "Shader.hpp"
#include "ShaderType.hpp"

namespace Graphics {

    [[nodiscard]] inline GLint
    CreateValidProgram() {
        int program = glCreateProgram();

        if (program == 0) {
            throw std::runtime_error("ShaderProgram: failed to create GL resource");
        }

        return program;
    }

    ShaderProgram::ShaderProgram(Shader &&inVertexShader, Shader &&inFragmentShader)
            : programID(CreateValidProgram())
            , vertexShader(std::move(inVertexShader))
            , fragmentShader(std::move(inFragmentShader)) {
        vertexShader.Attach(programID);
        fragmentShader.Attach(programID);
        glLinkProgram(programID);

        GLint success{};
        glGetProgramiv(programID, GL_LINK_STATUS, &success);

        if (success == GL_FALSE) {
            std::array<char, 512> buffer{};
            glGetProgramInfoLog(programID, std::size(buffer), nullptr, std::data(buffer));
            std::cerr << "[GL] ShaderProgram: " << std::data(buffer) << '\n';
            throw std::runtime_error("ShaderProgram: link failure");
        }

        glUseProgram(programID);
    }

    ShaderProgram::~ShaderProgram() noexcept {
        vertexShader.Detach();
        fragmentShader.Detach();

        if (programID != 0) {
            glDeleteProgram(programID);
        }
    }

} // namespace Graphics
