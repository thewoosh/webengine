/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

#include "ShaderType.hpp"

namespace Graphics {

    class Shader {
        std::uint32_t shaderID{};
        std::uint32_t programID{};

    public:
        enum class ConstructionMode {
            /**
             * The input sv specifies a path to a file containing GLSL code.
             */
            GLSL_PATH,

            /**
             * The input sv contains GLSL code.
             */
            GLSL_SOURCE
        };

        [[nodiscard]]
        Shader(ConstructionMode, ShaderType, std::string_view);

        Shader(const Shader &) = delete;
        inline
        Shader(Shader &&shader) noexcept
                : shaderID(shader.shaderID)
                , programID(shader.programID) {
            shader.shaderID = 0;
            shader.programID = 0;
        }

        ~Shader() noexcept;

        [[nodiscard]] constexpr inline int
        ShaderID() const noexcept {
            return shaderID;
        }

        void Attach(std::uint32_t program) noexcept;
        void Detach() noexcept;

    private:
        void CreateShader(ShaderType);
        void LoadShaderData(ConstructionMode, std::string_view);
        void CompileShader();

        void LoadGLSLSource(std::string_view);
    };

} // namespace Graphics
