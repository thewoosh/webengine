/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <compare>
#include <ostream>

namespace Graphics {

    template<typename Type>
    struct Vector2 {
        Type x;
        Type y;

        Vector2() = default;
        Vector2(const Vector2 &) = default;
        Vector2(Vector2 &&) = default;

        Vector2 &operator=(const Vector2 &) = default;
        Vector2 &operator=(Vector2 &&) = default;

        [[nodiscard]] constexpr inline
        Vector2(Type x, Type y) noexcept
                : x(x)
                , y(y) {
        }

        template<typename OtherType>
        [[nodiscard]] inline constexpr auto
        operator<=>(const Vector2<OtherType> &rhs) const noexcept {
            if (auto r = x <=> rhs.x; r != 0) return r;
            return y <=> rhs.y;
        }
    };

    template<typename Type>
    [[nodiscard]] inline std::ostream &
    operator<<(std::ostream &stream, Vector2<Type> vector) {
        stream << "Vector2{" << vector.x << ", " << vector.y << '}';
        return stream;
    }

    using Vector2d = Vector2<double>;
    using Vector2f = Vector2<float>;
    using Vector2i = Vector2<int>;

} // namespace Graphics
