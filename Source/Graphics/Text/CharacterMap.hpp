/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <map>

#include "Source/Graphics/Text/Character.hpp"
#include "Source/Text/Unicode.hpp"

namespace Graphics {

    class CharacterMap {
        std::map<Unicode::CodePoint, Character> map{};

    public:
        CharacterMap() = default;
        CharacterMap(const CharacterMap &) = delete;
        CharacterMap(CharacterMap &&) = delete;

        inline void
        Add(Unicode::CodePoint codePoint, Character &&character) {
            map.emplace(codePoint, std::move(character));
        }

        [[nodiscard]] inline const Character *
        operator[](Unicode::CodePoint codePoint) const noexcept {
            auto it = map.find(codePoint);

            if (it == std::end(map)) {
                return nullptr;
            }

            return &it->second;
        }
    };

} // namespace Graphics
