/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Dimensions.hpp"
#include "Source/Graphics/Vector2.hpp"
#include "Source/Graphics/Resources/Texture2D.hpp"

namespace Graphics {

    struct Character {
        Texture2D     texture;
        IntDimensions size;       // Size of glyph
        Vector2i      bearing;    // Offset from baseline to left/top of glyph
        long          advance;    // Offset to advance to next glyph

        [[nodiscard]] inline
        Character(Texture2D &&texture, const IntDimensions &size, const Vector2i &bearing, long advance) noexcept
                : texture(std::move(texture))
                , size(size)
                , bearing(bearing)
                , advance(advance) {
        }

        Character(Character &&) = default;
        Character(const Character &) = delete;
    };

} // namespace Graphics
