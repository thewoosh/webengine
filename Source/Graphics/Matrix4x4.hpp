/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */


#pragma once

#include <array> // for std::array
#include <cstring> // for std::memset

namespace Graphics {

    template<typename Type>
    struct Matrix4x4 {
        std::array<std::array<Type, 4>, 4> data{};

        inline constexpr void
        Zero() noexcept {
            data = {};
        }

        inline constexpr void
        Identity() noexcept {
            for (std::size_t i = 0; i < 4; ++i) {
                for (std::size_t j = 0; j < 4; ++j) {
                    data[i][j] = i == j;
                }
            }
        }

        /**
         * https://en.wikipedia.org/wiki/Orthographic_projection
         * https://wikimedia.org/api/rest_v1/media/math/render/svg/1d2af32ec0b29f7819e989e82c91dcee431a9921
         */
        inline constexpr void
        Ortho(Type left, Type right, Type bottom, Type top, Type near, Type far) noexcept {
            Zero();

            data[0][0] =  2 / (right - left);
            data[1][1] =  2 / (top - bottom);
            data[2][2] = -2 / (far - near);
            data[3][0] = -(right + left) / (right - left);
            data[3][1] = -(top + bottom) / (top - bottom);
            data[3][2] = -(far + near) / (far - near);
            data[3][3] = 1;
        }
    };

} // namespace Graphics
