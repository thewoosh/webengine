/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint> // for std::uint32_t

#include "Source/Graphics/Texture/TextureFormat.hpp"

namespace Graphics {

    template<typename DataFormat>
    struct TextureSource {
        TextureFormat format;
        std::uint32_t width;
        std::uint32_t height;

        const DataFormat *data;
    };

} // namespace Graphics
