/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Dimensions.hpp"

//
// DEBUG
//
#define GRAPHICS_WINDOW_GLFW

//
// Platform-independent Window API
//

#ifdef GRAPHICS_WINDOW_GLFW
    typedef struct GLFWwindow GLFWwindow;
#endif

namespace Graphics {

    class Window {
#ifdef GRAPHICS_WINDOW_GLFW
        GLFWwindow *window{};
#endif

    public:
        [[nodiscard]]
        Window() noexcept;
        ~Window() noexcept;

        /**
         * Check if window creation was successful.
         */
        [[nodiscard]] operator bool();
        [[nodiscard]] bool IsCloseRequested();
        [[nodiscard]] IntDimensions Size() const;
        void SwapBuffers();

    };

} // namespace Graphics
