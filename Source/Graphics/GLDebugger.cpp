/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Graphics/GLDebugger.hpp"

#include <iostream>

#include <GL/glew.h>

namespace Graphics {

    namespace GLDebugger {

        [[nodiscard]] inline constexpr std::string_view
        translateErrorType(GLenum type) {
            switch (type) {
                case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "deprecated-behavior";
                case GL_DEBUG_TYPE_ERROR: return "error";
                case GL_DEBUG_TYPE_OTHER: return "other";
                case GL_DEBUG_TYPE_PORTABILITY: return "portability";
                case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "undefined-behavior";
                default:
                    return "(invalid)";
            }
        }

        [[nodiscard]] inline constexpr std::string_view
        translateSeverity(GLenum type) {
            switch (type) {
                case GL_DEBUG_SEVERITY_LOW: return "low";
                case GL_DEBUG_SEVERITY_MEDIUM: return "medium";
                case GL_DEBUG_SEVERITY_NOTIFICATION: return "notification";
                case GL_DEBUG_SEVERITY_HIGH: return "high";
                default:
                    return "(invalid)";
            }
        }

        [[nodiscard]] inline constexpr std::string_view
        translateSource(GLenum type) {
            switch (type) {
                case GL_DEBUG_SOURCE_API: return "api";
                case GL_DEBUG_SOURCE_OTHER: return "other";
                case GL_DEBUG_SOURCE_SHADER_COMPILER: return "shader-compiler";
                case GL_DEBUG_SOURCE_THIRD_PARTY: return "third-party";
                case GL_DEBUG_SOURCE_WINDOW_SYSTEM: return "window-system";
                default: return "(invalid)";
            }
        }

    }

    void GLAPIENTRY
    messageCallback(GLenum source, GLenum type, GLuint id, GLenum severity,
                    GLsizei length, const GLchar *message, const void *) {
        std::cout << std::flush;
        auto &stream = std::cout;
        stream << "\x1b[37m=====[ \x1b[31mOpenGL Implementation Message \x1b[37m]=====\x1b[0m\n";
        stream << "       Type: " << GLDebugger::translateErrorType(type) << '\n';
        stream << "       Severity: " << GLDebugger::translateSeverity(severity) << '\n';
        stream << "       Source: " << GLDebugger::translateSource(source) << '\n';
        stream << "       ID: " << id << '\n';
        stream << "       Message: \"" << std::string_view(message, length) << "\"\n";
    }

    void
    GLDebugger::setup() noexcept {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(&messageCallback, nullptr);
    }

} // namespace Graphics
