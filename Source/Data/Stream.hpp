/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 - 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

namespace Data {

    /**
     * The Stream class is an abstraction for continuous resources. This class
     * provides an abstract interface for many different types of resources.
     * This way, a stream can get its resources either from disk, network memory
     * etc.
     */
    template<typename Unit>
    class Stream {
    public:

        virtual ~Stream() = default;

        /**
         * Returns whether or not the stream is past its elements.
         */
        [[nodiscard]] virtual bool
        IsEOF() const noexcept = 0;

        /**
         * Returns whether or not an Unit is ready to be read from this stream.
         */
        [[nodiscard]] virtual bool
        IsReady() const noexcept = 0;

        /**
         * Same behavior as Read(), but won't progress the cursor or pointer.
         *
         * @see Read()
         */
        [[nodiscard]] virtual Unit
        Peek() const noexcept = 0;

        /**
         * Populates the range.
         *
         * [Fail behavior]
         * This algorithm tries to populate the range by Read(), but if/when EOF
         * is reached, the function returns false. The data inside the range is
         * possibly undefined, so shouldn't be trusted. The stream is then reset
         * by Reconsume()'ing the partially-consumed range.
         *
         * @returns true if the stream was populated, otherwise false
         */
        template<typename IteratorType>
        [[nodiscard]] inline bool
        Populate(IteratorType begin, IteratorType end) {
            std::size_t reconsumeCounter = 0;

            for (IteratorType it = begin; it != end; ++it) {
                if (IsEOF()) {
                    for (std::size_t i = 0; i < reconsumeCounter; i++) {
                        Reconsume();
                    }

                    return false;
                }

                *it = Read();
                ++reconsumeCounter;
            }

            return true;
        }

        /**
         * Returns an Unit from this stream. This function will block if no Unit
         * is ready. To avoid blocking, check the status of the stream by
         * invoking the IsReady() function.
         *
         * On EOF, this function will return 0 as represented by the Unit. To
         * avoid clashes with normal Units of value 0, invoke the function
         * IsEOF().
         */
        [[nodiscard]] virtual Unit
        Read() noexcept = 0;

        /**
         * Reconsuming an unit from the stream will reduce the position in the
         * stream.
         *
         * Reconsuming an unit when the position is at the beginning of the
         * stream does nothing and doesn't throw.
         *
         * Reconsuming an unit when the position is past the end of the stream
         * doesn't reduce the position, and will remain EOS.
         *
         * This also means that the previous call to Read() will be reverted,
         * effectively making it a Peek().
         */
        virtual void
        Reconsume() noexcept = 0;

    };

} // namespace Data
