/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 - 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <string_view>

#include "Stream.hpp"

namespace Data {

    /**
     * A stream specifically for strings (std::string, std::string_view and
     * string literals).
     *
     * Note that this stream is only for 7-bit ASCII strings, the definition
     * characters above 0x7F are up to the std implementation, as the string
     * isn't being parsed or decoded; this stream is just a convenience stream
     * similar to IteratorStream.
     */
    template<typename Unit=std::uint8_t>
    class StringStream : public Stream<Unit> {
    private:
        constexpr void
        EnsureSafePosition() noexcept {
            if (pos > sv.length()) {
                pos = sv.length();
            }
        }

    public:
        constexpr explicit
        StringStream(const std::string_view &sv)
            : sv(sv), pos(0) {
        }

        /**
         * Returns whether or not the stream is past its elements.
         */
        [[nodiscard]] constexpr bool
        IsEOF() const noexcept {
            return pos >= sv.length();
        }

        /**
         * @see Stream::Peek
         */
        [[nodiscard]] constexpr bool
        IsReady() const noexcept {
            return pos < sv.length();
        }

        /**
         * @see Stream::Peek
         */
        [[nodiscard]] constexpr Unit
        Peek() const noexcept {
            if (IsEOF()) {
                return static_cast<Unit>(0);
            }

            return static_cast<Unit>(sv.at(pos));
        }

        /**
         * @see Stream::Read
         */
        [[nodiscard]] constexpr Unit
        Read() noexcept {
            if (IsEOF()) {
                EnsureSafePosition();
                return static_cast<Unit>(0);
            }

            return static_cast<Unit>(sv.at(pos++));
        }

        /**
         * @see Stream::Reconsume
         */
        void
        Reconsume() noexcept {
            if (pos < sv.length())
                pos--;
            else
                pos = sv.length(); // prevent overflow
        }

    private:
        std::string_view sv;
        std::size_t pos;
    };

} // namespace Data
