/*
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * https://url.spec.whatwg.org/
 */

#pragma once

#include <optional>
#include <string>

template<typename UserDataType=void *>
class URLParser;

namespace Web {

    class URL {
        friend class URLParser<>;
    public: // TODO make getters&setters and make members private
        std::string scheme{""};
        std::string username{""};
        std::string password{""};
        std::optional<std::string> host;
        std::uint16_t port{0};
        std::vector<std::string> path;
        std::optional<std::string> query;
        std::optional<std::string> fragment;
        bool cannotBeBaseURLFlag{false};
        // <<<<<< blob URL entry >>>>>>
    public:
        URL() = default;
    };

} // namespace Web
