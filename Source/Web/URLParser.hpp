/*
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: 2020-2021 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * https://url.spec.whatwg.org/
 */

#pragma once

#include <functional>

#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

#include "URL.hpp"
#include "URLValidationError.hpp"

namespace Web {

    template<typename UserDataType=void *>
    class URLParser {
    public:
        URLParser() = default;

    private:
        std::function<void(const URLValidationError &, UserDataType &)> validationCallback{};
        UserDataType userData{};

        /**
         * This will check if the std::function is empty.
         */
        [[nodiscard]] constexpr bool
        validate() const {
            return validationCallback.operator bool();
        }

        inline void
        reportValidationError(const URLValidationError &error) {
            if (validate()) {
                validationCallback(error, userData);
            }
        }

        void checkEmpty(const auto &input) {
            if (input.empty()) {
                reportValidationError({
                    {},
                    "input-empty",
                    "The parser was called with an empty string input."
                });
            }
        }

        constexpr bool
        IsSpaceOrC0(auto character) {
            return Unicode::IsC0Control(character) || character == Unicode::SPACE;
        };

        void validateSurroundingCharacters(const auto &input) {
            if (IsSpaceOrC0(input.front()) || IsSpaceOrC0(input.back())) {
                reportValidationError({
                    {},
                    "illegal-character",
                    "The parser was called with a string input containing one or more illegal characters."
                });
            }
        }

    public:
        /**
         * Parses an URL.
         */
        [[nodiscard]] URL
        parse(std::string input) {
            checkEmpty(input);
            if (validate()) {
                validateSurroundingCharacters(input);
            }

            URL url{};

            std::size_t beginIndex = 0;
            std::size_t endIndex = input.length();

            for (; beginIndex < endIndex; beginIndex++) {
                if (!IsSpaceOrC0(input[beginIndex])) {
                    break;
                }
            }

            for (; endIndex > 0; endIndex--) {
                if (!IsSpaceOrC0(input[endIndex - 1])) {
                    break;
                }
            }

            auto characterCount = endIndex - beginIndex;
            input = input.substr(beginIndex, characterCount);
            std::cout << "[+] String is \"" << input << "\"\n";

            input.erase(std::remove_if(std::begin(input), std::end(input), [](char c) {
                return c == Unicode::CHARACTER_TABULATION || c == Unicode::LINE_FEED;
            }), std::end(input));

            std::cout << "[+] String is \"" << input << "\"\n";

            return url;
        }
    };

} // namespace Web

