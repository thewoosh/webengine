/*
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "URL.hpp"

namespace Web {

    struct URLValidationError {

        /**
         * The URL if any could be generated anyway.
         *
         * Should only be used for debugging purposes.
         */
        std::optional<URL> url{};

        /**
         * A constant string containing the a tag which specifies the failure
         * that was encountered.
         *
         * Should be compatible with translations in the future.
         */
        std::string reason{};

        /**
         * A string containing a more user-friendly message describing the
         * problem.
         */
        std::string message{};
    };

}
