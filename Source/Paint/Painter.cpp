/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Paint/Painter.hpp"

#include <iostream>
#include <sstream>
#include <stdexcept>

#include <GL/glew.h>

#include "Source/Graphics/GLDebugger.hpp"
#include "Source/Graphics/Matrix4x4.hpp"

#define WINDOW_MAX_WIDTH 3000
#define WINDOW_MAX_HEIGHT 3000

namespace Paint {

    Painter::Painter() noexcept {
        Graphics::GLDebugger::setup();
    }

    void Painter::PaintRect(Rect rect, Color color) {
        const auto beginX = rect.position.x;
        const auto endX   = rect.position.x + rect.size.width;

        const auto beginY = rect.position.y;
        const auto endY   = rect.position.y + rect.size.height;

        rectPainter.Paint(beginX, endX, beginY, endY, color);

        auto err = glGetError();
        if (err != GL_NO_ERROR) {
            std::cerr << "Error Report for Painter\n";
            std::cerr << "GLError: " << err << '\n';
        }
    }

    void Painter::SetViewPort(Graphics::IntDimensions dimensions) {
        if (dimensions.width <= 0 ||
                dimensions.height <= 0 ||
                dimensions.width  >= WINDOW_MAX_WIDTH ||
                dimensions.height >= WINDOW_MAX_HEIGHT) {
            std::stringstream stream;
            stream << "Error Report for Painter\n";
            stream << "Illegal Window Dimensions: " << dimensions << '\n';
            throw std::runtime_error(stream.str());
        }

        viewport = dimensions;

        glViewport(0, 0, dimensions.width, dimensions.height);

        Graphics::Matrix4x4<float> matrix{};
        matrix.Ortho(0, dimensions.width,
                     dimensions.height, 0,
                     -1, 1);

        rectPainter.SetProjectionMatrix(matrix);
        textPainter.SetProjectionMatrix(matrix);
    }

    void Painter::Begin() {
        glClearColor(1, 1, 1, 1);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    void Painter::End() {
    }

} // namespace Paint
