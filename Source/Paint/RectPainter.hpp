/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Matrix4x4.hpp"
#include "Source/Graphics/Resources/Mesh.hpp"
#include "Source/Graphics/Resources/ShaderProgram.hpp"
#include "Source/Paint/Color.hpp"
#include "Source/Paint/Rect.hpp"

namespace Paint {
    
    class RectPainter {
        int shapeColorUniformLocation{};

        Graphics::ShaderProgram shaderProgram;
        Graphics::Mesh mesh{2, 6};
        Graphics::Mesh meshTriangle{2, 3};

    public:
        RectPainter();

        void Paint(float beginX, float endX, float beginY, float endY, Color);
        void PaintTriangle(Graphics::Vector2f, Graphics::Vector2f, Graphics::Vector2f, Color);
        void SetProjectionMatrix(const Graphics::Matrix4x4<float> &);
    };
    
} // namespace Paint
