/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <compare>

#include "Source/Graphics/Dimensions.hpp"
#include "Source/Graphics/Vector2.hpp"

namespace Paint {

    struct Rect {
        Graphics::Vector2f position{};
        Graphics::FloatDimensions size{};

        Rect() = default;

        [[nodiscard]] constexpr inline
        Rect(Graphics::Vector2f position, Graphics::FloatDimensions size) noexcept
                : position(position)
                , size(size) {
        }

        [[nodiscard]] constexpr inline
        Rect(float x, float y, float width, float height) noexcept
                : position(x, y)
                , size(width, height) {
        }

        [[nodiscard]] friend inline constexpr auto
        operator<=>(Rect lhs, Rect rhs) noexcept {
            if (auto r = lhs.position <=> rhs.position; r != 0) return r;
            return lhs.size <=> rhs.size;
        }

    };

} // namespace Rect
