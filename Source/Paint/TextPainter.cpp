/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Paint/TextPainter.hpp"

#include <array>
#include <iostream>
#include <stdexcept>

#include <unistd.h>

#include <GL/glew.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_ADVANCES_H

#include "Source/Text/UnicodeCommons.hpp"

namespace Paint {

    TextPainter::TextPainter()
            : shaderProgram(Graphics::Shader(Graphics::Shader::ConstructionMode::GLSL_SOURCE, Graphics::ShaderType::VERTEX, R"(
#version 330 core
layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>
out vec2 textureCoordinates;

uniform mat4 projection;

void main() {
    gl_Position = projection * vec4(vertex.xy, 0.0, 1.0);
    textureCoordinates = vertex.zw;
}
)")
            , Graphics::Shader(Graphics::Shader::ConstructionMode::GLSL_SOURCE, Graphics::ShaderType::FRAGMENT, R"(
#version 330 core
in vec2 textureCoordinates;
out vec4 color;

uniform sampler2D text;
uniform vec4 textColor;

void main() {
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, textureCoordinates).r);
    color = textColor * sampled;
}
)")) {
        glUseProgram(shaderProgram.ProgramID());

        auto samplerUniform = glGetUniformLocation(shaderProgram.ProgramID(), "text");
        if (samplerUniform == -1) {
            throw std::runtime_error("Error Report for Painter\nCouldn't find uniform location of \"text\"");
        }
        glUniform1i(samplerUniform, 0);

        textColorUniformLocation = glGetUniformLocation(shaderProgram.ProgramID(), "textColor");
        if (textColorUniformLocation == -1) {
            throw std::runtime_error("Error Report for Painter\nCouldn't find uniform location of \"shapeColor\"");
        }

        SetupFont();
    }

    bool
    TextPainter::doesFileExists(std::string_view path) noexcept {
        return access(path.data(), R_OK) == 0;
    }

    std::string_view
    TextPainter::findFontFamilyPath() noexcept {
#define TRY_PATH(path) if (doesFileExists(path)) return path;
        TRY_PATH("/usr/local/share/fonts/OTF/SanFranciscoText-Regular.otf")

#ifdef __FreeBSD__
        TRY_PATH("/usr/local/share/fonts/noto/NotoSans-Regular.ttf")
        TRY_PATH("/usr/local/share/fonts/dejavu/DejaVuSans.ttf")
#else
        TRY_PATH("/usr/share/fonts/truetype/lato/Lato-Regular.ttf")
#endif
#undef TRY_PATH
        return {};
    }

    const Graphics::Character &
    TextPainter::GetCharacterForCodePoint(Unicode::CodePoint codePoint) const {
        const auto *ptr = characterMap[codePoint];

        if (ptr != nullptr) {
            return *ptr;
        }

        if ((ptr = characterMap[Unicode::REPLACEMENT_CHARACTER])) {
            return *ptr;
        }

        if ((ptr = characterMap[Unicode::QUESTION_MARK])) {
            return *ptr;
        }

        throw std::runtime_error("TextPainter: insufficient character map");
    }

    void TextPainter::Paint(Graphics::Vector2f position, const Unicode::UString &text, Color color) {
//        glEnable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glUseProgram(shaderProgram.ProgramID());
        glUniform4f(textColorUniformLocation, color.red / 255.0, color.green / 255.0, color.blue / 255.0, color.alpha / 255.0);
        glActiveTexture(GL_TEXTURE0);

        const auto &fullSizeCharacter = GetCharacterForCodePoint(Unicode::LATIN_CAPITAL_LETTER_H);

        if (text.length() != 0) {
            position.x -= GetCharacterForCodePoint(text[0]).bearing.x;
        }

        for (Unicode::CodePoint codePoint : text) {
            const auto &character = GetCharacterForCodePoint(codePoint);

            const float beginX = position.x + character.bearing.x;
            const float endX   = beginX + character.size.width;

            const float beginY = position.y + character.size.height + (fullSizeCharacter.bearing.y - character.bearing.y);
            const float endY   = beginY - character.size.height;

            glBindTexture(GL_TEXTURE_2D, character.texture.TextureID());

            mesh.Draw(std::array<std::array<float, 4>, 6>{{
                    { beginX, endY,   0.0f, 0.0f },
                    { beginX, beginY, 0.0f, 1.0f },
                    { endX,   beginY, 1.0f, 1.0f },

                    { beginX, endY,   0.0f, 0.0f },
                    { endX,   beginY, 1.0f, 1.0f },
                    { endX,   endY,   1.0f, 0.0f }
            }});

            position.x += character.advance;
        }

        glBindTexture(GL_TEXTURE_2D, 0);

        glDisable(GL_BLEND);
        glDisable(GL_CULL_FACE);
    }

    void TextPainter::SetProjectionMatrix(const Graphics::Matrix4x4<float> &matrix) {
        glUseProgram(shaderProgram.ProgramID());

        auto location = glGetUniformLocation(shaderProgram.ProgramID(), "projection");
        if (location == -1) {
            throw std::runtime_error("Error Report for Painter\nCouldn't find uniform location of \"projection\"\n");
        }

        glUniformMatrix4fv(location, 1, GL_FALSE, &matrix.data[0][0]);

        glUseProgram(0);
    }

    static void AddGlyph(FT_Face face, Graphics::CharacterMap &characterMap, Unicode::CodePoint codePoint) {
        if (FT_Load_Char(face, codePoint, FT_LOAD_RENDER)) {
            std::cerr << "Warning: Failed to load Glyph: 0x" << std::hex << static_cast<std::uint16_t>(codePoint) << " i.e. " << (char)codePoint << '\n';
            return;
        }

        Graphics::Character character {
                std::move(Graphics::Texture2D(Graphics::TextureSource<unsigned char>{
                        Graphics::TextureFormat::GRAYSCALE,
                        face->glyph->bitmap.width,
                        face->glyph->bitmap.rows,
                        face->glyph->bitmap.buffer
                })
                                  .WithWrap(Graphics::TextureWrap::CLAMP_TO_EDGE)
                                  .WithMinFilter(Graphics::TextureMinFilter::LINEAR)
                                  .WithMagFilter(Graphics::TextureMagFilter::LINEAR)),
                Graphics::IntDimensions(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                Graphics::Vector2i(face->glyph->bitmap_left, face->glyph->bitmap_top),
                face->glyph->advance.x >> 6
        };

        characterMap.Add(codePoint, std::move(character));
    }

    void TextPainter::SetupFont() {
        FT_Library ft{};
        if (FT_Init_FreeType(&ft)) {
            throw std::runtime_error("Error: Could not init FreeType Library\n");
        }

        FT_Face face;
        if (FT_New_Face(ft, findFontFamilyPath().data(), 0, &face)) {
            throw std::runtime_error("Error: Could not load font\n");
        }

        FT_Set_Pixel_Sizes(face, 0, 48);

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // disable byte-alignment restriction

        for (Unicode::CodePoint codePoint = 0x20; codePoint < 0x7F; codePoint++) {
            AddGlyph(face, characterMap, codePoint);
        }

        AddGlyph(face, characterMap, 0x1e00);
        AddGlyph(face, characterMap, 0x00C5); // Å

        verticalAdvance = (face->bbox.yMax - face->bbox.yMin) >> 6;
        verticalAdvance += -(face->descender >> 6);

        FT_Done_Face(face);
        FT_Done_FreeType(ft);
    }

    Graphics::Dimensions<std::uint32_t>
    TextPainter::CalculateTextSize(const Unicode::UString &text) const {
        Graphics::Dimensions<std::uint32_t> dimensions{};
        dimensions.height = verticalAdvance;

        bool firstFlag{true};
        for (Unicode::CodePoint codePoint : text) {
            const auto &character = GetCharacterForCodePoint(codePoint);
            dimensions.width += character.advance;

            if (firstFlag) {
                firstFlag = false;
                dimensions.width -= character.bearing.x;
            }
        }

        if (text.length() != 0) {
            if (text.length() == 1 && text[0] == ' ') {
                // kinda a hack, but it works
                return dimensions;
            }

            const auto &character = GetCharacterForCodePoint(text[text.length() - 1]);
            dimensions.width -= character.advance - character.size.width - character.bearing.x;
        }

        return dimensions;
    }

} // namespace Paint
