/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Matrix4x4.hpp"
#include "Source/Graphics/Resources/Mesh.hpp"
#include "Source/Graphics/Resources/ShaderProgram.hpp"
#include "Source/Graphics/Text/CharacterMap.hpp"
#include "Source/Paint/Color.hpp"
#include "Source/Paint/Rect.hpp"
#include "Source/Text/UString.hpp"

#include <string_view>

namespace Paint {
    
    class TextPainter {
        int textColorUniformLocation;
        short verticalAdvance{};

        Graphics::ShaderProgram shaderProgram;
        Graphics::CharacterMap characterMap{};
        Graphics::Mesh mesh{4, 6};

        void SetupFont();

        [[nodiscard]] const Graphics::Character &
        GetCharacterForCodePoint(Unicode::CodePoint) const;

        [[nodiscard]] static bool
        doesFileExists(std::string_view path) noexcept;

        [[nodiscard]] static std::string_view
        findFontFamilyPath() noexcept;

    public:
        TextPainter();

        void Paint(Graphics::Vector2f, const Unicode::UString &, Color);
        void SetProjectionMatrix(const Graphics::Matrix4x4<float> &);

        [[nodiscard]] Graphics::Dimensions<std::uint32_t>
        CalculateTextSize(const Unicode::UString &) const;
    };
    
} // namespace Paint
