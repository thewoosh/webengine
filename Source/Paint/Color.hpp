/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint> // for std::uint8_t

namespace Paint {

    struct Color {

        std::uint8_t red{};
        std::uint8_t green{};
        std::uint8_t blue{};
        std::uint8_t alpha{};

        [[nodiscard]]
        Color() = default;

        [[nodiscard]] constexpr inline
        Color(std::uint8_t red, std::uint8_t green, std::uint8_t blue, std::uint8_t alpha = 255)
                : red(red)
                , green(green)
                , blue(blue)
                , alpha(alpha) {
        }

    };

    /**
     * https://www.w3.org/TR/css-color-3/#html4
     */
    namespace BasicColors {

        static constexpr Color BLACK{0, 0, 0};
        static constexpr Color SILVER{192, 192, 192};
        static constexpr Color GRAY{128, 128, 128};
        static constexpr Color WHITE{255, 255, 255};
        static constexpr Color MAROON{128, 0, 0};
        static constexpr Color RED{255, 0, 0};
        static constexpr Color PURPLE{128, 0, 128};
        static constexpr Color FUCHSIA{255, 0, 255};
        static constexpr Color GREEN{0, 128, 0};
        static constexpr Color LIME{0, 255, 0};
        static constexpr Color OLIVE{128, 128, 0};
        static constexpr Color YELLOW{255, 255, 0};
        static constexpr Color NAVY{0, 128, 0};
        static constexpr Color BLUE{0, 0, 255};
        static constexpr Color TEAL{0, 128, 128};
        static constexpr Color AQUA{0, 255, 255};

    } // namespace BasicColors

} // namespace Paint
