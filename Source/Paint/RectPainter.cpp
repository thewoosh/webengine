/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Paint/RectPainter.hpp"

#include <array>
#include <stdexcept>

#include <GL/glew.h>

namespace Paint {

    RectPainter::RectPainter()
            : shaderProgram(Graphics::Shader(Graphics::Shader::ConstructionMode::GLSL_SOURCE,
                                             Graphics::ShaderType::VERTEX, R"(
#version 330 core

layout (location = 0) in vec2 vertex;
uniform mat4 projection;

void main() {
    gl_Position = projection * vec4(vertex, 0.0, 1.0);
}
)"),
                            Graphics::Shader(Graphics::Shader::ConstructionMode::GLSL_SOURCE,
                                             Graphics::ShaderType::FRAGMENT, R"(
#version 330 core

out vec4 outputColor;

uniform vec4 shapeColor;

void main() {
    outputColor = shapeColor;
}
)")) {

        glUseProgram(shaderProgram.ProgramID());
        shapeColorUniformLocation = glGetUniformLocation(shaderProgram.ProgramID(), "shapeColor");

        if (shapeColorUniformLocation == -1) {
            throw std::runtime_error("Error Report for Painter\nCouldn't find uniform location of \"shapeColor\"");
        }
    }

    void RectPainter::Paint(float beginX, float endX, float beginY, float endY, Color color) {
        glUseProgram(shaderProgram.ProgramID());
        glUniform4f(shapeColorUniformLocation, color.red / 255.0, color.green / 255.0, color.blue / 255.0, color.alpha / 255.0);

        mesh.Draw(std::array<std::array<float, 2>, 6>{{
                {beginX, endY},
                {beginX, beginY},
                {endX,   beginY},

                {beginX, endY},
                {endX,   beginY},
                {endX,   endY},
        }});
    }

    void RectPainter::PaintTriangle(Graphics::Vector2f vectorA, Graphics::Vector2f vectorB, Graphics::Vector2f vectorC, Color color) {
        glUseProgram(shaderProgram.ProgramID());
        glUniform4f(shapeColorUniformLocation, color.red / 255.0, color.green / 255.0, color.blue / 255.0, color.alpha / 255.0);

        meshTriangle.Draw(std::array<std::array<float, 2>, 3>{{
                {vectorA.x, vectorA.y},
                {vectorB.x, vectorB.y},
                {vectorC.x, vectorC.y},
        }});
    }

    void RectPainter::SetProjectionMatrix(const Graphics::Matrix4x4<float> &matrix) {
        glUseProgram(shaderProgram.ProgramID());

        auto location = glGetUniformLocation(shaderProgram.ProgramID(), "projection");
        if (location == -1) {
            throw std::runtime_error("Error Report for Painter\nCouldn't find uniform location of \"projection\"\n");
        }

        glUniformMatrix4fv(location, 1, GL_FALSE, &matrix.data[0][0]);

        glUseProgram(0);
    }

} // namespace Paint
