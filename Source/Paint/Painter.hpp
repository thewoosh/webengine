/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Resources/ShaderProgram.hpp"
#include "Source/Graphics/Dimensions.hpp"
#include "Source/Paint/Color.hpp"
#include "Source/Paint/Rect.hpp"
#include "Source/Paint/RectPainter.hpp"
#include "Source/Paint/TextPainter.hpp"
#include "Source/Text/UString.hpp"

namespace Paint {

    class Painter {
        Graphics::IntDimensions viewport{};
        RectPainter rectPainter{};
        TextPainter textPainter{};

    public:
        [[nodiscard]]
        Painter() noexcept;

        void Begin();
        void End();

        void SetViewPort(Graphics::IntDimensions);
        void PaintRect(Rect, Color);

        [[nodiscard]] inline TextPainter &
        Text() noexcept {
            return textPainter;
        }

        [[nodiscard]] inline const TextPainter &
        Text() const noexcept {
            return textPainter;
        }

        [[nodiscard]] inline RectPainter &
        GetRectPainter() noexcept {
            return rectPainter;
        }

        [[nodiscard]] inline const RectPainter &
        GetRectPainter() const noexcept {
            return rectPainter;
        }
    };

} // namespace Paint
