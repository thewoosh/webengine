/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <functional>

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"

namespace HTML {

    /**
     * The tokenizer holds the context and states.
     */
    class Tokenizer {
    public:
        Tokenizer(const auto &stream, HTMLLogger &&logger) :
            context{stream, std::move(logger)}
        {}

        /**
         * Executes the Tokenizer, which will convert the stream into a list of
         * tokens.
         */
        void Run();

    private:
        TokenizerContext context;

        /**
         * For testing purposes, the following flag is set so the cycling of
         * steps by the tokenizer can be halted in a safe-manner (i.e. without
         * exceptions).
         */
         bool abortFlag{false};

        /**
         * A simple bridge function for wanting to invoke the state
         * implementation that is associated with the provided state name.
         */
        void InvokeStateImplementation(TokenizerStateName state);

    public:
        struct ImplementationMap {
            using FunctionType = std::function<void(TokenizerContext &)>;

            std::array<FunctionType, static_cast<std::size_t>(TokenizerStateName::IN_FOREIGN_CONTENT)>
                    lookup{};

            ImplementationMap() noexcept;

            [[nodiscard]] constexpr FunctionType &
            byName(TokenizerStateName stateName) {
                return lookup[static_cast<std::size_t>(stateName)];
            }

        private:
            void
            add(TokenizerStateName name, const FunctionType &function) {
                lookup[static_cast<std::size_t>(name)] = function;
            }
        };

        ImplementationMap implementationMap{};

    public:
        [[nodiscard]] TokenizerContext &
        Context() noexcept {
            return context;
        }

        inline void
        Abort() {
            abortFlag = true;
        }
    };

} // namespace HTML
