/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Tokenizer.hpp"

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStates/Index.hpp"
#include "Source/HTML/TokenizerStateName.hpp"

namespace HTML {

    void
    Tokenizer::InvokeStateImplementation(TokenizerStateName state) {
        auto function = implementationMap.byName(state);

        if (function) {
            function(context);
        } else {
            context.logger.log<LogModule::PARSE_ABORT>(
                    "Tokenizer was tasked with invoking an unimplemented state",
                    ": ", state, "!\n"
            );
        }
    }

    void Tokenizer::Run() {
        context.status = TokenizerStatus::RUNNING;

        while (true) {
            if (abortFlag) {
                context.status = TokenizerStatus::ABORTED;
                break;
            }

            const auto previousStateName = context.currentState;
            // TODO pause flag

            if (context.nextState != TokenizerStateName::UNDEFINED) {
                context.currentState = context.nextState;
                context.nextState = TokenizerStateName::UNDEFINED;
            }

            if (context.currentState == TokenizerStateName::UNDEFINED) {
                context.logger.log<LogModule::PARSE_ABORT>(
                        "The current tokenizer state is UNDEFINED, which is an "
                        "illegal null-like value which should only be used by "
                        "TokenizerContext::nextState and TokenizerContext::"
                        "returnState!\n");
                context.status = TokenizerStatus::FAILED;
                break;
            }

            if (previousStateName != context.currentState && !implementationMap.byName(context.currentState)) {
                context.logger.log<LogModule::PARSE_ABORT>(
                        "Tokenizer state ", context.currentState,
                        " isn't implemented, but was required by ",
                        previousStateName, "!\n"
                );
                context.status = TokenizerStatus::FAILED_TODO;
                break;
            }

#ifdef HTML_TOKENIZER_LOG_STATE_INVOCATION
            context.logger.log<LogModule::DEBUG>("Invoking state: ", context.currentState);
#endif
            InvokeStateImplementation(context.currentState);

            if (!context.tokenList.empty() && context.tokenList.back()->type == TokenType::END_OF_FILE) {
                context.logger.log<LogModule::DEBUG>("EOF token detected, exiting Tokenizer!");
                context.status = TokenizerStatus::SUCCESS;
                break;
            }
        }
    }

    Tokenizer::ImplementationMap::ImplementationMap() noexcept {
        add(TokenizerStateName::DATA, TokenizerStates::DataState);
        add(TokenizerStateName::MARKUP_DECLARATION_OPEN, TokenizerStates::MarkupDeclarationOpenState);

        add(TokenizerStateName::RAWTEXT, TokenizerStates::RawTextState);
        add(TokenizerStateName::RAWTEXT_LESS_THAN_SIGN, TokenizerStates::RawTextLessThanSignState);
        add(TokenizerStateName::RAWTEXT_END_TAG_OPEN, TokenizerStates::RawTextEndTagOpenState);
        add(TokenizerStateName::RAWTEXT_END_TAG_NAME, TokenizerStates::RawTextEndTagNameState);

        add(TokenizerStateName::RCDATA, TokenizerStates::RcdataState);
        add(TokenizerStateName::RCDATA_LESS_THAN_SIGN, TokenizerStates::RcdataLessThanSignState);
        add(TokenizerStateName::RCDATA_END_TAG_OPEN, TokenizerStates::RcdataEndTagOpenState);
        add(TokenizerStateName::RCDATA_END_TAG_NAME, TokenizerStates::RcdataEndTagNameState);

        add(TokenizerStateName::AFTER_DOCTYPE_NAME, TokenizerStates::AfterDoctypeNameState);
        add(TokenizerStateName::AFTER_DOCTYPE_PUBLIC_IDENTIFIER, TokenizerStates::AfterDoctypePublicIdentifierState);
        add(TokenizerStateName::AFTER_DOCTYPE_PUBLIC_KEYWORD, TokenizerStates::AfterDoctypePublicKeywordState);
        add(TokenizerStateName::AFTER_DOCTYPE_SYSTEM_IDENTIFIER, TokenizerStates::AfterDoctypeSystemIdentifierState);
        add(TokenizerStateName::AFTER_DOCTYPE_SYSTEM_KEYWORD, TokenizerStates::AfterDoctypeSystemKeywordState);
        add(TokenizerStateName::BEFORE_DOCTYPE_NAME, TokenizerStates::BeforeDoctypeNameState);
        add(TokenizerStateName::BEFORE_DOCTYPE_PUBLIC_IDENTIFIER, TokenizerStates::BeforeDoctypePublicIdentifier);
        add(TokenizerStateName::BEFORE_DOCTYPE_SYSTEM_IDENTIFIER, TokenizerStates::BeforeDoctypeSystemIdentifier);
        add(TokenizerStateName::BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS, TokenizerStates::BetweenDoctypePublicAndSystemIdentifiersState);
        add(TokenizerStateName::BOGUS_DOCTYPE, TokenizerStates::BogusDoctypeState);
        add(TokenizerStateName::DOCTYPE, TokenizerStates::DoctypeState);
        add(TokenizerStateName::DOCTYPE_NAME, TokenizerStates::DoctypeNameState);
        add(TokenizerStateName::DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED, TokenizerStates::DoctypePublicIdentifierDoubleQuoted);
        add(TokenizerStateName::DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED, TokenizerStates::DoctypePublicIdentifierSingleQuoted);
        add(TokenizerStateName::DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED, TokenizerStates::DoctypeSystemIdentifierDoubleQuoted);
        add(TokenizerStateName::DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED, TokenizerStates::DoctypeSystemIdentifierSingleQuoted);

        add(TokenizerStateName::END_TAG_OPEN, TokenizerStates::EndTagOpenState);
        add(TokenizerStateName::TAG_NAME, TokenizerStates::TagNameState);
        add(TokenizerStateName::TAG_OPEN, TokenizerStates::TagOpenState);

        add(TokenizerStateName::AFTER_ATTRIBUTE_NAME, TokenizerStates::AfterAttributeNameState);
        add(TokenizerStateName::AFTER_ATTRIBUTE_VALUE_QUOTED, TokenizerStates::AfterAttributeValueUnquoted);
        add(TokenizerStateName::ATTRIBUTE_NAME, TokenizerStates::AttributeNameState);
        add(TokenizerStateName::ATTRIBUTE_VALUE_DOUBLE_QUOTED, TokenizerStates::AttributeValueDoubleQuoted);
        add(TokenizerStateName::ATTRIBUTE_VALUE_SINGLE_QUOTED, TokenizerStates::AttributeValueSingleQuoted);
        add(TokenizerStateName::ATTRIBUTE_VALUE_UNQUOTED, TokenizerStates::AttributeValueUnquoted);
        add(TokenizerStateName::BEFORE_ATTRIBUTE_NAME, TokenizerStates::BeforeAttributeNameState);
        add(TokenizerStateName::BEFORE_ATTRIBUTE_VALUE, TokenizerStates::BeforeAttributeValueState);

        add(TokenizerStateName::BOGUS_COMMENT, TokenizerStates::BogusCommentState);
        add(TokenizerStateName::COMMENT, TokenizerStates::CommentState);
        add(TokenizerStateName::COMMENT_END, TokenizerStates::CommentEndState);
        add(TokenizerStateName::COMMENT_END_BANG, TokenizerStates::CommentEndBangState);
        add(TokenizerStateName::COMMENT_END_DASH, TokenizerStates::CommentEndDashState);
        add(TokenizerStateName::COMMENT_LESS_THAN_SIGN, TokenizerStates::CommentLessThanSignState);
        add(TokenizerStateName::COMMENT_LESS_THAN_SIGN_BANG, TokenizerStates::CommentLessThanSignBangState);
        add(TokenizerStateName::COMMENT_LESS_THAN_SIGN_BANG_DASH, TokenizerStates::CommentLessThanSignBangDashState);
        add(TokenizerStateName::COMMENT_LESS_THAN_SIGN_BANG_DASH_DASH, TokenizerStates::CommentLessThanSignBangDashDashState);
        add(TokenizerStateName::COMMENT_START, TokenizerStates::CommentStartState);
        add(TokenizerStateName::COMMENT_START_DASH, TokenizerStates::CommentStartDashState);
    }

} // namespace HTML
