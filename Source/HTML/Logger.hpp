/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <optional>
#include <string_view>

#include "Source/Base/Terminate.hpp"
#include "Source/Text/UString.hpp"

namespace HTML {

    enum class LogModule {
        /**
         * Reflect development information.
         */
        DEBUG,

        /**
         * Explain why the parser was forced to abort.
         */
        PARSE_ABORT,

        /**
         * Report a parse error.
         */
        PARSE_ERROR,
    };

    inline std::ostream &
    operator<<(std::ostream &stream, const std::optional<Unicode::UString> &string) {
        if (string.has_value()) {
            stream << '"' << string.value() << '"';
        } else {
            stream << "missing";
        }
        return stream;
    }

    class HTMLLogger {
    public:
        /**
         * Get the name of the LogModule.
         */
        [[nodiscard]] inline constexpr const char *
        nameOfLogModule(LogModule mod) noexcept {
            switch (mod) {
                case LogModule::DEBUG:
                    return "Debug";
                case LogModule::PARSE_ABORT:
                    return "ParseAbort";
                case LogModule::PARSE_ERROR:
                    return "ParseError";
                default: SWITCH_DEFAULT_UNREACHABLE();
            }
        }

        /**
         * Log something.
         */
        template<LogModule mod, class...Types>
        inline void log(const Types&...elements) requires(sizeof...(elements) > 0) {
            auto &stream = findStream();

            stream << '[' << nameOfLogModule(mod) << "] ";

//            ([&stream](const auto &thing) mutable {
//                stream << thing << ' ';
//            }(elements), ...);

            ((stream << elements), ...);

            // intentional endline to avoid buffering which is unwanted anyway
            // in case of debugging.
            stream << std::endl;
        }

    private:
        std::unique_ptr<std::ostream> selfStream;
        std::optional<std::reference_wrapper<std::ostream>> refStream;

        inline std::ostream &
        findStream() {
            if (selfStream)
                return *selfStream;
            else
                return *refStream;
        }

    public:
        /**
         * Constructor for loggers that keep their own stream.
         */
        explicit
        HTMLLogger(std::unique_ptr<std::ostream> &&stream)
            : selfStream(std::move(stream)), refStream{std::nullopt}
        {}

        /**
         * Constructor for loggers that keep their own stream.
         */
        explicit
        HTMLLogger(std::ostream &stream)
        : selfStream{nullptr}, refStream(stream)
        {}
    };

} // namespace HTML
