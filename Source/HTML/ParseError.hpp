/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020-2021 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * https://html.spec.whatwg.org/multipage/parsing.html#parse-errors
 */

#pragma once

namespace HTML {

    enum class ParseError {
        ABRUPT_CLOSING_OF_EMPTY_COMMENT,
        ABRUPT_DOCTYPE_PUBLIC_IDENTIFIER,
        ABRUPT_DOCTYPE_SYSTEM_IDENTIFIER,
        ABSENCE_OF_DIGITS_IN_NUMERIC_CHARACTER_REFERENCE,
        CDATA_IN_HTML_CONTENT,
        CHARACTER_REFERENCE_OUTSIDE_UNICODE_RANGE,
        CONTROL_CHARACTER_IN_INPUT_STREAM,
        CONTROL_CHARACTER_REFERENCE,
        END_TAG_WITH_ATTRIBUTES,
        DUPLICATE_ATTRIBUTE,
        END_TAG_WITH_TRAILING_SOLIDUS,
        EOF_BEFORE_TAG_NAME,
        EOF_IN_CDATA,
        EOF_IN_COMMENT,
        EOF_IN_DOCTYPE,
        EOF_IN_SCRIPT_HTML_COMMENT_LIKE_TEXT,
        EOF_IN_TAG,
        INCORRECTLY_CLOSED_COMMENT,
        INCORRECTLY_OPENED_COMMENT,
        INVALID_CHARACTER_SEQUENCE_AFTER_DOCTYPE_NAME,
        INVALID_FIRST_CHARACTER_OF_TAG_NAME,
        MISSING_ATTRIBUTE_VALUE,
        MISSING_DOCTYPE_NAME,
        MISSING_DOCTYPE_PUBLIC_IDENTIFIER,
        MISSING_DOCTYPE_SYSTEM_IDENTIFIER,
        MISSING_END_TAG_NAME,
        MISSING_QUOTE_BEFORE_DOCTYPE_PUBLIC_IDENTIFIER,
        MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER,
        MISSING_SEMICOLON_AFTER_CHARACTER_REFERENCE,
        MISSING_WHITESPACE_AFTER_DOCTYPE_PUBLIC_KEYWORD,
        MISSING_WHITESPACE_AFTER_DOCTYPE_SYSTEM_KEYWORD,
        MISSING_WHITESPACE_BEFORE_DOCTYPE_NAME,
        MISSING_WHITESPACE_BETWEEN_ATTRIBUTES,
        MISSING_WHITESPACE_BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS,
        NESTED_COMMENT,
        NONCHARACTER_CHARACTER_REFERENCE,
        NONCHARACTER_IN_INPUT_STREAM,
        NON_VOID_HTML_ELEMENT_START_TAG_WITH_TRAILING_SOLIDUS,
        NULL_CHARACTER_REFERENCE,
        SURROGATE_CHARACTER_REFERENCE,
        SURROGATE_IN_INPUT_STREAM,
        UNEXPECTED_CHARACTER_AFTER_DOCTYPE_SYSTEM_IDENTIFIER,
        UNEXPECTED_CHARACTER_IN_ATTRIBUTE_NAME,
        UNEXPECTED_CHARACTER_IN_UNQUOTED_ATTRIBUTE_VALUE,
        UNEXPECTED_EQUALS_SIGN_BEFORE_ATTRIBUTE_NAME,
        UNEXPECTED_NULL_CHARACTER,
        UNEXPECTED_QUESTION_MARK_INSTEAD_OF_TAG_NAME,
        UNEXPECTED_SOLIDUS_IN_TAG,
        UNKNOWN_NAMED_CHARACTER_REFERENCE,

        //
        // Start of parse errors defined by the tree constructor stage. The
        // names of these parse errors are non-normative.
        //

        /**
         * UA encountered a DOCTYPE token in the initial insertion mode, which
         * is not expected in HTML documents.
         *
         * https://html.spec.whatwg.org/multipage/parsing.html#the-initial-insertion-mode
         */
        DOCTYPE_NOT_HTML_LIKE,

        /**
         * UA encountered a DOCTYPE token outside the initial insertion mode.
         */
        DOCTYPE_INCORRECTLY_PLACED,

        /**
         * UA encountered an end-of-file token at the "text" insertion mode,
         * which is illegal in that context.
         */
        EOF_IN_TEXT_MODE,

        /**
         * UA encountered a <head> token inside another head element, e.g.:
         *
         * <head>
         *   <title>Hello!</title>
         *   <head>
         *     <link href="styles.css" rel="stylesheet">
         *   </head>
         * </head>
         */
        HEAD_START_TAG_TOKEN_IN_HEAD,

        /**
         * UA encountered an end tag token which wasn't expected at this mode.
         *
         * Shouldn't be used in the case where an end tag token was encountered
         * without an associated start tag.
         */
        UNEXPECTED_END_TAG_TOKEN,

        /**
         * UA encountered a token that shouldn't be processed by the current
         * insertion mode.
         */
        UNEXPECTED_TOKEN,

        /**
         * There were multiple <head> tokens found.
         *
         * E.g.
         * <html>
         *   <head></head>
         *   <head></head>
         *   ^^^^^
         * </html>
         */
        MULTIPLE_HEAD_TOKENS,

        /**
         * https://html.spec.whatwg.org/multipage/parsing.html#parsing-main-inbody:parse-errors-11
         */
        HEADING_TAGS_CANT_NEST,

        /**
         * When invoking the algorithm "Generate implied end tags" in the tree
         * construction stage, the current element's name doesn't match the one
         * of the tree constructor end tag token.
         */
        IMPLIED_END_TAGS_IS_NOT_CURRENT_ELEMENT,

        /**
         * A special node was encountered in the stack of open elements, whilst
         * processing the "Any other end tag" clause in the "in body" insertion mode.
         *
         * https://html.spec.whatwg.org/multipage/parsing.html#parsing-main-inbody:parse-errors-39
         */
        SPECIAL_NODE_ENCOUNTERED_IN_BODY_ANY_OTHER_END_TAG,
    };

    [[nodiscard]] constexpr const char *
    stringOfParseError(ParseError error) noexcept {
        switch (error) {
            case ParseError::ABRUPT_CLOSING_OF_EMPTY_COMMENT: return "abrupt-closing-of-empty-comment";
            case ParseError::ABRUPT_DOCTYPE_PUBLIC_IDENTIFIER: return "abrupt-doctype-public-identifier";
            case ParseError::ABRUPT_DOCTYPE_SYSTEM_IDENTIFIER: return "abrupt-doctype-system-identifier";
            case ParseError::ABSENCE_OF_DIGITS_IN_NUMERIC_CHARACTER_REFERENCE: return "absence-of-digits-in-numeric-character-reference";
            case ParseError::CDATA_IN_HTML_CONTENT: return "cdata-in-html-content";
            case ParseError::CHARACTER_REFERENCE_OUTSIDE_UNICODE_RANGE: return "character-reference-outside-unicode-range";
            case ParseError::CONTROL_CHARACTER_IN_INPUT_STREAM: return "control-character-in-input-stream";
            case ParseError::CONTROL_CHARACTER_REFERENCE: return "control-character-reference";
            case ParseError::END_TAG_WITH_ATTRIBUTES: return "end-tag-with-attributes";
            case ParseError::DUPLICATE_ATTRIBUTE: return "duplicate-attribute";
            case ParseError::END_TAG_WITH_TRAILING_SOLIDUS: return "end-tag-with-trailing-solidus";
            case ParseError::EOF_BEFORE_TAG_NAME: return "eof-before-tag-name";
            case ParseError::EOF_IN_CDATA: return "eof-in-cdata";
            case ParseError::EOF_IN_COMMENT: return "eof-in-comment";
            case ParseError::EOF_IN_DOCTYPE: return "eof-in-doctype";
            case ParseError::EOF_IN_SCRIPT_HTML_COMMENT_LIKE_TEXT: return "eof-in-script-html-comment-like-text";
            case ParseError::EOF_IN_TAG: return "eof-in-tag";
            case ParseError::INCORRECTLY_CLOSED_COMMENT: return "incorrectly-closed-comment";
            case ParseError::INCORRECTLY_OPENED_COMMENT: return "incorrectly-opened-comment";
            case ParseError::INVALID_CHARACTER_SEQUENCE_AFTER_DOCTYPE_NAME: return "invalid-character-sequence-after-doctype-name";
            case ParseError::INVALID_FIRST_CHARACTER_OF_TAG_NAME: return "invalid-first-character-of-tag-name";
            case ParseError::MISSING_ATTRIBUTE_VALUE: return "missing-attribute-value";
            case ParseError::MISSING_DOCTYPE_NAME: return "missing-doctype-name";
            case ParseError::MISSING_DOCTYPE_PUBLIC_IDENTIFIER: return "missing-doctype-public-identifier";
            case ParseError::MISSING_DOCTYPE_SYSTEM_IDENTIFIER: return "missing-doctype-system-identifier";
            case ParseError::MISSING_END_TAG_NAME: return "missing-end-tag-name";
            case ParseError::MISSING_QUOTE_BEFORE_DOCTYPE_PUBLIC_IDENTIFIER: return "missing-quote-before-doctype-public-identifier";
            case ParseError::MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER: return "missing-quote-before-doctype-system-identifier";
            case ParseError::MISSING_SEMICOLON_AFTER_CHARACTER_REFERENCE: return "missing-semicolon-after-character-reference";
            case ParseError::MISSING_WHITESPACE_AFTER_DOCTYPE_PUBLIC_KEYWORD: return "missing-whitespace-after-doctype-public-keyword";
            case ParseError::MISSING_WHITESPACE_AFTER_DOCTYPE_SYSTEM_KEYWORD: return "missing-whitespace-after-doctype-system-keyword";
            case ParseError::MISSING_WHITESPACE_BEFORE_DOCTYPE_NAME: return "missing-whitespace-before-doctype-name";
            case ParseError::MISSING_WHITESPACE_BETWEEN_ATTRIBUTES: return "missing-whitespace-between-attributes";
            case ParseError::MISSING_WHITESPACE_BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS: return "missing-whitespace-between-doctype-public-and-system-identifiers";
            case ParseError::NESTED_COMMENT: return "nested-comment";
            case ParseError::NONCHARACTER_CHARACTER_REFERENCE: return "noncharacter-character-reference";
            case ParseError::NONCHARACTER_IN_INPUT_STREAM: return "noncharacter-in-input-stream";
            case ParseError::NON_VOID_HTML_ELEMENT_START_TAG_WITH_TRAILING_SOLIDUS: return "non-void-html-element-start-tag-with-trailing-solidus";
            case ParseError::NULL_CHARACTER_REFERENCE: return "null-character-reference";
            case ParseError::SURROGATE_CHARACTER_REFERENCE: return "surrogate-character-reference";
            case ParseError::SURROGATE_IN_INPUT_STREAM: return "surrogate-in-input-stream";
            case ParseError::UNEXPECTED_CHARACTER_AFTER_DOCTYPE_SYSTEM_IDENTIFIER: return "unexpected-character-after-doctype-system-identifier";
            case ParseError::UNEXPECTED_CHARACTER_IN_ATTRIBUTE_NAME: return "unexpected-character-in-attribute-name";
            case ParseError::UNEXPECTED_CHARACTER_IN_UNQUOTED_ATTRIBUTE_VALUE: return "unexpected-character-in-unquoted-attribute-value";
            case ParseError::UNEXPECTED_EQUALS_SIGN_BEFORE_ATTRIBUTE_NAME: return "unexpected-equals-sign-before-attribute-name";
            case ParseError::UNEXPECTED_NULL_CHARACTER: return "unexpected-null-character";
            case ParseError::UNEXPECTED_QUESTION_MARK_INSTEAD_OF_TAG_NAME: return "unexpected-question-mark-instead-of-tag-name";
            case ParseError::UNEXPECTED_SOLIDUS_IN_TAG: return "unexpected-solidus-in-tag";
            case ParseError::UNKNOWN_NAMED_CHARACTER_REFERENCE: return "unknown-named-character-reference";

            //
            // Non-normative names used by the tree constructor should be
            // prefixed with "tc-".
            //

            case ParseError::DOCTYPE_NOT_HTML_LIKE: return "tc-doctype-not-html-like";
            case ParseError::DOCTYPE_INCORRECTLY_PLACED: return "tc-doctype-incorrectly-placed";
            case ParseError::EOF_IN_TEXT_MODE: return "tc-eof-in-text-mode";
            case ParseError::HEAD_START_TAG_TOKEN_IN_HEAD: return "tc-head-start-tag-token-in-head";
            case ParseError::UNEXPECTED_END_TAG_TOKEN: return "tc-unexpected-end-tag-token";
            case ParseError::UNEXPECTED_TOKEN: return "tc-unexpected-token";
            case ParseError::MULTIPLE_HEAD_TOKENS: return "tc-multiple-head-tokens";
            case ParseError::HEADING_TAGS_CANT_NEST: return "tc-heading-tags-can't-nest";
            case ParseError::IMPLIED_END_TAGS_IS_NOT_CURRENT_ELEMENT: return "tc-implied-end-tags-isn't-current-element";
            case ParseError::SPECIAL_NODE_ENCOUNTERED_IN_BODY_ANY_OTHER_END_TAG: return "tc-special-node-encountered-in-body-any-other-end-tag";

            default: SWITCH_DEFAULT_UNREACHABLE();
        }
    }

    constexpr auto &
    operator<<(auto &stream, ParseError error) {
        stream << stringOfParseError(error);
        return stream;
    }

} // namespace HTML
