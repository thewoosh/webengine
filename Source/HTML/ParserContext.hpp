/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <memory> // for std::shared_ptr, std::make_shared
#include <vector> // for std::vector

#include "Source/DOM/Document.hpp"
#include "Source/DOM/Element.hpp"

#include "Source/HTML/InsertionModeName.hpp"
#include "Source/HTML/ParseError.hpp"
#include "Source/HTML/Token.hpp"

#include "Source/HTML/Elements/HTMLFormElement.hpp"
#include "Source/HTML/Elements/HTMLHeadElement.hpp"

#define PARSE_ERROR(type) \
    do {                       \
        parserContext.ReportParseError(type, __FILE__, __LINE__); \
    } while (0);


namespace HTML {

    struct TokenizerContext;
    class TreeConstructor;

    struct ParserContext {

        TokenizerContext *tokenizerContext{};
        TreeConstructor *treeConstructor{};

        std::shared_ptr<DOM::Document> document = std::make_shared<DOM::Document>();

        [[nodiscard]] std::shared_ptr<DOM::Node>
        CurrentNode() noexcept {
            if (std::empty(stackOfOpenElements))
                return document;

            return stackOfOpenElements.back();
        }

        InsertionModeName currentInsertionMode{InsertionModeName::INITIAL};

        /**
         * https://html.spec.whatwg.org/multipage/parsing.html#original-insertion-mode
         */
        InsertionModeName originalInsertionMode{};

        std::vector<std::shared_ptr<DOM::Element>> stackOfOpenElements{};

        struct FormattingElement {
            std::shared_ptr<DOM::Element> element{};
            const TagToken *tokenWhichCreatedMe{};
            bool isMarker{false};

            inline
            FormattingElement(const std::shared_ptr<DOM::Element> &element,
                              const TagToken *tokenWhichCreatedMe,
                              bool isMarker) noexcept
                    : element(element)
                    , tokenWhichCreatedMe(tokenWhichCreatedMe)
                    , isMarker(isMarker) {
            }

            FormattingElement(FormattingElement &&) = default;
            FormattingElement(const FormattingElement &) = default;
        };

        /**
         * https://html.spec.whatwg.org/multipage/parsing.html#list-of-active-formatting-elements
         * https://html.spec.whatwg.org/multipage/parsing.html#formatting
         */
        std::vector<FormattingElement> listOfActiveFormattingElements{};

        /**
         * https://html.spec.whatwg.org/multipage/parsing.html#stack-of-template-insertion-modes
         */
        std::vector<InsertionModeName> stackOfTemplateInsertionModes{};

        /**
         * TODO when implementing <iframe>s, set this flag somehow for parsing of
         *
         * https://html.spec.whatwg.org/multipage/iframe-embed-object.html#an-iframe-srcdoc-document
         */
        bool isSrcdocDocument{false};

        /**
         * TODO currently, this flag is always disabled.
         *
         * https://html.spec.whatwg.org/multipage/parsing.html#scripting-flag
         */
        bool scriptingFlag{false};

        /**
         * Is only updated, never queried atm.
         *
         * https://html.spec.whatwg.org/multipage/parsing.html#frameset-ok-flag
         */
        bool framesetOk{true};

        //
        // Element pointers
        // https://html.spec.whatwg.org/multipage/parsing.html#the-element-pointers
        //
        std::shared_ptr<HTMLHeadElement> headElementPointer{nullptr};
        std::shared_ptr<HTMLFormElement> formElementPointer{nullptr};

        /**
         * Use this function for debugging purposes, instead of setting the
         * currentInsertionMode manually.
         */
        inline void
        SwitchInsertionModeTo(InsertionModeName newInsertionMode) {
            currentInsertionMode = newInsertionMode;
        }

        void
        ReportParseError(ParseError error, const char *, std::size_t);

        /**
         * https://html.spec.whatwg.org/multipage/parsing.html#using-the-rules-for
         */
        void
        ProcessUsingOtherRules(InsertionModeName, const Token &);

        inline void
        PopCurrentNodeOffStack() {
            stackOfOpenElements.pop_back();
//            if (stackOfOpenElements.empty())
//                currentNode = document;
//            else
//                currentNode = stackOfOpenElements.back();
        }

        inline void
        ReprocessToken(const Token &token) {
            // Name of the function that it is calling may seem weird, but the
            // use case is correct.
            ProcessUsingOtherRules(currentInsertionMode, token);
        }

        [[nodiscard]] inline bool
        IsElementOfTypeOnStackOfOpenElements(DOM::CommonElementType commonElementType) const noexcept {
            return std::any_of(std::begin(stackOfOpenElements), std::end(stackOfOpenElements),
           [commonElementType](const std::shared_ptr<DOM::Element> &element) {
                    return element->commonElementType == commonElementType;
                }
            );
        }
    };

} // namespace HTML
