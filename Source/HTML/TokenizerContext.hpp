/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <iomanip>
#include <iostream>
#include <memory>
#include <vector>

namespace HTML {

    struct TokenizerContext;

} // namespace HTML

#include "Source/Data/Stream.hpp"
#include "Source/HTML/Logger.hpp"
#include "Source/HTML/ParseError.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/HTML/TokenizerStatus.hpp"
#include "Source/HTML/TreeConstructor.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace HTML {

    struct TokenizerContextDataBuilder {

        template<typename TokenType>
        struct TokenWrapper {
            std::unique_ptr<TokenType> token{nullptr};

            explicit TokenWrapper(TokenizerContext &context) : context(context) {}

            [[nodiscard]] TokenType *
            operator->() {
                return token.operator->();
            }

            template<typename...Types>
            void create(Types...types) {
                token = std::make_unique<TokenType>(std::forward<Types>(types)...);
            }

            void emit();

            [[nodiscard]] bool
            isAppropriateEndTagToken();

        private:
            TokenizerContext &context;
        };

        TokenWrapper<CommentToken> commentToken;
        TokenWrapper<DoctypeToken> doctypeToken;
        TokenWrapper<TagToken> tagToken;

        explicit TokenizerContextDataBuilder(TokenizerContext &context) :
            commentToken(context),
            doctypeToken(context),
            tagToken(context)
        {}

    };

    /**
     * The tokenizer context holds the current state and flags for the tokenizer
     * and it's execution states.
     */
    struct TokenizerContext {

        std::shared_ptr<Data::Stream<Unicode::CodePoint>> stream;

        HTMLLogger logger;

        TreeConstructor treeConstructor{this};

        std::vector<std::unique_ptr<Token>> tokenList{};

        TokenizerContextDataBuilder builder{*this};

        TokenizerStateName currentState{TokenizerStateName::DATA};
        TokenizerStateName nextState{TokenizerStateName::UNDEFINED};
        TokenizerStateName returnState{TokenizerStateName::UNDEFINED};

        TokenizerStatus status{TokenizerStatus::INITIAL};

        Unicode::UString temporaryBuffer{};

        inline void EmitToken(auto &&token) {
            tokenList.push_back(std::forward<decltype(token)>(token));

            treeConstructor.DispatchEmit(*(tokenList.back()));
        }

        inline void
        EmitCharacterToken(Unicode::CodePoint character) {
            EmitToken(std::make_unique<CharacterToken>(character));
        }

        inline void
        EmitEOFToken() {
            EmitToken(std::make_unique<Token>(TokenType::END_OF_FILE));
        }

        inline void
        ReconsumeIn(TokenizerStateName state) {
            stream->Reconsume();
            nextState = state;
        }

        inline void
        ReportParseError(ParseError error) {
            logger.log<LogModule::PARSE_ERROR>(error);
        }

        inline void
        SwitchToState(TokenizerStateName state) {
            nextState = state;
        }

        inline void
        ReconsumeStream(std::size_t times) {
            for (std::size_t i = 0; i < times; i++) {
                stream->Reconsume();
            }
        }

        template<std::size_t LiteralLength, std::size_t Length>
        inline bool
        CompareCodePointsAndStringLiteral(const char (&literal)[LiteralLength],
                                          const std::array<Unicode::CodePoint, Length> &array,
                                          bool caseSensitive) {
            static_assert(LiteralLength - 1 == Length);

            for (std::size_t i = 0; i < Length; i++) {
                auto literalCharacter = static_cast<Unicode::CodePoint>(literal[i]);

                if (caseSensitive ?
                    (literalCharacter != array[i]) :
                    (literalCharacter != Unicode::ToLowerASCII(array[i]))
                        ) {
                    return false;
                }
            }

            return true;
        }

    };

    template<typename T>
    void
    TokenizerContextDataBuilder::TokenWrapper<T>::emit() {
        if constexpr (std::is_same_v<T, DoctypeToken>) {
            DoctypeToken *doctype = reinterpret_cast<DoctypeToken *>(token.get());
            context.logger.log<LogModule::DEBUG>(
                    "force-quirks=", doctype->forceQuirks,
                    ", name: ", token->name,
                    ", public identifier: ", token->publicIdentifier,
                    ", system identifier: ", token->systemIdentifier,
                    ".");
        } else if (std::is_same_v<T, TagToken>) {
            TagToken *tag = reinterpret_cast<TagToken *>(token.get());
            context.logger.log<LogModule::DEBUG>(
                    "Emitting tag-token(", tag->type,
                    ") name: ", tag->name,
                    ", self-closing: ", tag->selfClosing,
                    ".");
        } else {
            context.logger.log<LogModule::DEBUG>("Emitted generic ", token->type, " token");
        }

        context.EmitToken(std::move(token));
    }

    template<>
    inline bool
    TokenizerContextDataBuilder::TokenWrapper<TagToken>::isAppropriateEndTagToken() {
        if (token->type != TokenType::END_TAG) {
            return false;
        }

        Unicode::UString *lastStartTagName{nullptr};
        for (auto it = context.tokenList.crbegin(); it != context.tokenList.crend(); ++it) {
            if (it->get()->type == TokenType::START_TAG) {
                lastStartTagName = &static_cast<TagToken *>(it->get())->name;
                break;
            }
        }

        if (!lastStartTagName) {
            return false;
        }

        return lastStartTagName->equalsIgnoreCase(static_cast<TagToken *>(token.get())->name);
    }

} // namespace HTML

#define HTML_TOKENIZER_CONTEXT_DEFINED
