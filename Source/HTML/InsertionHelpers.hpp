/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <optional>

#include "Source/DOM/Comment.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/DOM/ResettableElement.hpp"
#include "Source/DOM/Text.hpp"
#include "Source/DOM/Helpers/Create.hpp"
#include "Source/HTML/ParserContext.hpp"
#include "Source/Text/Namespaces.hpp"

#define PARSER_TODO(message)                                          \
    do {                                                              \
        std::cout << "TODO: " << message << '\n';                     \
        std::cout << "Function: " << __PRETTY_FUNCTION__ << '\n';     \
        std::cout << "File: " << __FILE__ << ':' << __LINE__ << '\n'; \
        throw std::runtime_error(std::string("TODO: ") + message);    \
    } while(0);

#define STOP_PARSING() return; // TODO

/**
 * Subroutines defined by the HTML standard which can be called from insertion
 * modes.
 */
namespace HTML::InsertionHelpers {

    [[nodiscard]] inline bool
    IsWhitespaceToken(const Token *token) {
        if (token->type != TokenType::CHARACTER)
            return false;
        Unicode::CodePoint character = static_cast<const CharacterToken *>(token)->data;
        return character == Unicode::CHARACTER_TABULATION ||
               character == Unicode::LINE_FEED ||
               character == Unicode::FORM_FEED ||
               character == Unicode::CARRIAGE_RETURN ||
               character == Unicode::SPACE;
    }

    struct Position {
        std::shared_ptr<DOM::Node> node;
        decltype(DOM::Node::childNodes)::const_iterator iterator;
    };

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#appropriate-place-for-inserting-a-node
     */
    inline Position
    FindAppropriatePlaceForInsertingNode(HTML::ParserContext *context,
                                         std::shared_ptr<DOM::Node> overrideTarget=nullptr) {
        std::shared_ptr<DOM::Node> target =
                overrideTarget ? overrideTarget : context->CurrentNode();

        // TODO foster parenting

        return {
                target,
                std::cend(target->childNodes)
        };
    }

    inline void
    InsertComment(HTML::ParserContext *context,
                  const Unicode::UString &data,
                  std::optional<Position> optionalPosition={}) {
        Position position = optionalPosition.has_value() ?
                optionalPosition.value() :
                FindAppropriatePlaceForInsertingNode(context);

        position.node->childNodes.insert(position.iterator, std::make_shared<DOM::Comment>(data));
    }

    /**
     * Note: discarding return value is OK.
     */
    inline std::shared_ptr<DOM::DocumentType>
    InsertDocumentType(HTML::ParserContext *context,
                       const Unicode::UString &name,
                       const Unicode::UString *publicId,
                       const Unicode::UString *systemId) {
        Position position{ context->document, context->document->childNodes.end() };

        position.node->childNodes.insert(position.iterator, std::make_shared<DOM::DocumentType>(
                Unicode::UString(name),
                publicId == nullptr ? Unicode::UString() : Unicode::UString(*publicId),
                systemId == nullptr ? Unicode::UString() : Unicode::UString(*systemId)
        ));

        return std::static_pointer_cast<DOM::DocumentType>(position.node->childNodes.back());
    }

    [[nodiscard]] inline const Unicode::UString *
    FindAttributeInList(const TokenAttributeList &list, const Unicode::UString &name) {
        for (const TokenAttribute &attribute : list) {
            if (attribute.name.equalsIgnoreCase(name)) {
                return &attribute.value;
            }
        }

        return nullptr;
    }

    [[nodiscard]] inline std::shared_ptr<DOM::Element>
    CreateElementForToken(HTML::ParserContext *context,
                          const TagToken *token,
                          const Unicode::UString &givenNamespace,
                          std::shared_ptr<DOM::Node> intendedParent) {
        static_cast<void>(intendedParent);

        // step 1
        // spec states that the document must be retrieved from the intended
        // parent, but we don't store a pointer to the document in each element
        // for optimization purposes. When parsing iframe's, we should probably
        // take this as an optional parameter.
        std::shared_ptr<DOM::Document> document = context->document;

        // step 2
        const Unicode::UString &localName = token->name;

        // step 3
        const Unicode::UString *is = FindAttributeInList(token->attributes, Unicode::UString("is"));

        //
        // step 4 & 5
        // TODO custom element definition
        //
        // step 6
        // TODO execute script (also synchronous custom elements flag)
        //

        // step 7
        std::shared_ptr<DOM::Element> element = DOM::Helpers::CreateElement(
                document, localName, givenNamespace, nullptr, is);

        // step 8
        for (const TokenAttribute &attribute : token->attributes) {
            element->attributeList.emplace(attribute.name, attribute.value);
        }

        // step 9
        // TODO execute script

        // step 10
        // TODO xml

        // step 11
        DOM::ResettableElement *resettableElement = dynamic_cast<DOM::ResettableElement *>(element.get());
        if (resettableElement) {
            resettableElement->InvokeReset();
        }

        // step 12
        // TODO form element with custom element definitions

        // step 12
        return element;
    }

    /**
     * Discardable.
     *
     * https://html.spec.whatwg.org/multipage/parsing.html#insert-a-foreign-element
     */
    inline std::shared_ptr<DOM::Element>
    InsertForeignElement(ParserContext *parserContext,
                         const TagToken *token,
                         const Unicode::UString &givenNamespace) {
        // step 1
        Position adjustedInsertionLocation = FindAppropriatePlaceForInsertingNode(parserContext);

        // step 2
        std::shared_ptr<DOM::Element> element = CreateElementForToken(parserContext, token, givenNamespace, adjustedInsertionLocation.node);

        // step 3
        // TODO if it is possible to insert? why wouldn't it be?

        // step 3.1
        // TODO custom element reactions

        // step 3.2
        adjustedInsertionLocation.node->childNodes.insert(adjustedInsertionLocation.iterator, element);

        // step 3.3
        // TODO custom element reactions

        // step 4
        parserContext->stackOfOpenElements.push_back(element);

        // step 5
        return element;
    }

    /**
     * Discardable.
     *
     * https://html.spec.whatwg.org/multipage/parsing.html#insert-an-html-element
     */
    inline std::shared_ptr<HTMLElement>
    InsertHTMLElement(ParserContext *parserContext, const TagToken *token) {
        return std::static_pointer_cast<HTMLElement>(
                InsertForeignElement(parserContext, token, Unicode::UString(Text::Namespaces::HTML))
        );
    }

    inline std::shared_ptr<DOM::Node>
    GetNodeBeforeInsertionLocation(Position position) {
        if (position.node->childNodes.empty())
            return {nullptr};

        return *(position.iterator - 1);
    }

    /**
     * Algorithm for inserting a character.
     *
     * https://html.spec.whatwg.org/multipage/parsing.html#insert-a-character
     */
    inline void
    InsertCharacter(ParserContext *parserContext,
                    const CharacterToken *token) {
        // step 2
        Position position = FindAppropriatePlaceForInsertingNode(parserContext);

        // step 3
        if (position.node->nodeType == DOM::NodeType::DOCUMENT_NODE) {
            // Document may not have text node children
            return;
        }

        // step 4
        std::shared_ptr<DOM::Node> nodeBeforeInsertionLocation = GetNodeBeforeInsertionLocation(position);
        if (nodeBeforeInsertionLocation && nodeBeforeInsertionLocation->nodeType == DOM::NodeType::TEXT_NODE) {
            std::shared_ptr<DOM::Text> textNode = std::static_pointer_cast<DOM::Text>(nodeBeforeInsertionLocation);
            textNode->data += token->data;
            return;
        }

        std::shared_ptr<DOM::Text> textNode = std::make_shared<DOM::Text>(Unicode::UString{token->data});
        position.node->childNodes.insert(position.iterator, textNode);
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#generic-rcdata-element-parsing-algorithm
     */
    inline void
    FollowGenericRCDATAAlgorithm(ParserContext *parserContext,
                                 const TagToken *token) {
        // step 1
        InsertHTMLElement(parserContext, token);

        // step 2
        parserContext->tokenizerContext->SwitchToState(TokenizerStateName::RCDATA);

        // step 3
        parserContext->originalInsertionMode = parserContext->currentInsertionMode;

        // step 3
        parserContext->SwitchInsertionModeTo(InsertionModeName::TEXT);
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#generic-raw-text-element-parsing-algorithm
     */
    inline void
    FollowGenericRawTextAlgorithm(ParserContext *parserContext,
                                  const TagToken *token) {
        // step 1
        InsertHTMLElement(parserContext, token);

        // step 2
        parserContext->tokenizerContext->SwitchToState(TokenizerStateName::RAWTEXT);

        // step 3
        parserContext->originalInsertionMode = parserContext->currentInsertionMode;

        // step 3
        parserContext->SwitchInsertionModeTo(InsertionModeName::TEXT);
    }

    /**
     * Non-normative algorithm for handling script tags. Essentially the script
     * section of the "in head" insertion mode.
     *
     * https://html.spec.whatwg.org/multipage/parsing.html#scriptTag
     */
    inline void
    InsertScriptElement(ParserContext *parserContext, const TagToken *token) {
        // step 1
        Position adjustedInsertionLocation = FindAppropriatePlaceForInsertingNode(parserContext);

        // step 2
        std::shared_ptr<DOM::Element> element = CreateElementForToken(
                parserContext, token, Unicode::UString(::Text::Namespaces::HTML), adjustedInsertionLocation.node
        );

        // step 3
        // TODO set non-blocking flag (create Script element first)

        // step 4
        // TODO set fragment parser things if applicable

        // step 5
        // TODO set document.write() things if applicable

        // step 6
        adjustedInsertionLocation.node->childNodes.insert(adjustedInsertionLocation.iterator, element);

        // step 7
        parserContext->stackOfOpenElements.push_back(element);

        // step 8
        parserContext->tokenizerContext->SwitchToState(TokenizerStateName::SCRIPT_DATA);

        // step 9
        parserContext->originalInsertionMode = parserContext->currentInsertionMode;

        // step 10
        parserContext->SwitchInsertionModeTo(InsertionModeName::TEXT);
    }

    [[nodiscard]] inline bool
    IsEndTagTokenWithName(const Token *token, const char *name) {
        return token->type == TokenType::END_TAG &&
                // we could use the ignore-case variant, but since the tokenizer
                // explicitly lower cases all ASCII upper alpha characters, it
                // is redundant (TEST-POINT).
                Unicode::FastUStrings::EqualsASCII(
                        static_cast<const TagToken *>(token)->name, name);
    }

    [[nodiscard]] inline bool
    ShouldTokenBeHandledInHead(const TagToken *tagToken) {
        return Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "base") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "basefont") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "bgsound") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "link") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "meta") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "noframes") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "script") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "style") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "template") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "title");
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#concept-parser-marker
     */
    [[nodiscard]] inline bool
    IsElementMarker(const std::shared_ptr<DOM::Element> &element) noexcept {
        return Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "applet") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "object") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "marquee") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "template") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "td") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "th") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "caption");
    }

    /**
     * E.g.
     * https://html.spec.whatwg.org/multipage/parsing.html#parsing-main-inbody
     * in: An end-of-file token
     */
    [[nodiscard]] inline bool
    IsEOFAbleElementInStackOfOpenElements(const std::shared_ptr<DOM::Element> &element) noexcept {
        return Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "dd") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "dt") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "li") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "optgroup") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "option") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "p") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "rb") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "rp") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "rt") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "rtc") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "tbody") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "td") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "tfoot") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "th") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "thead") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "tr") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "body") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(element->localName, "html");
    }

    inline void
    ReconstructActiveFormattingElements(ParserContext *parserContext) {
        if (std::empty(parserContext->listOfActiveFormattingElements))
            return;

        auto &lastFormattingElement = parserContext->listOfActiveFormattingElements.back();
        if (lastFormattingElement.isMarker)
            return;

        if (std::find_if(std::cbegin(parserContext->stackOfOpenElements),
                std::cend(parserContext->stackOfOpenElements),
                [lastFormattingElement](const auto &element) {
                    return element == lastFormattingElement.element;
                }
        ) != std::cend(parserContext->stackOfOpenElements)) {
            return;
        }

        auto entry = std::end(parserContext->listOfActiveFormattingElements) - 1;

        rewind:
        if (parserContext->stackOfOpenElements.front() == entry->element)
            goto create;

        --entry;

        if (entry == std::begin(parserContext->listOfActiveFormattingElements))
            goto rewind;

        advance:
        ++entry;

        create:
        auto newElement = InsertHTMLElement(parserContext, entry->tokenWhichCreatedMe);
        entry->element = newElement;

        if (entry != std::end(parserContext->listOfActiveFormattingElements) - 1) {
            goto advance;
        }
    }

    /**
     * Matches for h1, h2, h3, h4, h5 & h6.
     */
    [[nodiscard]] inline bool
    IsHeadingToken(const Unicode::UString name) noexcept {
        return name.length() == 2 &&
               (name[0] == Unicode::LATIN_SMALL_LETTER_H || name[0] == Unicode::LATIN_CAPITAL_LETTER_H) &&
                name[1] >= Unicode::DIGIT_ONE &&
                name[1] <= Unicode::DIGIT_SIX;
    }

    /**
     * TODO intent of this function and usage guidelines are unclear by the name of the function.
     */
    [[nodiscard]] inline bool
    IsBlockElement(const TagToken *tagToken) noexcept {
        return Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "address") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "article") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "aside") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "blockquote") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "center") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "details") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "dialog") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "dir") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "div") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "dl") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "fieldset") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "figcaption") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "figure") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "footer") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "header") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "hgroup") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "main") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "menu") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "nav") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "ol") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "p") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "section") ||
               Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "summary");
    }

    template <std::size_t Size>
    [[nodiscard]] inline bool
    IsUStringAnyOf(const std::array<const char *, Size> &array, const Unicode::UString &unicodeString) noexcept {
        return std::find_if(std::cbegin(array), std::cend(array), [unicodeString](const char *arrayString) {
            return Unicode::FastUStrings::EqualsASCIIIgnoreCase(unicodeString, arrayString);
        });
    }

    [[nodiscard]] inline bool
    IsSpecialElementInStackOfOpenElements(const DOM::Element *element) {
        static const constexpr std::array specialHTML {
                "address", "applet", "area", "article", "aside", "base",
                "basefont", "bgsound", "blockquote", "body", "br", "button",
                "caption", "center", "col", "colgroup", "dd", "details", "dir",
                "div", "dl", "dt", "embed", "fieldset", "figcaption", "figure",
                "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4",
                "h5", "h6", "head", "header", "hgroup", "hr", "html", "iframe",
                "img", "input", "keygen", "li", "link", "listing", "main",
                "marquee", "menu", "meta", "nav", "noembed", "noframes",
                "noscript", "object", "ol", "p", "param", "plaintext", "pre",
                "script", "section", "select", "source", "style", "summary",
                "table", "tbody", "td", "template", "textarea", "tfoot", "th",
                "thead", "title", "tr", "track", "ul", "wbr", "xmp"
        };
        static const constexpr std::array specialMathML {
            "mi", "mo", "mn", "ms", "mtext", "annotation-xml"
        };
        static const constexpr std::array specialSVG {
            "foreignObject", "desc", "title"
        };

        if (Unicode::FastUStrings::EqualsASCII(element->namespaceValue, Text::Namespaces::HTML))
            return IsUStringAnyOf(specialHTML, element->localName);

        if (Unicode::FastUStrings::EqualsASCII(element->namespaceValue, Text::Namespaces::MathML))
            return IsUStringAnyOf(specialMathML, element->localName);

        if (Unicode::FastUStrings::EqualsASCII(element->namespaceValue, Text::Namespaces::SVG))
            return IsUStringAnyOf(specialSVG, element->localName);

        return false;
    }

} // namespace HTML::InsertionHelpers
