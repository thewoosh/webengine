/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * https://html.spec.whatwg.org/multipage/parsing.html#tokenization
 */

#pragma once

#include <optional>

#include "Source/Base/Terminate.hpp"
#include "Source/Text/UString.hpp"

namespace HTML {

    /**
     * A token attribute is a struct which represents a name and value pair.
     *
     * The attribute is recognized as such:
     * <a href="home.html">Homepage</a>
     *    ^^^^ ^^^^^^^^^^
     *    name    value
     */
    struct TokenAttribute {
        Unicode::UString name;
        Unicode::UString value;

        TokenAttribute() = default;
        TokenAttribute(TokenAttribute &&) = default;
        TokenAttribute(const TokenAttribute &) = default;

        TokenAttribute &operator=(TokenAttribute &&) = default;
        TokenAttribute &operator=(const TokenAttribute &) = default;

        [[nodiscard]] inline explicit
        TokenAttribute(Unicode::CodePoint firstCharacterOfName) noexcept
                : name(firstCharacterOfName)
                , value() {
        }

        [[nodiscard]] inline
        TokenAttribute(const Unicode::UString &name,
                       const Unicode::UString &value) noexcept
                : name(name)
                , value(value) {
        }

        [[nodiscard]] inline
        TokenAttribute(Unicode::UString &&name, Unicode::UString &&value) noexcept
                : name(std::move(name))
                , value(std::move(value)) {
        }
    };

    using TokenAttributeList = std::vector<TokenAttribute>;

    enum class TokenType {
        DOCTYPE,
        START_TAG,
        END_TAG,
        COMMENT,
        CHARACTER,
        END_OF_FILE
    };

    [[nodiscard]] inline const char *
    stringOfTokenType(TokenType type) {
        switch (type) {
            case TokenType::DOCTYPE: return "doctype";
            case TokenType::START_TAG: return "start-tag";
            case TokenType::END_TAG: return "end-tag";
            case TokenType::COMMENT: return "comment";
            case TokenType::CHARACTER: return "character";
            case TokenType::END_OF_FILE: return "eof";
            default: SWITCH_DEFAULT_UNREACHABLE();
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, TokenType type) {
        return stream << stringOfTokenType(type);
    }

    struct Token {
        TokenType type;

        constexpr explicit
        Token(TokenType type) noexcept
            : type(type) {}
    };

    /**
     * The DOCTYPE token indicates the compatibility mode of the document. If
     * absent/invalid or an explicitly legacy variant, the document should be
     * treated as limited-quirks-mode or just quirks-mode. When this mode is
     * set, the browser will introduce some compatibility features to aid the
     * rendering of the website.
     *
     * Further reading:
     * https://html.spec.whatwg.org/multipage/syntax.html#the-doctype
     * https://en.wikipedia.org/wiki/Document_type_declaration
     * https://html.spec.whatwg.org/multipage/parsing.html#the-initial-insertion-mode
     */
    struct DoctypeToken : public Token {

        /**
         * The name of the DOCTYPE should in all cases be 'html', otherwise
         * there is a parse error.
         *
         * The name of a DOCTYPE is extracted as such:
         * <!doctype html>
         *           ^^^^
         *
         * The name of a document may differ in different DTDs, but such
         * mechanisms are barely used anymore (afaik), and is unsupported by the
         * modern HTML specification.
         *
         * Further reading:
         * https://html.spec.whatwg.org/multipage/parsing.html#doctype-name-state
         */
        std::optional<Unicode::UString> name{std::nullopt};

        /**
         * NOTE: The following mechanism is unclear to me, and isn't widely used
         * or documented anymore, so take the following section with a grain of
         * salt.
         *
         * The public identifier is a string containing the URL of a Document
         * Type Definition.
         *
         * Further reading:
         * https://en.wikipedia.org/wiki/Document_type_declaration
         * https://en.wikipedia.org/wiki/Formal_Public_Identifier
         */
        std::optional<Unicode::UString> publicIdentifier{std::nullopt};

        /**
         * TODO Document the behavior and semantics of the system identifier.
         *      Problem is that I don't have a clue what it is or what it does
         *      or anything.
         */
        std::optional<Unicode::UString> systemIdentifier{std::nullopt};

        /**
         * force-quirks is a flag indicating whether or not the DOCTYPE token
         * hints to us that the browser should treat the page with quirks
         * enabled.
         *
         * Default is off (=false)
         *
         * Further reading:
         * https://html.spec.whatwg.org/multipage/parsing.html#force-quirks-flag
         * https://dom.spec.whatwg.org/#concept-document-quirks
         * https://developer.mozilla.org/en-US/docs/Web/HTML/Quirks_Mode_and_Standards_Mode
         */
        bool forceQuirks{false};

        DoctypeToken() : Token(TokenType::DOCTYPE) {};

        explicit DoctypeToken(bool forceQuirks)
            : Token(TokenType::DOCTYPE), forceQuirks(forceQuirks) {}
    };

    /**
     * The TagToken is a struct that can either represent a start tag token or
     * an end tag token, because these types contain the same members.
     *
     * A tag token is either a:
     * - start token, e.g. <div>
     * - end token,   e.g. </div>
     *
     * Further reading:
     * https://html.spec.whatwg.org/multipage/parsing.html#tokenization
     */
    struct TagToken : public Token {

        /**
         * The name of a tag token is the name of the element it represents;
         * often called the element type.
         *
         * The name of a tag token is extracted as such:
         * <div class="blue-container">
         *  ^^^
         *
         * </div>
         *   ^^^
         */
        Unicode::UString name;

        /**
         * Self-closing tags don't officially exist in HTML - they are the
         * result of XML in HTML - but are recognized and handled by the HTML
         * specification.
         *
         * Only void elements are allowed to be self-closing, otherwise a parse
         * error is emitted:
         * Valid: <link href="styles.css" rel="stylesheet" />
         * Invalid: <div class="footer-parent" />
         *
         * Further reading:
         * https://html.spec.whatwg.org/multipage/parsing.html#self-closing-flag
         * https://html.spec.whatwg.org/multipage/parsing.html#parse-error-non-void-html-element-start-tag-with-trailing-solidus
         */
        bool selfClosing{false};

        /**
         * The list of attributes contains name-and-value-pairs. These pairs
         * convey the semantics of an HTML element, e.g.:
         * - Providing the source of a subresource.
         * - Providing the destination of a hyperlink.
         * - Signalling the language of the document or it's subresources.
         * ...
         *
         * The format of attributes comes in the following four forms:
         * - <a href>
         * - <a href=test>
         * - <a href="test">
         * - <a href='test'>
         *
         * Further reading:
         * https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
         */
        TokenAttributeList attributes{};

        TagToken(TokenType type, const Unicode::UString &name) : Token(type), name(name) {}
        TagToken(TokenType type, Unicode::UString &&name) : Token(type), name(std::move(name)) {}

    };

    /**
     * The writer of an HTML element can insert comments into the document,
     * often abstracting a section of the document or declaring intellectual
     * property rights.
     *
     * Further reading:
     * https://html.spec.whatwg.org/multipage/syntax.html#comments
     */
     struct CommentToken : public Token {

         /**
          * The comment's data is the actual text inside a comment.
          *
          * The data is recognized as such:
          * <!-- Hello World! -->
          *     ^^^^^^^^^^^^^^
          *
          * Note the inclusion of the leading and trailing spaces.
          */
         Unicode::UString data{};

         CommentToken() : Token(TokenType::COMMENT) {}

         explicit
         CommentToken(const Unicode::UString &string) noexcept
            : Token(TokenType::COMMENT), data(string) {}

         explicit
         CommentToken(Unicode::UString &&string) noexcept
                 : Token(TokenType::COMMENT), data(std::move(string)) {}

     };

     /**
      * A character token is emitted for any character that isn't recognized by
      * any other state. This includes whitespace and just text (but not textual
      * data inside other tokens, though).
      */
     struct CharacterToken : public Token {

         Unicode::CodePoint data{};

         constexpr explicit
         CharacterToken(Unicode::CodePoint character) noexcept
            : Token(TokenType::CHARACTER), data(character) {}

     };

} // namespace HTML
