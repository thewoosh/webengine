/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <functional>

#include "Source/HTML/InsertionModeName.hpp"
#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/ParserContext.hpp"

namespace HTML {

    struct TokenizerContext;

    /**
     *
     */
    class TreeConstructor {
    public:
        TreeConstructor(TokenizerContext *context) :
            parserContext{context, this}
        {}

        /**
         * Dispatch tree constructor because this new token was emitted.
         */
        void DispatchEmit(const Token &);

        void InvokeStateImplementation(InsertionModeName state, const Token &);

    private:
        ParserContext parserContext;

    public:
        struct ImplementationMap {
            using FunctionType = std::function<void(ParserContext &, const Token &)>;

            std::array<FunctionType, static_cast<std::size_t>(InsertionModeName::AFTER_AFTER_FRAMESET)>
                    lookup{};

            ImplementationMap() noexcept;

            [[nodiscard]] constexpr FunctionType &
            byName(InsertionModeName stateName) {
                return lookup[static_cast<std::size_t>(stateName)];
            }

        private:
            void
            add(InsertionModeName name, const FunctionType &function) {
                lookup[static_cast<std::size_t>(name)] = function;
            }
        };

        ImplementationMap implementationMap{};

        [[nodiscard]] inline constexpr ParserContext &
        Context() noexcept {
            return parserContext;
        }

        [[nodiscard]] inline constexpr const ParserContext &
        Context() const noexcept {
            return parserContext;
        }
    };

} // namespace HTML
