/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Index.hpp"

#include "Source/HTML/TokenizerContext.hpp"

#include "Source/HTML/InsertionHelpers.hpp"
#include "Source/HTML/InsertionModes/Index.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace HTML::InsertionModeImplementations {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#parsing-main-incdata
     */
    void
    TextMode(ParserContext &parserContext, const Token &token) {
        if (token.type == TokenType::CHARACTER) {
            InsertionHelpers::InsertCharacter(&parserContext, static_cast<const CharacterToken *>(&token));
            return;
        }

        if (token.type == TokenType::END_OF_FILE) {
            PARSE_ERROR(ParseError::EOF_IN_TEXT_MODE);
            return;
        }

        if (InsertionHelpers::IsEndTagTokenWithName(&token, "script")) {
            //
            // TODO
            // handle scripts
            //
            return;
        }

        if (token.type == TokenType::END_TAG) {
            parserContext.PopCurrentNodeOffStack();

            parserContext.SwitchInsertionModeTo(parserContext.originalInsertionMode);

            // reset the original insertion mode:
            parserContext.originalInsertionMode = InsertionModeName::INITIAL;
            return;
        }

        //
        // The specification implicitly states that tokens that don't match any
        // of the conditions above should be ignored, since the specification
        // doesn't contain i
        //
        // I am uncertain about this, though? We might need to stringify the
        // token so that more-or-less represents the characters consumed by the
        // tokenizer and reprocess them as characters of the text node?
        // TODO.
        //
    }

} // namespace HTML::InsertionModeImplementations
