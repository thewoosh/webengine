/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"

#include "Source/HTML/InsertionHelpers.hpp"
#include "Source/HTML/InsertionModes/Index.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace HTML::InsertionModeImplementations {

    constexpr std::array quirksPublicIdentifier = {
           "-//W3C//DTD W3 HTML Strict 3.0//EN//",
           "-/W3C/DTD HTML 4.0 Transitional/EN",
           "HTML"
    };

    constexpr std::array quirksPublicIdentifiersPrefix = {
            "+//Silmaril//dtd html Pro v0r11 19970101//",
            "-//AS//DTD HTML 3.0 asWedit + extensions//",
            "-//AdvaSoft Ltd//DTD HTML 3.0 asWedit + extensions//",
            "-//IETF//DTD HTML 2.0 Level 1//",
            "-//IETF//DTD HTML 2.0 Level 2//",
            "-//IETF//DTD HTML 2.0 Strict Level 1//",
            "-//IETF//DTD HTML 2.0 Strict Level 2//",
            "-//IETF//DTD HTML 2.0 Strict//",
            "-//IETF//DTD HTML 2.0//",
            "-//IETF//DTD HTML 2.1E//",
            "-//IETF//DTD HTML 3.0//",
            "-//IETF//DTD HTML 3.2 Final//",
            "-//IETF//DTD HTML 3.2//",
            "-//IETF//DTD HTML 3//",
            "-//IETF//DTD HTML Level 0//",
            "-//IETF//DTD HTML Level 1//",
            "-//IETF//DTD HTML Level 2//",
            "-//IETF//DTD HTML Level 3//",
            "-//IETF//DTD HTML Strict Level 0//",
            "-//IETF//DTD HTML Strict Level 1//",
            "-//IETF//DTD HTML Strict Level 2//",
            "-//IETF//DTD HTML Strict Level 3//",
            "-//IETF//DTD HTML Strict//",
            "-//IETF//DTD HTML//",
            "-//Metrius//DTD Metrius Presentational//",
            "-//Microsoft//DTD Internet Explorer 2.0 HTML Strict//",
            "-//Microsoft//DTD Internet Explorer 2.0 HTML//",
            "-//Microsoft//DTD Internet Explorer 2.0 Tables//",
            "-//Microsoft//DTD Internet Explorer 3.0 HTML Strict//",
            "-//Microsoft//DTD Internet Explorer 3.0 HTML//",
            "-//Microsoft//DTD Internet Explorer 3.0 Tables//",
            "-//Netscape Comm. Corp.//DTD HTML//",
            "-//Netscape Comm. Corp.//DTD Strict HTML//",
            "-//O'Reilly and Associates//DTD HTML 2.0//",
            "-//O'Reilly and Associates//DTD HTML Extended 1.0//",
            "-//O'Reilly and Associates//DTD HTML Extended Relaxed 1.0//",
            "-//SQ//DTD HTML 2.0 HoTMetaL + extensions//",
            "-//SoftQuad Software//DTD HoTMetaL PRO 6.0::19990601::extensions to HTML 4.0//",
            "-//SoftQuad//DTD HoTMetaL PRO 4.0::19971010::extensions to HTML 4.0//",
            "-//Spyglass//DTD HTML 2.0 Extended//",
            "-//Sun Microsystems Corp.//DTD HotJava HTML//",
            "-//Sun Microsystems Corp.//DTD HotJava Strict HTML//",
            "-//W3C//DTD HTML 3 1995-03-24//",
            "-//W3C//DTD HTML 3.2 Draft//",
            "-//W3C//DTD HTML 3.2 Final//",
            "-//W3C//DTD HTML 3.2//",
            "-//W3C//DTD HTML 3.2S Draft//",
            "-//W3C//DTD HTML 4.0 Frameset//",
            "-//W3C//DTD HTML 4.0 Transitional//",
            "-//W3C//DTD HTML Experimental 19960712//",
            "-//W3C//DTD HTML Experimental 970421//",
            "-//W3C//DTD W3 HTML//",
            "-//W3O//DTD W3 HTML 3.0//",
            "-//WebTechs//DTD Mozilla HTML 2.0//",
            "-//WebTechs//DTD Mozilla HTML//"
    };

    [[nodiscard]] static inline DOM::Concept::QuirksMode
    DetermineQuirksMode(const DoctypeToken *token) {
        if (token->forceQuirks) return DOM::Concept::QuirksMode::QUIRKS;
        if (!token->name.has_value() || !Unicode::FastUStrings::EqualsASCIIIgnoreCase(token->name.value(), "html")) return DOM::Concept::QuirksMode::QUIRKS;

        if (token->systemIdentifier.has_value() &&
                Unicode::FastUStrings::EqualsASCIIIgnoreCase(token->systemIdentifier.value(),
                                                             "http://www.ibm.com/data/dtd/v11/ibmxhtml1-transitional.dtd")) {
            return DOM::Concept::QuirksMode::QUIRKS;
        }

        if (token->publicIdentifier.has_value()) {
            const Unicode::UString publicId = token->publicIdentifier.value();

            for (const char *string : quirksPublicIdentifier) {
                if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(publicId, string)) {
                    return DOM::Concept::QuirksMode::QUIRKS;
                }
            }

            for (const char *string : quirksPublicIdentifiersPrefix) {
                if (Unicode::FastUStrings::StartsWithASCIIIgnoreCase(publicId, string)) {
                    return DOM::Concept::QuirksMode::QUIRKS;
                }
            }
        }

        if (token->publicIdentifier.has_value() &&
            (Unicode::FastUStrings::StartsWithASCIIIgnoreCase(token->publicIdentifier.value(),
                                                              "-//W3C//DTD XHTML 1.0 Frameset//") ||
             Unicode::FastUStrings::StartsWithASCIIIgnoreCase(token->publicIdentifier.value(),
                                                                     "-//W3C//DTD XHTML 1.0 Transitional//"))) {
            return DOM::Concept::QuirksMode::LIMITED_QUIRKS;
        }


        if (token->publicIdentifier.has_value() &&
            (Unicode::FastUStrings::StartsWithASCIIIgnoreCase(token->publicIdentifier.value(),
                                                              "-//W3C//DTD HTML 4.01 Frameset//") ||
             Unicode::FastUStrings::StartsWithASCIIIgnoreCase(token->publicIdentifier.value(),
                                                                     "-//W3C//DTD HTML 4.01 Transitional//"))) {
            return token->systemIdentifier.has_value() ?
                DOM::Concept::QuirksMode::LIMITED_QUIRKS :
                DOM::Concept::QuirksMode::QUIRKS;
        }

        return DOM::Concept::QuirksMode::NO_QUIRKS;
    }

    static inline void
    ExamineDoctypeToken(ParserContext &parserContext, const DoctypeToken *token) {
        if (!token->name.has_value() ||
            !Unicode::FastUStrings::EqualsASCIIIgnoreCase(token->name.value(), "html")
            || token->publicIdentifier.has_value()
            || (token->systemIdentifier.has_value() &&
                 !Unicode::FastUStrings::EqualsASCIIIgnoreCase(token->systemIdentifier.value(), "html"))) {
            PARSE_ERROR(ParseError::DOCTYPE_NOT_HTML_LIKE);
        }

        if (parserContext.isSrcdocDocument) {
            parserContext.document->quirksMode = DOM::Concept::QuirksMode::NO_QUIRKS;
        } else {
            parserContext.document->quirksMode = DetermineQuirksMode(token);
        }

        auto doctype = InsertionHelpers::InsertDocumentType(
                &parserContext,
                token->name.value(),
                token->publicIdentifier ? &*token->publicIdentifier : nullptr,
                token->systemIdentifier ? &*token->systemIdentifier : nullptr
        );

        // associate the DocumentType node with the document
        parserContext.document->doctype = doctype;
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#data-state
     */
    void
    InitialMode(ParserContext &parserContext, const Token &token) {
        if (InsertionHelpers::IsWhitespaceToken(&token)) {
            // Ignore the token.
        } else if (token.type == TokenType::COMMENT) {
            InsertionHelpers::InsertComment(
                    &parserContext,
                    reinterpret_cast<const CommentToken *>(&token)->data,
                    InsertionHelpers::Position{ parserContext.document, parserContext.document->childNodes.cend()}
            );
        } else if (token.type == TokenType::DOCTYPE) {
            ExamineDoctypeToken(parserContext, reinterpret_cast<const DoctypeToken *>(&token));

            parserContext.SwitchInsertionModeTo(InsertionModeName::BEFORE_HTML);
        } else {
            if (!parserContext.isSrcdocDocument) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
            }

            parserContext.SwitchInsertionModeTo(InsertionModeName::BEFORE_HTML);
            parserContext.treeConstructor->InvokeStateImplementation(InsertionModeName::BEFORE_HTML, token);
        }
    }

} // namespace HTML::InsertionModeImplementations
