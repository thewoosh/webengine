/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Index.hpp"

#include "Source/HTML/TokenizerContext.hpp"

#include "Source/HTML/InsertionHelpers.hpp"
#include "Source/HTML/InsertionModes/Index.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace HTML::InsertionModeImplementations {

    static void
    RemoveHeadElementFromOpenElementsStack(ParserContext &parserContext) {
        bool flag{true};

        while (flag) {
            if (parserContext.stackOfOpenElements.back() == parserContext.headElementPointer) {
                flag = false;
            }

            parserContext.stackOfOpenElements.pop_back();
        }
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#the-after-head-insertion-mode
     */
    void
    AfterHeadMode(ParserContext &parserContext, const Token &token) {
        if (InsertionHelpers::IsWhitespaceToken(&token)) {
            InsertionHelpers::InsertCharacter(&parserContext, static_cast<const CharacterToken *>(&token));
            return;
        }

        if (token.type == TokenType::COMMENT) {
            InsertionHelpers::InsertComment(
                    &parserContext,
                    static_cast<const CommentToken *>(&token)->data
            );
            return;
        }

        if (token.type == TokenType::DOCTYPE) {
            PARSE_ERROR(ParseError::DOCTYPE_INCORRECTLY_PLACED);
            // Ignore the token.
            return;
        }

        if (token.type == TokenType::START_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "html")) {
                parserContext.ProcessUsingOtherRules(InsertionModeName::IN_BODY, token);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "body")) {
                auto bodyElement = InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                parserContext.SwitchInsertionModeTo(InsertionModeName::IN_BODY);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "frameset")) {
                InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                parserContext.SwitchInsertionModeTo(InsertionModeName::IN_FRAMESET);
                return;
            }
            if (InsertionHelpers::ShouldTokenBeHandledInHead(tagToken)) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                parserContext.stackOfOpenElements.emplace_back(parserContext.headElementPointer);
                parserContext.ProcessUsingOtherRules(InsertionModeName::IN_HEAD, token);
                RemoveHeadElementFromOpenElementsStack(parserContext);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "head")) {
                PARSE_ERROR(ParseError::MULTIPLE_HEAD_TOKENS);
                // Ignore the token
                return;
            }
        }

        if (token.type == TokenType::END_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "template")) {
                parserContext.ProcessUsingOtherRules(InsertionModeName::IN_HEAD, token);
                return;
            }

            if (!Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "body") &&
                    !Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "br") &&
                    !Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "html")) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                // Ignore the token.
                return;
            }
        }

        TagToken emptyBodyStartTagToken{
            TokenType::START_TAG,
            Unicode::UString("body")
        };

        auto bodyElement = InsertionHelpers::InsertHTMLElement(&parserContext, &emptyBodyStartTagToken);
        parserContext.SwitchInsertionModeTo(InsertionModeName::IN_BODY);

        parserContext.ProcessUsingOtherRules(InsertionModeName::IN_BODY, token);
    }

} // namespace HTML::InsertionModeImplementations
