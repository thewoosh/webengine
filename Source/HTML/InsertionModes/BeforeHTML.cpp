/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Index.hpp"

#include "Source/HTML/TokenizerContext.hpp"

#include "Source/HTML/InsertionHelpers.hpp"
#include "Source/HTML/InsertionModes/Index.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace HTML::InsertionModeImplementations {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#the-before-html-insertion-mode
     */
    void
    BeforeHTMLMode(ParserContext &parserContext, const Token &token) {
        if (token.type == TokenType::DOCTYPE) {
            PARSE_ERROR(ParseError::DOCTYPE_INCORRECTLY_PLACED);
            // Ignore the token.
            return;
        }

        if (token.type == TokenType::COMMENT) {
            InsertionHelpers::InsertComment(
                    &parserContext,
                    static_cast<const CommentToken *>(&token)->data,
                    InsertionHelpers::Position{ parserContext.document, parserContext.document->childNodes.cend()}
            );
            return;
        }

        if (InsertionHelpers::IsWhitespaceToken(&token)) {
            // Ignore the token.
            return;
        }

        if (token.type == TokenType::START_TAG &&
            Unicode::FastUStrings::EqualsASCIIIgnoreCase(static_cast<const TagToken *>(&token)->name, "html")) {
            std::shared_ptr<DOM::Element> element = InsertionHelpers::CreateElementForToken(
                    &parserContext,
                    static_cast<const TagToken *>(&token),
                    Unicode::UString(Text::Namespaces::HTML),
                    parserContext.document
            );

            parserContext.document->append(element);
            parserContext.stackOfOpenElements.push_back(element);
            parserContext.SwitchInsertionModeTo(InsertionModeName::BEFORE_HEAD);
            return;
        }

        if (token.type == TokenType::END_TAG) {
            const Unicode::UString &name = static_cast<const TagToken *>(&token)->name;
            if (!Unicode::FastUStrings::EqualsASCIIIgnoreCase(name, "head") &&
                !Unicode::FastUStrings::EqualsASCIIIgnoreCase(name, "body") &&
                !Unicode::FastUStrings::EqualsASCIIIgnoreCase(name, "html") &&
                !Unicode::FastUStrings::EqualsASCIIIgnoreCase(name, "br")) {
                PARSE_ERROR(ParseError::UNEXPECTED_END_TAG_TOKEN);
                // Ignore the token.
                return;
            }
            // otherwise act as described in the anything else below:
        }

        std::shared_ptr<HTML::HTMLHtmlElement> element = std::make_shared<HTML::HTMLHtmlElement>();
        parserContext.document->append(element);
        parserContext.stackOfOpenElements.push_back(element);

        parserContext.SwitchInsertionModeTo(InsertionModeName::BEFORE_HEAD);
        parserContext.ReprocessToken(token);
    }

} // namespace HTML::InsertionModeImplementations
