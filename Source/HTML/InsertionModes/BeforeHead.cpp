/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Index.hpp"

#include "Source/HTML/TokenizerContext.hpp"

#include "Source/HTML/InsertionHelpers.hpp"
#include "Source/HTML/InsertionModes/Index.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace HTML::InsertionModeImplementations {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#the-before-head-insertion-mode
     */
    void
    BeforeHeadMode(ParserContext &parserContext, const Token &token) {
        if (InsertionHelpers::IsWhitespaceToken(&token)) {
            // Ignore the token.
            return;
        }

        if (token.type == TokenType::COMMENT) {
            InsertionHelpers::InsertComment(
                    &parserContext,
                    static_cast<const CommentToken *>(&token)->data
            );
            return;
        }

        if (token.type == TokenType::DOCTYPE) {
            PARSE_ERROR(ParseError::DOCTYPE_INCORRECTLY_PLACED);
            // Ignore the token.
            return;
        }

        if (token.type == TokenType::START_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "html")) {
                parserContext.ProcessUsingOtherRules(InsertionModeName::IN_BODY, token);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "head")) {
                auto headElement = InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                parserContext.headElementPointer = std::static_pointer_cast<HTMLHeadElement>(headElement);
                parserContext.SwitchInsertionModeTo(InsertionModeName::IN_HEAD);
                return;
            }
        }

        if (token.type == TokenType::END_TAG) {
            const Unicode::UString &name = static_cast<const TagToken *>(&token)->name;
            if (!Unicode::FastUStrings::EqualsASCIIIgnoreCase(name, "head") &&
                !Unicode::FastUStrings::EqualsASCIIIgnoreCase(name, "body") &&
                !Unicode::FastUStrings::EqualsASCIIIgnoreCase(name, "html") &&
                !Unicode::FastUStrings::EqualsASCIIIgnoreCase(name, "br")) {
                PARSE_ERROR(ParseError::UNEXPECTED_END_TAG_TOKEN);
                // Ignore the token.
                return;
            }
            // otherwise act as described in the anything else below:
        }

        TagToken emptyHeadStartTagToken{
            TokenType::START_TAG,
            Unicode::UString("head")
        };

        auto headElement = InsertionHelpers::InsertHTMLElement(&parserContext, &emptyHeadStartTagToken);
        parserContext.headElementPointer = std::static_pointer_cast<HTMLHeadElement>(headElement);
        parserContext.SwitchInsertionModeTo(InsertionModeName::IN_HEAD);

        parserContext.ReprocessToken(token);
    }

} // namespace HTML::InsertionModeImplementations
