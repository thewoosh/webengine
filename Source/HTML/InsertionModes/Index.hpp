/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/HTML/ParserContext.hpp"
#include "Source/HTML/Token.hpp"

/**
 * @see the TokenizerStates/Index.html for paradigm
 *
 * Sort the names by order in which the appear in the specification, not
 * alphabetically.
 */
namespace HTML::InsertionModeImplementations {

    void InitialMode(ParserContext &, const Token &);
    void BeforeHTMLMode(ParserContext &, const Token &);
    void BeforeHeadMode(ParserContext &, const Token &);
    void InHeadMode(ParserContext &, const Token &);
    void AfterHeadMode(ParserContext &, const Token &);
    void InBodyMode(ParserContext &, const Token &);

    void TextMode(ParserContext &, const Token &);

} // namespace HTML::InsertionModeImplementations
