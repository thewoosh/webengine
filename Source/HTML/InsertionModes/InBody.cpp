/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Index.hpp"

#include <sstream>

#include "Source/HTML/TokenizerContext.hpp"

#include "Source/HTML/InsertionHelpers.hpp"
#include "Source/HTML/InsertionModes/Index.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace HTML::InsertionModeImplementations {

    static void
    HandleCharacter(ParserContext &parserContext, const CharacterToken *token) {
        Unicode::CodePoint character = token->data;

        if (character == Unicode::NULL_CHARACTER) {
            PARSE_ERROR(ParseError::UNEXPECTED_NULL_CHARACTER);
            return;
        }

        InsertionHelpers::ReconstructActiveFormattingElements(&parserContext);
        InsertionHelpers::InsertCharacter(&parserContext, token);

        if (character != Unicode::CHARACTER_TABULATION &&
                character != Unicode::LINE_FEED &&
                character != Unicode::FORM_FEED &&
                character != Unicode::CARRIAGE_RETURN &&
                character != Unicode::SPACE) {
            parserContext.framesetOk = false;
        }
    }

    static void
    HandleFormClosure(ParserContext &parserContext) {
        bool inTemplate{parserContext.IsElementOfTypeOnStackOfOpenElements(DOM::CommonElementType::TEMPLATE)};

        if (!inTemplate) {
            std::shared_ptr<HTMLFormElement> node{nullptr};
            std::swap(node, parserContext.formElementPointer);

            if (node == nullptr /* or if node isn't in scope */) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                // Ignore the token.
                return;
            }

            // TODO generate implied end tags.

            if (parserContext.stackOfOpenElements.back() != node) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
            } else {
                parserContext.PopCurrentNodeOffStack();
            }
        } else {
            // TODO check if in scope

            // TODO generate implied end tags.


            if (parserContext.stackOfOpenElements.back()->commonElementType != DOM::CommonElementType::FORM) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
            }

            do {
                parserContext.PopCurrentNodeOffStack();
            } while (parserContext.stackOfOpenElements.back()->commonElementType != DOM::CommonElementType::FORM);
        }
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#parsing-main-inbody
     */
    void
    InBodyMode(ParserContext &parserContext, const Token &token) {
        if (token.type == TokenType::CHARACTER) {
            HandleCharacter(parserContext, static_cast<const CharacterToken *>(&token));
            return;
        }

        if (token.type == TokenType::COMMENT) {
            InsertionHelpers::InsertComment(
                    &parserContext,
                    static_cast<const CommentToken *>(&token)->data
            );
            return;
        }

        if (token.type == TokenType::DOCTYPE) {
            PARSE_ERROR(ParseError::DOCTYPE_INCORRECTLY_PLACED);
            // Ignore the token.
            return;
        }

        if (token.type == TokenType::END_OF_FILE) {
            if (!std::empty(parserContext.stackOfTemplateInsertionModes)) {
                parserContext.ProcessUsingOtherRules(InsertionModeName::IN_TEMPLATE, token);
                return;
            }

            for (const auto &element : parserContext.stackOfOpenElements) {
                if (InsertionHelpers::IsEOFAbleElementInStackOfOpenElements(element)) {
                    // TODO add better parse error enum
                    PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                    break;
                }
            }

            STOP_PARSING();
        }

        if (token.type == TokenType::END_OF_FILE) {
            if (!std::empty(parserContext.stackOfTemplateInsertionModes)) {
                parserContext.ProcessUsingOtherRules(InsertionModeName::IN_TEMPLATE, token);
                return;
            }

            for (const auto &element : parserContext.stackOfOpenElements) {
                if (InsertionHelpers::IsEOFAbleElementInStackOfOpenElements(element)) {
                    // TODO add better parse error enum
                    PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                    break;
                }
            }

            STOP_PARSING();
        }

        if (token.type == TokenType::START_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "html")) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);

                if (parserContext.IsElementOfTypeOnStackOfOpenElements(DOM::CommonElementType::TEMPLATE)) {
                    // Ignore token
                    return;
                }

                // copy attributes to the top element of the stack of open
                // elements. if the top element doesn't have an attribute with
                // the name of the attribute of the token, add the attribute to
                // the attribute list of the top element.
                auto &attribList = parserContext.stackOfOpenElements.front()->attributeList;
                for (const auto &attrib : tagToken->attributes) {
                    if (!attribList.contains(attrib.name)) {
                        attribList.emplace(std::move(attrib.name), std::move(attrib.value));
                    }
                }
                return;
            }

            if (InsertionHelpers::ShouldTokenBeHandledInHead(tagToken)) {
                parserContext.ProcessUsingOtherRules(InsertionModeName::IN_HEAD, token);
                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "body")) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);

                if (std::size(parserContext.stackOfOpenElements) == 1 ||
                        parserContext.stackOfOpenElements[1]->commonElementType == DOM::CommonElementType::BODY ||
                        parserContext.IsElementOfTypeOnStackOfOpenElements(DOM::CommonElementType::TEMPLATE)) {
                    // Ignore the token (fragment case).
                    // https://html.spec.whatwg.org/multipage/parsing.html#fragment-case
                    return;
                }

                parserContext.framesetOk = false;
                // same as with html
                auto &attribList = parserContext.stackOfOpenElements[1]->attributeList;
                for (const auto &attrib : tagToken->attributes) {
                    if (!attribList.contains(attrib.name)) {
                        attribList.emplace(std::move(attrib.name), std::move(attrib.value));
                    }
                }
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "frameset")) {
                PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);

                if (std::size(parserContext.stackOfOpenElements) == 1 ||
                    parserContext.stackOfOpenElements[1]->commonElementType == DOM::CommonElementType::BODY ||
                    parserContext.IsElementOfTypeOnStackOfOpenElements(DOM::CommonElementType::TEMPLATE)) {
                    // Ignore the token (fragment case).
                    // https://html.spec.whatwg.org/multipage/parsing.html#fragment-case
                    return;
                }

                if (!parserContext.framesetOk) {
                    // Ignore the token
                    return;
                }

                // TODO I don't understand the terminology here.
                PARSER_TODO("Terminology")
            }

            if (InsertionHelpers::IsBlockElement(tagToken)) {
                // TODO have p in button element scope of stack of open elements
                //      close a p element
                InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                return;
            }

            if (InsertionHelpers::IsHeadingToken(tagToken->name)) {
                // TODO have p in button element scope of stack of open elements
                //      close a p element

                if (InsertionHelpers::IsHeadingToken(parserContext.stackOfOpenElements.back()->localName)) {
                    PARSE_ERROR(ParseError::HEADING_TAGS_CANT_NEST);
                    parserContext.PopCurrentNodeOffStack();
                }

                InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "pre") ||
                Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "listing")) {
                PARSER_TODO("tech isn't there yet");
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "form")) {
                bool inTemplate{parserContext.IsElementOfTypeOnStackOfOpenElements(DOM::CommonElementType::TEMPLATE)};

                if (parserContext.formElementPointer != nullptr && !inTemplate) {
                    PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                    // Ignore the token.
                    return;
                }

                // TODO close a p element
                auto element = InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                if (!inTemplate) {
                    parserContext.formElementPointer = std::static_pointer_cast<HTMLFormElement>(element);
                }

                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "li")) {
                parserContext.framesetOk = false;

                PARSER_TODO("Not implemented");
//                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "dd") ||
                    Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "dt")) {
                PARSER_TODO("Not implemented");
//                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "plaintext")) {
                // TODO close a p element
                InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                parserContext.tokenizerContext->SwitchToState(TokenizerStateName::PLAINTEXT);
                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "button")) {
                PARSER_TODO("Not implemented");
            }
        }

        if (token.type == TokenType::END_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "body")) {
                // TODO: Check if in scope

                for (const auto &element : parserContext.stackOfOpenElements) {
                    if (InsertionHelpers::IsEOFAbleElementInStackOfOpenElements(element)) {
                        // TODO add better parse error enum
                        PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                        break;
                    }
                }

                parserContext.SwitchInsertionModeTo(InsertionModeName::AFTER_BODY);
                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "html")) {
                // TODO: Check if in scope

                for (const auto &element : parserContext.stackOfOpenElements) {
                    if (InsertionHelpers::IsEOFAbleElementInStackOfOpenElements(element)) {
                        // TODO add better parse error enum
                        PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                        break;
                    }
                }

                parserContext.SwitchInsertionModeTo(InsertionModeName::AFTER_BODY);
                parserContext.ReprocessToken(token);
                return;
            }

            if (InsertionHelpers::IsBlockElement(tagToken)) {
                bool found = false;
                for (auto it = std::crbegin(parserContext.stackOfOpenElements); it != std::crend(parserContext.stackOfOpenElements); ++it) {
                    const auto &element = **it;
                    if (Unicode::FastUStrings::EqualsASCII(element.namespaceValue, Text::Namespaces::HTML) &&
                            element.localName == tagToken->name) {
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    PARSE_ERROR(ParseError::UNEXPECTED_END_TAG_TOKEN);
                    return; // Ignore the token;
                }

                // TODO generate implied end tags
                auto &backElement = parserContext.stackOfOpenElements.back();
                if (Unicode::FastUStrings::EqualsASCII(backElement->namespaceValue, Text::Namespaces::HTML) &&
                        backElement->localName != tagToken->name) {
                    PARSE_ERROR(ParseError::IMPLIED_END_TAGS_IS_NOT_CURRENT_ELEMENT);
                }

                while (true) {
                    bool isElement = backElement->localName == tagToken->name &&
                            Unicode::FastUStrings::EqualsASCII(backElement->namespaceValue, Text::Namespaces::HTML);

                    parserContext.stackOfOpenElements.pop_back();

                    if (isElement) {
                        return;
                    }

                    backElement = parserContext.stackOfOpenElements.back();
                }
                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "form")) {
                HandleFormClosure(parserContext);
                return;
            }

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "p")) {
                // TODO check if in scope.

                // TODO close a p element
                PARSER_TODO("Close a p element");
//                return;
            }

            // INFO: Skipping a bunch of stuff here!

            if (InsertionHelpers::IsHeadingToken(tagToken->name)) {
                // TODO check if element is in scope

                // TODO generate implied end tags.

                if (parserContext.stackOfOpenElements.back()->localName != tagToken->name) {
                    PARSE_ERROR(ParseError::UNEXPECTED_TOKEN);
                }


                while (!(parserContext.stackOfOpenElements.back()->commonElementType == DOM::CommonElementType::UNCOMMON && // optimization
                         InsertionHelpers::IsHeadingToken(parserContext.stackOfOpenElements.back()->localName))) {
                    parserContext.PopCurrentNodeOffStack();
                }
                parserContext.PopCurrentNodeOffStack();
                return;
            }
        }

        if (token.type == TokenType::START_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);

            if (tagToken->name == Unicode::UString("area") || tagToken->name == Unicode::UString("br") ||
                tagToken->name == Unicode::UString("embed") || tagToken->name == Unicode::UString("img") ||
                tagToken->name == Unicode::UString("keygen") || tagToken->name == Unicode::UString("wbr")) {
                InsertionHelpers::ReconstructActiveFormattingElements(&parserContext);
                InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                parserContext.stackOfOpenElements.pop_back();
                // TODO acknowledge self-closing flag mechanism
                parserContext.framesetOk = false;
                return;
            }

            //
            // Any other start tag:
            //

            InsertionHelpers::ReconstructActiveFormattingElements(&parserContext);
            InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
            return;
        }

        if (token.type == TokenType::END_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);

            //
            // Any other end tag:
            //
            for (auto it = std::end(parserContext.stackOfOpenElements) - 1;
                    it >= std::begin(parserContext.stackOfOpenElements);
                    --it) {
                auto &element = **it;
                if (element.localName == tagToken->name) {
                    // TODO generate implied end tags (except...
                    if (parserContext.stackOfOpenElements.back() != *it) {
                        PARSE_ERROR(ParseError::IMPLIED_END_TAGS_IS_NOT_CURRENT_ELEMENT);
                    }
                    break;
                }

                if (InsertionHelpers::IsSpecialElementInStackOfOpenElements(&element)) {
                    PARSE_ERROR(ParseError::SPECIAL_NODE_ENCOUNTERED_IN_BODY_ANY_OTHER_END_TAG);
                }
            }

            // pop all elements off stackOfOpenElements up including current tag name.
            bool isTargetName = false;
            while (!isTargetName) {
                isTargetName = parserContext.stackOfOpenElements.back()->localName == tagToken->name;
                parserContext.stackOfOpenElements.pop_back();
            }

            return;
        }

        std::stringstream stream;
        stream << "Not implemented: ";
        switch (token.type) {
            case TokenType::CHARACTER:
                stream << "character token (" << static_cast<const CharacterToken *>(&token)->data << ')';
                break;
            case TokenType::COMMENT:
                stream << "comment token";
                break;
            case TokenType::DOCTYPE:
                stream << "DOCTYPE token";
                break;
            case TokenType::END_OF_FILE:
                stream << "EOF token";
                break;
            case TokenType::END_TAG:
                stream << "end tag (" << static_cast<const TagToken *>(&token)->name << ')';
                break;
            case TokenType::START_TAG:
                stream << "start tag (" << static_cast<const TagToken *>(&token)->name << ')';
                break;
        }

        PARSER_TODO(stream.str());
    }

} // namespace HTML::InsertionModeImplementations
