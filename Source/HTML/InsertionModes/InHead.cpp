/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Index.hpp"

#include "Source/HTML/TokenizerContext.hpp"

#include "Source/HTML/InsertionHelpers.hpp"
#include "Source/HTML/InsertionModes/Index.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace HTML::InsertionModeImplementations {

    static void
    HandleInHeadAnythingElse(ParserContext &, const Token &);

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#parsing-main-inhead
     */
    void
    InHeadMode(ParserContext &parserContext, const Token &token) {
        if (InsertionHelpers::IsWhitespaceToken(&token)) {
            InsertionHelpers::InsertCharacter(&parserContext, static_cast<const CharacterToken *>(&token));
            return;
        }

        if (token.type == TokenType::COMMENT) {
            InsertionHelpers::InsertComment(
                    &parserContext,
                    static_cast<const CommentToken *>(&token)->data
            );
            return;
        }

        if (token.type == TokenType::DOCTYPE) {
            PARSE_ERROR(ParseError::DOCTYPE_INCORRECTLY_PLACED);
            // Ignore the token.
            return;
        }

        if (token.type == TokenType::START_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);

            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "html")) {
                parserContext.ProcessUsingOtherRules(InsertionModeName::IN_BODY, token);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "base") ||
                Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "basefont") ||
                Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "bgsound") ||
                Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "link")) {
                auto element = InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                parserContext.PopCurrentNodeOffStack();

                //
                // TODO how to acknowledge the self-closing flag?
                //

                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "meta")) {
                auto element = InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                parserContext.PopCurrentNodeOffStack();

                //
                // TODO how to acknowledge the self-closing flag?
                //

                //
                // TODO handle charset and content-type extracting and handling.
                //

                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "title")) {
                InsertionHelpers::FollowGenericRCDATAAlgorithm(&parserContext, tagToken);
                return;
            }
            bool isNoScript = Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "noscript");
            if ((isNoScript && parserContext.scriptingFlag) ||
                    Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "noframes") ||
                    Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "style")) {
                InsertionHelpers::FollowGenericRawTextAlgorithm(&parserContext, tagToken);
                return;
            }
            if (isNoScript && !parserContext.scriptingFlag) {
                InsertionHelpers::InsertHTMLElement(&parserContext, tagToken);
                parserContext.SwitchInsertionModeTo(InsertionModeName::IN_HEAD_NOSCRIPT);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "script")) {
                InsertionHelpers::InsertScriptElement(&parserContext, tagToken);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "template")) {
                //
                // TODO handle the template tag
                //
                return;
            }
        }
        if (token.type == TokenType::END_TAG) {
            const TagToken *tagToken = static_cast<const TagToken *>(&token);
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "head")) {
                parserContext.PopCurrentNodeOffStack();
                parserContext.SwitchInsertionModeTo(InsertionModeName::AFTER_HEAD);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "body") ||
                Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "html") ||
                Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "br")) {
                HandleInHeadAnythingElse(parserContext, token);
                return;
            }
            if (Unicode::FastUStrings::EqualsASCIIIgnoreCase(tagToken->name, "template")) {
                //
                // TODO handle the template tag
                //
                return;
            }
        }

        if (token.type == TokenType::END_TAG) {
            PARSE_ERROR(ParseError::UNEXPECTED_END_TAG_TOKEN);
            // Ignore the token.
            return;
        }
        if (token.type == TokenType::START_TAG &&
            Unicode::FastUStrings::EqualsASCIIIgnoreCase(static_cast<const TagToken *>(&token)->name, "head")) {
            PARSE_ERROR(ParseError::HEAD_START_TAG_TOKEN_IN_HEAD);
            // Ignore the token.
            return;
        }
        HandleInHeadAnythingElse(parserContext, token);
    }

    void
    HandleInHeadAnythingElse(ParserContext &parserContext, const Token &token) {
        // pop head element off the stack
        // TODO maybe verify it was the head element?
        parserContext.PopCurrentNodeOffStack();

        parserContext.SwitchInsertionModeTo(InsertionModeName::AFTER_HEAD);

        parserContext.ReprocessToken(token);
    }

} // namespace HTML::InsertionModeImplementations
