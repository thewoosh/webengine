/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/HTML/Elements/HTMLElement.hpp"

namespace HTML {

    /**
     * https://html.spec.whatwg.org/multipage/sections.html#the-body-element
     */
    struct HTMLBodyElement : public HTMLElement {

        inline
        HTMLBodyElement()
                : HTMLElement("body") {
            commonElementType = DOM::CommonElementType::BODY;
        }

    };

} // namespace HTML
