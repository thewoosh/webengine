/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/Element.hpp"
#include "Source/Text/Namespaces.hpp"

namespace HTML {

    /**
     * https://html.spec.whatwg.org/multipage/dom.html#htmlelement
     */
    struct HTMLElement : public DOM::Element {

        explicit HTMLElement(const char *name) {
            Initialize(Unicode::UString(Text::Namespaces::HTML),
                       nullptr,
                       Unicode::UString(name),
                       DOM::Concept::CustomElementState::UNCUSTOMIZED,
                       nullptr);
        }

    };

} // namespace HTML
