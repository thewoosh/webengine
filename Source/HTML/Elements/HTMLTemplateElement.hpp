/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/HTML/Elements/HTMLElement.hpp"

namespace HTML {

    /**
     * https://html.spec.whatwg.org/multipage/scripting.html#the-template-element
     */
    struct HTMLTemplateElement : public HTMLElement {

        inline
        HTMLTemplateElement()
                : HTMLElement("template") {
            commonElementType = DOM::CommonElementType::TEMPLATE;
        }

    };

} // namespace HTML
