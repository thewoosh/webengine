/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/HTML/Elements/HTMLElement.hpp"

namespace HTML {

    /**
     * https://html.spec.whatwg.org/multipage/semantics.html#the-html-element
     */
    struct HTMLHtmlElement : public HTMLElement {

        inline HTMLHtmlElement()
                : HTMLElement("html") {
            commonElementType = DOM::CommonElementType::HTML;
        }

    };

} // namespace HTML
