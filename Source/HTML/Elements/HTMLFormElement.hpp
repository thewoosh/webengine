/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/ResettableElement.hpp"
#include "Source/HTML/Elements/HTMLElement.hpp"

namespace HTML {

    /**
     * https://html.spec.whatwg.org/multipage/semantics.html#the-html-element
     */
    struct HTMLFormElement : public HTMLElement, DOM::ResettableElement {

        inline HTMLFormElement()
                : HTMLElement("form") {
            commonElementType = DOM::CommonElementType::FORM;
        }

        void
        InvokeReset() override {
            // TODO
        }

    };

} // namespace HTML
