/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

namespace HTML {

    /**
     * This enum holds the names of the states of the tokenizer.
     */
    enum class TokenizerStateName {

        /**
         * Internal null-like value.
         */
        UNDEFINED,

        DATA,
        RCDATA,
        RAWTEXT,
        SCRIPT_DATA,
        PLAINTEXT,
        TAG_OPEN,
        END_TAG_OPEN,
        TAG_NAME,
        RCDATA_LESS_THAN_SIGN,
        RCDATA_END_TAG_OPEN,
        RCDATA_END_TAG_NAME,
        RAWTEXT_LESS_THAN_SIGN,
        RAWTEXT_END_TAG_OPEN,
        RAWTEXT_END_TAG_NAME,
        SCRIPT_DATA_LESS_THAN_SIGN,
        SCRIPT_DATA_END_TAG_OPEN,
        SCRIPT_DATA_END_TAG_NAME,
        SCRIPT_DATA_ESCAPE_START,
        SCRIPT_DATA_ESCAPE_START_DASH,
        SCRIPT_DATA_ESCAPED,
        SCRIPT_DATA_ESCAPED_DASH,
        SCRIPT_DATA_ESCAPED_DASH_DASH,
        SCRIPT_DATA_ESCAPED_LESS_THAN_SIGN,
        SCRIPT_DATA_ESCAPED_END_TAG_OPEN,
        SCRIPT_DATA_ESCAPED_END_TAG_NAME,
        SCRIPT_DATA_DOUBLE_ESCAPE_START,
        SCRIPT_DATA_DOUBLE_ESCAPED,
        SCRIPT_DATA_DOUBLE_ESCAPED_DASH,
        SCRIPT_DATA_DOUBLE_ESCAPED_DASH_DASH,
        SCRIPT_DATA_DOUBLE_ESCAPED_LESS_THAN_SIGN,
        SCRIPT_DATA_DOUBLE_ESCAPE_END,
        BEFORE_ATTRIBUTE_NAME,
        ATTRIBUTE_NAME,
        AFTER_ATTRIBUTE_NAME,
        BEFORE_ATTRIBUTE_VALUE,
        ATTRIBUTE_VALUE_DOUBLE_QUOTED,
        ATTRIBUTE_VALUE_SINGLE_QUOTED,
        ATTRIBUTE_VALUE_UNQUOTED,
        AFTER_ATTRIBUTE_VALUE_QUOTED,
        SELF_CLOSING_START_TAG,
        BOGUS_COMMENT,
        MARKUP_DECLARATION_OPEN,
        COMMENT_START,
        COMMENT_START_DASH,
        COMMENT,
        COMMENT_LESS_THAN_SIGN,
        COMMENT_LESS_THAN_SIGN_BANG,
        COMMENT_LESS_THAN_SIGN_BANG_DASH,
        COMMENT_LESS_THAN_SIGN_BANG_DASH_DASH,
        COMMENT_END_DASH,
        COMMENT_END,
        COMMENT_END_BANG,
        DOCTYPE,
        BEFORE_DOCTYPE_NAME,
        DOCTYPE_NAME,
        AFTER_DOCTYPE_NAME,
        AFTER_DOCTYPE_PUBLIC_KEYWORD,
        BEFORE_DOCTYPE_PUBLIC_IDENTIFIER,
        DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED,
        DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED,
        AFTER_DOCTYPE_PUBLIC_IDENTIFIER,
        BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS,
        AFTER_DOCTYPE_SYSTEM_KEYWORD,
        BEFORE_DOCTYPE_SYSTEM_IDENTIFIER,
        DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED,
        DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED,
        AFTER_DOCTYPE_SYSTEM_IDENTIFIER,
        BOGUS_DOCTYPE,
        CDATA_SECTION,
        CDATA_SECTION_BRACKET,
        CDATA_SECTION_END,
        CHARACTER_REFERENCE,
        NAMED_CHARACTER_REFERENCE,
        AMBIGUOUS_AMPERSAND,
        NUMERIC_CHARACTER_REFERENCE,
        HEXADECIMAL_CHARACTER_REFERENCE_START,
        DECIMAL_CHARACTER_REFERENCE_START,
        HEXADECIMAL_CHARACTER_REFERENCE,
        DECIMAL_CHARACTER_REFERENCE,
        NUMERIC_CHARACTER_REFERENCE_END,
        IN_FOREIGN_CONTENT
    };

    constexpr const char *
    stringOfTokenizerStringName(TokenizerStateName name) {
        switch (name) {
            case TokenizerStateName::UNDEFINED:
                return "UNDEFINED(INTERNAL)";
            case TokenizerStateName::DATA:
                return "DATA";
            case TokenizerStateName::RCDATA:
                return "RCDATA";
            case TokenizerStateName::RAWTEXT:
                return "RAWTEXT";
            case TokenizerStateName::SCRIPT_DATA:
                return "SCRIPT_DATA";
            case TokenizerStateName::PLAINTEXT:
                return "PLAINTEXT";
            case TokenizerStateName::TAG_OPEN:
                return "TAG_OPEN";
            case TokenizerStateName::END_TAG_OPEN:
                return "END_TAG_OPEN";
            case TokenizerStateName::TAG_NAME:
                return "TAG_NAME";
            case TokenizerStateName::RCDATA_LESS_THAN_SIGN:
                return "RCDATA_LESS_THAN_SIGN";
            case TokenizerStateName::RCDATA_END_TAG_OPEN:
                return "RCDATA_END_TAG_OPEN";
            case TokenizerStateName::RCDATA_END_TAG_NAME:
                return "RCDATA_END_TAG_NAME";
            case TokenizerStateName::RAWTEXT_LESS_THAN_SIGN:
                return "RAWTEXT_LESS_THAN_SIGN";
            case TokenizerStateName::RAWTEXT_END_TAG_OPEN:
                return "RAWTEXT_END_TAG_OPEN";
            case TokenizerStateName::RAWTEXT_END_TAG_NAME:
                return "RAWTEXT_END_TAG_NAME";
            case TokenizerStateName::SCRIPT_DATA_LESS_THAN_SIGN:
                return "SCRIPT_DATA_LESS_THAN_SIGN";
            case TokenizerStateName::SCRIPT_DATA_END_TAG_OPEN:
                return "SCRIPT_DATA_END_TAG_OPEN";
            case TokenizerStateName::SCRIPT_DATA_END_TAG_NAME:
                return "SCRIPT_DATA_END_TAG_NAME";
            case TokenizerStateName::SCRIPT_DATA_ESCAPE_START:
                return "SCRIPT_DATA_ESCAPE_START";
            case TokenizerStateName::SCRIPT_DATA_ESCAPE_START_DASH:
                return "SCRIPT_DATA_ESCAPE_START_DASH";
            case TokenizerStateName::SCRIPT_DATA_ESCAPED:
                return "SCRIPT_DATA_ESCAPED";
            case TokenizerStateName::SCRIPT_DATA_ESCAPED_DASH:
                return "SCRIPT_DATA_ESCAPED_DASH";
            case TokenizerStateName::SCRIPT_DATA_ESCAPED_DASH_DASH:
                return "SCRIPT_DATA_ESCAPED_DASH_DASH";
            case TokenizerStateName::SCRIPT_DATA_ESCAPED_LESS_THAN_SIGN:
                return "SCRIPT_DATA_ESCAPED_LESS_THAN_SIGN";
            case TokenizerStateName::SCRIPT_DATA_ESCAPED_END_TAG_OPEN:
                return "SCRIPT_DATA_ESCAPED_END_TAG_OPEN";
            case TokenizerStateName::SCRIPT_DATA_ESCAPED_END_TAG_NAME:
                return "SCRIPT_DATA_ESCAPED_END_TAG_NAME";
            case TokenizerStateName::SCRIPT_DATA_DOUBLE_ESCAPE_START:
                return "SCRIPT_DATA_DOUBLE_ESCAPE_START";
            case TokenizerStateName::SCRIPT_DATA_DOUBLE_ESCAPED:
                return "SCRIPT_DATA_DOUBLE_ESCAPED";
            case TokenizerStateName::SCRIPT_DATA_DOUBLE_ESCAPED_DASH:
                return "SCRIPT_DATA_DOUBLE_ESCAPED_DASH";
            case TokenizerStateName::SCRIPT_DATA_DOUBLE_ESCAPED_DASH_DASH:
                return "SCRIPT_DATA_DOUBLE_ESCAPED_DASH_DASH";
            case TokenizerStateName::SCRIPT_DATA_DOUBLE_ESCAPED_LESS_THAN_SIGN:
                return "SCRIPT_DATA_DOUBLE_ESCAPED_LESS_THAN_SIGN";
            case TokenizerStateName::SCRIPT_DATA_DOUBLE_ESCAPE_END:
                return "SCRIPT_DATA_DOUBLE_ESCAPE_END";
            case TokenizerStateName::BEFORE_ATTRIBUTE_NAME:
                return "BEFORE_ATTRIBUTE_NAME";
            case TokenizerStateName::ATTRIBUTE_NAME:
                return "ATTRIBUTE_NAME";
            case TokenizerStateName::AFTER_ATTRIBUTE_NAME:
                return "AFTER_ATTRIBUTE_NAME";
            case TokenizerStateName::BEFORE_ATTRIBUTE_VALUE:
                return "BEFORE_ATTRIBUTE_VALUE";
            case TokenizerStateName::ATTRIBUTE_VALUE_DOUBLE_QUOTED:
                return "ATTRIBUTE_VALUE_DOUBLE_QUOTED";
            case TokenizerStateName::ATTRIBUTE_VALUE_SINGLE_QUOTED:
                return "ATTRIBUTE_VALUE_SINGLE_QUOTED";
            case TokenizerStateName::ATTRIBUTE_VALUE_UNQUOTED:
                return "ATTRIBUTE_VALUE_UNQUOTED";
            case TokenizerStateName::AFTER_ATTRIBUTE_VALUE_QUOTED:
                return "AFTER_ATTRIBUTE_VALUE_QUOTED";
            case TokenizerStateName::SELF_CLOSING_START_TAG:
                return "SELF_CLOSING_START_TAG";
            case TokenizerStateName::BOGUS_COMMENT:
                return "BOGUS_COMMENT";
            case TokenizerStateName::MARKUP_DECLARATION_OPEN:
                return "MARKUP_DECLARATION_OPEN";
            case TokenizerStateName::COMMENT_START:
                return "COMMENT_START";
            case TokenizerStateName::COMMENT_START_DASH:
                return "COMMENT_START_DASH";
            case TokenizerStateName::COMMENT:
                return "COMMENT";
            case TokenizerStateName::COMMENT_LESS_THAN_SIGN:
                return "COMMENT_LESS_THAN_SIGN";
            case TokenizerStateName::COMMENT_LESS_THAN_SIGN_BANG:
                return "COMMENT_LESS_THAN_SIGN_BANG";
            case TokenizerStateName::COMMENT_LESS_THAN_SIGN_BANG_DASH:
                return "COMMENT_LESS_THAN_SIGN_BANG_DASH";
            case TokenizerStateName::COMMENT_LESS_THAN_SIGN_BANG_DASH_DASH:
                return "COMMENT_LESS_THAN_SIGN_BANG_DASH_DASH";
            case TokenizerStateName::COMMENT_END_DASH:
                return "COMMENT_END_DASH";
            case TokenizerStateName::COMMENT_END:
                return "COMMENT_END";
            case TokenizerStateName::COMMENT_END_BANG:
                return "COMMENT_END_BANG";
            case TokenizerStateName::DOCTYPE:
                return "DOCTYPE";
            case TokenizerStateName::BEFORE_DOCTYPE_NAME:
                return "BEFORE_DOCTYPE_NAME";
            case TokenizerStateName::DOCTYPE_NAME:
                return "DOCTYPE_NAME";
            case TokenizerStateName::AFTER_DOCTYPE_NAME:
                return "AFTER_DOCTYPE_NAME";
            case TokenizerStateName::AFTER_DOCTYPE_PUBLIC_KEYWORD:
                return "AFTER_DOCTYPE_PUBLIC_KEYWORD";
            case TokenizerStateName::BEFORE_DOCTYPE_PUBLIC_IDENTIFIER:
                return "BEFORE_DOCTYPE_PUBLIC_IDENTIFIER";
            case TokenizerStateName::DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED:
                return "DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED";
            case TokenizerStateName::DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED:
                return "DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED";
            case TokenizerStateName::AFTER_DOCTYPE_PUBLIC_IDENTIFIER:
                return "AFTER_DOCTYPE_PUBLIC_IDENTIFIER";
            case TokenizerStateName::BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS:
                return "BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS";
            case TokenizerStateName::AFTER_DOCTYPE_SYSTEM_KEYWORD:
                return "AFTER_DOCTYPE_SYSTEM_KEYWORD";
            case TokenizerStateName::BEFORE_DOCTYPE_SYSTEM_IDENTIFIER:
                return "BEFORE_DOCTYPE_SYSTEM_IDENTIFIER";
            case TokenizerStateName::DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED:
                return "DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED";
            case TokenizerStateName::DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED:
                return "DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED";
            case TokenizerStateName::AFTER_DOCTYPE_SYSTEM_IDENTIFIER:
                return "AFTER_DOCTYPE_SYSTEM_IDENTIFIER";
            case TokenizerStateName::BOGUS_DOCTYPE:
                return "BOGUS_DOCTYPE";
            case TokenizerStateName::CDATA_SECTION:
                return "CDATA_SECTION";
            case TokenizerStateName::CDATA_SECTION_BRACKET:
                return "CDATA_SECTION_BRACKET";
            case TokenizerStateName::CDATA_SECTION_END:
                return "CDATA_SECTION_END";
            case TokenizerStateName::CHARACTER_REFERENCE:
                return "CHARACTER_REFERENCE";
            case TokenizerStateName::NAMED_CHARACTER_REFERENCE:
                return "NAMED_CHARACTER_REFERENCE";
            case TokenizerStateName::AMBIGUOUS_AMPERSAND:
                return "AMBIGUOUS_AMPERSAND";
            case TokenizerStateName::NUMERIC_CHARACTER_REFERENCE:
                return "NUMERIC_CHARACTER_REFERENCE";
            case TokenizerStateName::HEXADECIMAL_CHARACTER_REFERENCE_START:
                return "HEXADECIMAL_CHARACTER_REFERENCE_START";
            case TokenizerStateName::DECIMAL_CHARACTER_REFERENCE_START:
                return "DECIMAL_CHARACTER_REFERENCE_START";
            case TokenizerStateName::HEXADECIMAL_CHARACTER_REFERENCE:
                return "HEXADECIMAL_CHARACTER_REFERENCE";
            case TokenizerStateName::DECIMAL_CHARACTER_REFERENCE:
                return "DECIMAL_CHARACTER_REFERENCE";
            case TokenizerStateName::NUMERIC_CHARACTER_REFERENCE_END:
                return "NUMERIC_CHARACTER_REFERENCE_END";
            case TokenizerStateName::IN_FOREIGN_CONTENT:
                return "IN_FOREIGN_CONTENT";
            default:
                // unreachable, but GCC complains.
                return NULL;
        }
    }

    constexpr auto &
    operator<<(auto &stream, TokenizerStateName name) {
        stream << stringOfTokenizerStringName(name);
        return stream;
    }

} // namespace HTML
