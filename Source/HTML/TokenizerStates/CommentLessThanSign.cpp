/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#comment-less-than-sign-state
     */
    void CommentLessThanSignState(TokenizerContext &context) {
        if (!context.stream->IsEOF()) {
            Unicode::CodePoint character = context.stream->Read();

            if (character == Unicode::EXCLAMATION_MARK) {
                context.builder.commentToken->data += character;
                context.SwitchToState(TokenizerStateName::COMMENT_LESS_THAN_SIGN_BANG);
                return;
            }

            if (character == Unicode::LESS_THAN_SIGN) {
                context.builder.commentToken->data += character;
                // implicitly don't change state
                return;
            }
        }

        context.ReconsumeIn(TokenizerStateName::COMMENT);
    }

} // namespace HTML::TokenizerStates
