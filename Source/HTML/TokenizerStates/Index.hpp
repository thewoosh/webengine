/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

/**
 * This is the index for the tokenizer state implementation functions. This way,
 * I don't have to create a separate header file for each state.
 */
namespace HTML::TokenizerStates {

    void DataState(TokenizerContext &);
    void MarkupDeclarationOpenState(TokenizerContext &);

    void RawTextState(TokenizerContext &);
    void RawTextLessThanSignState(TokenizerContext &);
    void RawTextEndTagOpenState(TokenizerContext &);
    void RawTextEndTagNameState(TokenizerContext &);

    void RcdataEndTagNameState(TokenizerContext &);
    void RcdataEndTagOpenState(TokenizerContext &);
    void RcdataLessThanSignState(TokenizerContext &);
    void RcdataState(TokenizerContext &);

    void AfterDoctypeNameState(TokenizerContext &);
    void AfterDoctypePublicIdentifierState(TokenizerContext &);
    void AfterDoctypePublicKeywordState(TokenizerContext &);
    void AfterDoctypeSystemIdentifierState(TokenizerContext &);
    void AfterDoctypeSystemKeywordState(TokenizerContext &);
    void BeforeDoctypeNameState(TokenizerContext &);
    void BeforeDoctypePublicIdentifier(TokenizerContext &);
    void BeforeDoctypeSystemIdentifier(TokenizerContext &);
    void BetweenDoctypePublicAndSystemIdentifiersState(TokenizerContext &);
    void BogusDoctypeState(TokenizerContext &);
    void DoctypeNameState(TokenizerContext &);
    void DoctypePublicIdentifierDoubleQuoted(TokenizerContext &);
    void DoctypePublicIdentifierSingleQuoted(TokenizerContext &);
    void DoctypeSystemIdentifierDoubleQuoted(TokenizerContext &);
    void DoctypeSystemIdentifierSingleQuoted(TokenizerContext &);
    void DoctypeState(TokenizerContext &);

    void EndTagOpenState(TokenizerContext &);
    void TagNameState(TokenizerContext &);
    void TagOpenState(TokenizerContext &);

    void AfterAttributeNameState(TokenizerContext &);
    void AfterAttributeValueUnquoted(TokenizerContext &);
    void AttributeNameState(TokenizerContext &);
    void AttributeValueDoubleQuoted(TokenizerContext &);
    void AttributeValueSingleQuoted(TokenizerContext &);
    void AttributeValueUnquoted(TokenizerContext &);
    void BeforeAttributeNameState(TokenizerContext &);
    void BeforeAttributeValueState(TokenizerContext &);

    void BogusCommentState(TokenizerContext &);
    void CommentEndBangState(TokenizerContext &);
    void CommentEndDashState(TokenizerContext &);
    void CommentEndState(TokenizerContext &);
    void CommentLessThanSignState(TokenizerContext &);
    void CommentLessThanSignBangState(TokenizerContext &);
    void CommentLessThanSignBangDashState(TokenizerContext &);
    void CommentLessThanSignBangDashDashState(TokenizerContext &);
    void CommentState(TokenizerContext &);
    void CommentStartState(TokenizerContext &);
    void CommentStartDashState(TokenizerContext &);

} // namespace HTML::TokenizerStates
