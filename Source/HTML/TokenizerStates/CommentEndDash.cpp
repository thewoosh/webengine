/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#comment-end-dash-state
     */
    void CommentEndDashState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_COMMENT);
            context.builder.commentToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();
        if (character == Unicode::HYPHEN_MINUS) {
            context.SwitchToState(TokenizerStateName::COMMENT_END);
        } else {
            context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
            context.SwitchToState(TokenizerStateName::COMMENT);
        }
    }

} // namespace HTML::TokenizerStates
