/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#tag-name-state
     */
    void TagNameState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_TAG);
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                context.SwitchToState(TokenizerStateName::BEFORE_ATTRIBUTE_NAME);
                break;
            case Unicode::SOLIDUS:
                context.SwitchToState(TokenizerStateName::SELF_CLOSING_START_TAG);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.tagToken.emit();
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                context.builder.tagToken->name += Unicode::REPLACEMENT_CHARACTER;
                break;
            default:
                if (Unicode::IsASCIIUpperAlpha(character)) {
                    context.builder.tagToken->name += Unicode::ToLowerASCII(character);
                    break;
                }

                context.builder.tagToken->name += character;
                break;
        }
    }

} // namespace HTML::TokenizerStates
