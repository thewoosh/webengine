/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#before-doctype-name-state
     *
     * In this state, the '<!doctype' portions are already consumed by:
     * data -> tag open -> markup declaration open
     */
    void BeforeDoctypeNameState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_DOCTYPE);

            context.builder.doctypeToken.create();
            context.builder.doctypeToken.token->forceQuirks = true;
            context.builder.doctypeToken.emit();

            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                // Ignore character
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                context.builder.doctypeToken.create();
                context.builder.doctypeToken->forceQuirks = true;
                context.builder.doctypeToken->name = Unicode::UString(Unicode::REPLACEMENT_CHARACTER);
                context.SwitchToState(TokenizerStateName::DOCTYPE_NAME);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.builder.doctypeToken.create();
                context.builder.doctypeToken.token->forceQuirks = true;
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.doctypeToken.emit();
                break;
            default:
                context.builder.doctypeToken.create();
                if (Unicode::IsASCIIUpperAlpha(character)) {
                    context.builder.doctypeToken.token->name = Unicode::UString(Unicode::ToLowerASCII(character));
                } else {
                    context.builder.doctypeToken.token->name = Unicode::UString(character);
                }
                context.SwitchToState(TokenizerStateName::DOCTYPE_NAME);
                break;
        }
    }

} // namespace HTML::TokenizerStates
