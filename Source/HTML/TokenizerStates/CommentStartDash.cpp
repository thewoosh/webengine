/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#comment-start-dash-state
     */
    void CommentStartDashState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_COMMENT);
            context.builder.commentToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::HYPHEN_MINUS:
                context.SwitchToState(TokenizerStateName::COMMENT_END);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.ReportParseError(ParseError::ABRUPT_CLOSING_OF_EMPTY_COMMENT);
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.commentToken.emit();
                break;
            default:
                context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
                context.ReconsumeIn(TokenizerStateName::COMMENT);
                break;
        }
    }

} // namespace HTML::TokenizerStates
