/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    template<std::size_t LiteralLength>
    [[nodiscard]] static bool
    CheckCaseInsensitive(TokenizerContext &context, const char (&literal)[LiteralLength]) {
        constexpr std::size_t length = LiteralLength - 1;
        std::array<Unicode::CodePoint, length> array{};

        if (!context.stream->Populate(std::begin(array), std::end(array))) {
            return false;
        }

        if (!context.CompareCodePointsAndStringLiteral(literal, array, false)) {
            context.ReconsumeStream(array.size());
            return false;
        }

        return true;
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#after-doctype-name-state
     */
    void AfterDoctypeNameState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_DOCTYPE);
            context.builder.doctypeToken->forceQuirks = true;
            context.builder.doctypeToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();
        if (character == Unicode::CHARACTER_TABULATION ||
            character == Unicode::LINE_FEED ||
            character == Unicode::FORM_FEED ||
            character == Unicode::SPACE) {
            // ignore
            return;
        }

        if (character == Unicode::GREATER_THAN_SIGN) {
            context.SwitchToState(TokenizerStateName::DATA);
            context.builder.doctypeToken.emit();
            return;
        }

        context.stream->Reconsume();

        if (CheckCaseInsensitive(context, "public")) {
            context.SwitchToState(TokenizerStateName::AFTER_DOCTYPE_PUBLIC_KEYWORD);
            return;
        }

        if (CheckCaseInsensitive(context, "system")) {
            context.SwitchToState(TokenizerStateName::AFTER_DOCTYPE_SYSTEM_KEYWORD);
            return;
        }

        context.ReportParseError(ParseError::INVALID_CHARACTER_SEQUENCE_AFTER_DOCTYPE_NAME);
        context.builder.doctypeToken->forceQuirks = true;
        context.SwitchToState(TokenizerStateName::BOGUS_DOCTYPE);
    }

} // namespace HTML::TokenizerStates
