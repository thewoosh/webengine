/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#tag-open-state
     */
    void TagOpenState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_BEFORE_TAG_NAME);
            context.EmitCharacterToken(Unicode::LESS_THAN_SIGN);
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::EXCLAMATION_MARK:
                context.SwitchToState(TokenizerStateName::MARKUP_DECLARATION_OPEN);
                break;
            case Unicode::SOLIDUS:
                context.SwitchToState(TokenizerStateName::END_TAG_OPEN);
                break;
            case Unicode::QUESTION_MARK:
                context.ReportParseError(ParseError::UNEXPECTED_QUESTION_MARK_INSTEAD_OF_TAG_NAME);
                context.builder.commentToken.create();
                context.ReconsumeIn(TokenizerStateName::BOGUS_COMMENT);
                break;
            default:
                if (Unicode::IsASCIIAlpha(character)) {
                    context.builder.tagToken.create(TokenType::START_TAG, Unicode::UString());
                    context.ReconsumeIn(TokenizerStateName::TAG_NAME);
                    break;
                }

                context.ReportParseError(ParseError::INVALID_FIRST_CHARACTER_OF_TAG_NAME);
                context.SwitchToState(TokenizerStateName::DATA);
                context.EmitCharacterToken(Unicode::LESS_THAN_SIGN);
                break;
        }
    }

} // namespace HTML::TokenizerStates
