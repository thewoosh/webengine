/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#after-doctype-public-keyword-state
     */
    void AfterDoctypePublicKeywordState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_DOCTYPE);
            context.builder.doctypeToken->forceQuirks = true;
            context.builder.doctypeToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();
        if (character == Unicode::CHARACTER_TABULATION ||
            character == Unicode::LINE_FEED ||
            character == Unicode::FORM_FEED ||
            character == Unicode::SPACE) {
            context.SwitchToState(TokenizerStateName::BEFORE_DOCTYPE_PUBLIC_IDENTIFIER);
            return;
        }

        if (character == Unicode::QUOTATION_MARK) {
            context.ReportParseError(ParseError::MISSING_WHITESPACE_AFTER_DOCTYPE_PUBLIC_KEYWORD);
            context.builder.doctypeToken->publicIdentifier.emplace();
            context.SwitchToState(TokenizerStateName::DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED);
            return;
        }

        if (character == Unicode::APOSTROPHE) {
            context.ReportParseError(ParseError::MISSING_WHITESPACE_AFTER_DOCTYPE_PUBLIC_KEYWORD);
            context.builder.doctypeToken->publicIdentifier.emplace();
            context.SwitchToState(TokenizerStateName::DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED);
            return;
        }

        if (character == Unicode::GREATER_THAN_SIGN) {
            context.ReportParseError(ParseError::MISSING_DOCTYPE_PUBLIC_IDENTIFIER);
            context.SwitchToState(TokenizerStateName::DATA);
            context.builder.doctypeToken.emit();
            return;
        }

        context.ReportParseError(ParseError::MISSING_QUOTE_BEFORE_DOCTYPE_PUBLIC_IDENTIFIER);
        context.builder.doctypeToken->forceQuirks = true;
        context.ReconsumeIn(TokenizerStateName::BOGUS_DOCTYPE);
    }

} // namespace HTML::TokenizerStates
