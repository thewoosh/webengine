/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#rawtext-end-tag-open-state
     */
    void RawTextEndTagOpenState(TokenizerContext &context) {
        if (!context.stream->IsEOF()) {
            const auto character = context.stream->Read();

            if (Unicode::IsASCIIAlpha(character)) {
                context.builder.tagToken.create(TokenType::END_TAG, Unicode::UString());
                context.ReconsumeIn(TokenizerStateName::RAWTEXT_END_TAG_NAME);
                return;
            }

            context.stream->Reconsume();
        }

        context.EmitCharacterToken(Unicode::LESS_THAN_SIGN);
        context.EmitCharacterToken(Unicode::SOLIDUS);
        context.SwitchToState(TokenizerStateName::RAWTEXT);
    }

} // namespace HTML::TokenizerStates
