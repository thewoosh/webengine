/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#rcdata-end-tag-name-state
     */
    void RcdataEndTagNameState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                if (context.builder.tagToken.isAppropriateEndTagToken()) {
                    context.SwitchToState(TokenizerStateName::BEFORE_ATTRIBUTE_NAME);
                    return;
                }
                break;
            case Unicode::SOLIDUS:
                if (context.builder.tagToken.isAppropriateEndTagToken()) {
                    context.SwitchToState(TokenizerStateName::SELF_CLOSING_START_TAG);
                    return;
                }
                break;
            case Unicode::GREATER_THAN_SIGN:
                if (context.builder.tagToken.isAppropriateEndTagToken()) {
                    context.SwitchToState(TokenizerStateName::DATA);
                    context.builder.tagToken.emit();
                    return;
                }
                break;
            default:
                if (Unicode::IsASCIIUpperAlpha(character)) {
                    context.builder.tagToken->name += Unicode::ToLowerASCII(character);
                    context.temporaryBuffer += character;
                    return;
                }
                // The following acts like IsASCIILowerAlpha, since
                // IsASCIIUpperAlpha is already handled.
                if (Unicode::IsASCIIAlpha(character)) {
                    context.builder.tagToken->name += character;
                    context.temporaryBuffer += character;
                    return;
                }
                break;
        }

        // Anything else:
        context.EmitCharacterToken(Unicode::LESS_THAN_SIGN);
        context.EmitCharacterToken(Unicode::SOLIDUS);
        for (const auto &tempBufferCharacter : context.temporaryBuffer) {
            context.EmitCharacterToken(tempBufferCharacter);
        }
        context.temporaryBuffer = {}; // non-normative: clear buffer
        context.ReconsumeIn(TokenizerStateName::RCDATA);
    }

} // namespace HTML::TokenizerStates
