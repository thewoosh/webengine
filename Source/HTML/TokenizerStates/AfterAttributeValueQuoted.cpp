/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#after-attribute-value-(quoted)-state
     */
    void AfterAttributeValueUnquoted(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_TAG);
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                context.SwitchToState(TokenizerStateName::BEFORE_ATTRIBUTE_NAME);
                break;
            case Unicode::SOLIDUS:
                context.SwitchToState(TokenizerStateName::SELF_CLOSING_START_TAG);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.tagToken.emit();
                break;
            default:
                context.ReportParseError(ParseError::MISSING_WHITESPACE_BETWEEN_ATTRIBUTES);
                context.ReconsumeIn(TokenizerStateName::BEFORE_ATTRIBUTE_NAME);
                break;
        }
    }

} // namespace HTML::TokenizerStates
