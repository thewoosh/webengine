/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#after-doctype-system-keyword-state
     */
    void AfterDoctypeSystemKeywordState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_DOCTYPE);
            context.builder.doctypeToken->forceQuirks = true;
            context.builder.doctypeToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();
        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                context.SwitchToState(TokenizerStateName::BEFORE_DOCTYPE_SYSTEM_IDENTIFIER);
                break;
            case Unicode::QUOTATION_MARK:
                context.ReportParseError(ParseError::MISSING_WHITESPACE_AFTER_DOCTYPE_SYSTEM_KEYWORD);
                context.builder.doctypeToken->systemIdentifier.emplace();
                context.SwitchToState(TokenizerStateName::DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED);
                break;
            case Unicode::APOSTROPHE:
                context.ReportParseError(ParseError::MISSING_WHITESPACE_AFTER_DOCTYPE_SYSTEM_KEYWORD);
                context.builder.doctypeToken->systemIdentifier.emplace();
                context.SwitchToState(TokenizerStateName::DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.ReportParseError(ParseError::MISSING_DOCTYPE_SYSTEM_IDENTIFIER);
                context.builder.doctypeToken->forceQuirks = true;
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.doctypeToken.emit();
                break;
            default:
                context.ReportParseError(ParseError::MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER);
                context.builder.doctypeToken->forceQuirks = true;
                context.ReconsumeIn(TokenizerStateName::BOGUS_DOCTYPE);
                break;
        }
    }

} // namespace HTML::TokenizerStates
