/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#doctype-public-identifier-(double-quoted)-state
     * https://html.spec.whatwg.org/multipage/parsing.html#doctype-public-identifier-(single-quoted)-state
     *
     * Generalized function for single-quoted *and* double-quoted
     * doctype-public-identifier states.
     */
    template<Unicode::CodePoint quoteCharacter>
    void DoctypePublicIdentifierQuoted(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_DOCTYPE);
            context.builder.doctypeToken.token->forceQuirks = true;
            context.builder.doctypeToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case quoteCharacter:
                context.SwitchToState(TokenizerStateName::AFTER_DOCTYPE_PUBLIC_IDENTIFIER);
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                context.builder.doctypeToken->publicIdentifier.value() += Unicode::REPLACEMENT_CHARACTER;
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.ReportParseError(ParseError::ABRUPT_DOCTYPE_PUBLIC_IDENTIFIER);
                context.builder.doctypeToken.token->forceQuirks = true;
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.doctypeToken.emit();
                break;
            default:
                context.builder.doctypeToken->publicIdentifier.value() += character;
                break;
        }
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#doctype-public-identifier-(double-quoted)-state
     */
    void DoctypePublicIdentifierDoubleQuoted(TokenizerContext &context) {
        DoctypePublicIdentifierQuoted<Unicode::QUOTATION_MARK>(context);
    }

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#doctype-public-identifier-(single-quoted)-state
     */
    void DoctypePublicIdentifierSingleQuoted(TokenizerContext &context) {
        DoctypePublicIdentifierQuoted<Unicode::APOSTROPHE>(context);
    }

} // namespace HTML::TokenizerStates
