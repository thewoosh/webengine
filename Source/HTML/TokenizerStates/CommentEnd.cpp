/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#comment-end-dash-state
     */
    void CommentEndState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_COMMENT);
            context.builder.commentToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::GREATER_THAN_SIGN:
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.commentToken.emit();
                break;
            case Unicode::EXCLAMATION_MARK:
                context.SwitchToState(TokenizerStateName::COMMENT_END_BANG);
                break;
            case Unicode::HYPHEN_MINUS:
                context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
                // don't switch states (implicitly)
                break;
            default:
                context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
                context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
                context.ReconsumeIn(TokenizerStateName::COMMENT);
                break;
        }
    }

} // namespace HTML::TokenizerStates
