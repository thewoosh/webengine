/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#rcdata-end-tag-open-state
     */
    void RcdataEndTagOpenState(TokenizerContext &context) {
        if (!context.stream->IsEOF()) {
            if (Unicode::IsASCIIAlpha(context.stream->Read())) {
                context.builder.tagToken.create(TokenType::END_TAG, Unicode::UString());
                context.ReconsumeIn(TokenizerStateName::RCDATA_END_TAG_NAME);
                return;
            }
            context.stream->Reconsume();
        }

        context.EmitCharacterToken(Unicode::LESS_THAN_SIGN);
        context.EmitCharacterToken(Unicode::SOLIDUS);
        context.ReconsumeIn(TokenizerStateName::RCDATA);
    }

} // namespace HTML::TokenizerStates
