/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#end-tag-open-state
     */
    void EndTagOpenState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_BEFORE_TAG_NAME);
            context.EmitCharacterToken(Unicode::LESS_THAN_SIGN);
            context.EmitCharacterToken(Unicode::SOLIDUS);
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        if (Unicode::IsASCIIAlpha(character)) {
            context.builder.tagToken.create(TokenType::END_TAG, Unicode::UString(character));
            context.SwitchToState(TokenizerStateName::TAG_NAME);
            return;
        }

        if (character == Unicode::GREATER_THAN_SIGN) {
            context.ReportParseError(ParseError::MISSING_END_TAG_NAME);
            context.SwitchToState(TokenizerStateName::DATA);
            return;
        }

        context.ReportParseError(ParseError::INVALID_FIRST_CHARACTER_OF_TAG_NAME);
        context.builder.commentToken.create(Unicode::UString());
        context.ReconsumeIn(TokenizerStateName::BOGUS_COMMENT);
    }

} // namespace HTML::TokenizerStates
