/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#attribute-name-state
     */
    void AttributeNameState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.SwitchToState(TokenizerStateName::AFTER_ATTRIBUTE_NAME);
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        auto &attribute = context.builder.tagToken->attributes.back();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
            case Unicode::SOLIDUS:
            case Unicode::GREATER_THAN_SIGN:
                context.ReconsumeIn(TokenizerStateName::AFTER_ATTRIBUTE_NAME);
                break;
            case Unicode::EQUALS_SIGN:
                context.SwitchToState(TokenizerStateName::BEFORE_ATTRIBUTE_VALUE);
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                attribute.name += Unicode::REPLACEMENT_CHARACTER;
                break;
            case Unicode::QUOTATION_MARK:
            case Unicode::APOSTROPHE:
            case Unicode::LESS_THAN_SIGN:
                context.ReportParseError(ParseError::UNEXPECTED_CHARACTER_IN_ATTRIBUTE_NAME);
                [[fallthrough]];
            default:
                if (Unicode::IsASCIIUpperAlpha(character)) {
                    attribute.name += Unicode::ToLowerASCII(character);
                } else {
                    attribute.name += character;
                }
                break;
        }
    }

} // namespace HTML::TokenizerStates
