/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#before-doctype-system-identifier-state
     */
    void BeforeDoctypeSystemIdentifier(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_DOCTYPE);
            context.builder.doctypeToken.token->forceQuirks = true;
            context.builder.doctypeToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                // Ignore the character.
                break;
            case Unicode::QUOTATION_MARK:
                context.builder.doctypeToken->systemIdentifier.emplace();
                context.SwitchToState(TokenizerStateName::DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED);
                break;
            case Unicode::APOSTROPHE:
                context.builder.doctypeToken->systemIdentifier.emplace();
                context.SwitchToState(TokenizerStateName::DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.ReportParseError(ParseError::MISSING_DOCTYPE_SYSTEM_IDENTIFIER);
                context.builder.doctypeToken.token->forceQuirks = true;
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.doctypeToken.emit();
                break;
            default:
                context.ReportParseError(ParseError::MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER);
                context.builder.doctypeToken.token->forceQuirks = true;
                context.ReconsumeIn(TokenizerStateName::BOGUS_DOCTYPE);
                break;
        }
    }

} // namespace HTML::TokenizerStates
