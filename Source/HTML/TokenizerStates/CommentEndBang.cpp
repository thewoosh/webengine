/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#comment-end-bang-state
     */
    void CommentEndBangState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_COMMENT);
            context.builder.commentToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();
        if (character == Unicode::HYPHEN_MINUS) {
            context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
            context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
            context.builder.commentToken->data += Unicode::EXCLAMATION_MARK;
            context.SwitchToState(TokenizerStateName::COMMENT_END_DASH);
        } else if (character == Unicode::GREATER_THAN_SIGN) {
            context.ReportParseError(ParseError::INCORRECTLY_CLOSED_COMMENT);
            context.SwitchToState(TokenizerStateName::DATA);
            context.builder.commentToken.emit();
        } else {
            context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
            context.builder.commentToken->data += Unicode::HYPHEN_MINUS;
            context.builder.commentToken->data += Unicode::EXCLAMATION_MARK;
            context.ReconsumeIn(TokenizerStateName::COMMENT);
        }
    }

} // namespace HTML::TokenizerStates
