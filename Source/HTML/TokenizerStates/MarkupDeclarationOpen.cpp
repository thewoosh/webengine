/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    [[nodiscard]] static bool
    CheckCommentStart(TokenizerContext &context) {
        std::array<Unicode::CodePoint, 2> array{};

        if (!context.stream->Populate(std::begin(array), std::end(array))) {
            return false;
        }

        if (array[0] != Unicode::HYPHEN_MINUS || array[1] != Unicode::HYPHEN_MINUS) {
            context.ReconsumeStream(array.size());
            return false;
        }

        return true;
    }

    [[nodiscard]] static bool
    CheckDoctypeStart(TokenizerContext &context) {
        std::array<Unicode::CodePoint, 7> array{};

        if (!context.stream->Populate(std::begin(array), std::end(array))) {
            return false;
        }

        if (!context.CompareCodePointsAndStringLiteral("doctype", array, false)) {
            context.ReconsumeStream(array.size());
            return false;
        }

        return true;
    }

    [[nodiscard]] static bool
    CheckCDataStart(TokenizerContext &context) {
        std::array<Unicode::CodePoint, 7> array{};

        if (!context.stream->Populate(std::begin(array), std::end(array))) {
            return false;
        }

        if (!context.CompareCodePointsAndStringLiteral("[CDATA[", array, true)) {
            context.ReconsumeStream(array.size());
            return false;
        }

        return true;
    }


    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#markup-declaration-open-state
     */
    void MarkupDeclarationOpenState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.EmitEOFToken();
            return;
        }

        if (CheckCommentStart(context)) {
            context.builder.commentToken.create();
            context.SwitchToState(TokenizerStateName::COMMENT_START);
            return;
        }

        if (CheckDoctypeStart(context)) {
            context.SwitchToState(TokenizerStateName::DOCTYPE);
            return;
        }

        if (CheckCDataStart(context)) {
            std::cerr << "[!] CDATA-support isn't implemented.\n";
            return;
        }

        context.ReportParseError(ParseError::INCORRECTLY_OPENED_COMMENT);
        context.builder.commentToken.create();
        context.SwitchToState(TokenizerStateName::BOGUS_COMMENT);
    }

} // namespace HTML::TokenizerStates
