/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#rcdata-state
     */
    void RcdataLessThanSignState(TokenizerContext &context) {
        if (!context.stream->IsEOF()) {
            if (context.stream->Read() == Unicode::SOLIDUS) {
                context.temporaryBuffer = {};
                context.SwitchToState(TokenizerStateName::RCDATA_END_TAG_OPEN);
                return;
            }
            context.stream->Reconsume();
        }

        context.EmitCharacterToken(Unicode::LESS_THAN_SIGN);
        context.ReconsumeIn(TokenizerStateName::RCDATA);
    }

} // namespace HTML::TokenizerStates
