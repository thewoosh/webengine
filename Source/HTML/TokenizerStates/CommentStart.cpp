/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#comment-start-state
     */
    void CommentStartState(TokenizerContext &context) {
        if (!context.stream->IsEOF()) {
            Unicode::CodePoint character = context.stream->Read();

            if (character == Unicode::HYPHEN_MINUS) {
                context.SwitchToState(TokenizerStateName::COMMENT_START_DASH);
                return;
            }

            if (character == Unicode::GREATER_THAN_SIGN) {
                context.ReportParseError(ParseError::ABRUPT_CLOSING_OF_EMPTY_COMMENT);
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.commentToken.emit();
                return;
            }
        }

        context.ReconsumeIn(TokenizerStateName::COMMENT);
    }

} // namespace HTML::TokenizerStates
