/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#comment-state
     */
    void CommentState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_COMMENT);
            context.builder.commentToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::LESS_THAN_SIGN:
                context.builder.commentToken->data += Unicode::LESS_THAN_SIGN;
                context.SwitchToState(TokenizerStateName::COMMENT_LESS_THAN_SIGN);
                break;
            case Unicode::HYPHEN_MINUS:
                context.SwitchToState(TokenizerStateName::COMMENT_END_DASH);
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                context.builder.commentToken->data += Unicode::REPLACEMENT_CHARACTER;
                break;
            default:
                context.builder.commentToken->data += character;
                break;
        }
    }

} // namespace HTML::TokenizerStates
