/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"
#include "Source/Text/UnicodeTools.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#doctype-name-state
     *
     * In this state, the '<!doctype' portions are already consumed by:
     * data -> tag open -> markup declaration open
     */
    void DoctypeNameState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_DOCTYPE);

            context.builder.doctypeToken.token->forceQuirks = true;
            context.builder.doctypeToken.emit();

            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                context.SwitchToState(TokenizerStateName::AFTER_DOCTYPE_NAME);
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                context.builder.doctypeToken->name.value() += Unicode::REPLACEMENT_CHARACTER;
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.doctypeToken.emit();
                break;
            default:
                if (Unicode::IsASCIIUpperAlpha(character)) {
                    context.builder.doctypeToken.token->name.value() += Unicode::UString(Unicode::ToLowerASCII(character));
                } else {
                    context.builder.doctypeToken.token->name.value() += Unicode::UString(character);
                }
                break;
        }
    }

} // namespace HTML::TokenizerStates
