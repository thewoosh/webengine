/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#doctype-state
     *
     * In this state, the '<!doctype' portions are already consumed by:
     * data -> tag open -> markup declaration open
     */
    void DoctypeState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_DOCTYPE);

            context.builder.doctypeToken.create();
            context.builder.doctypeToken.token->forceQuirks = true;
            context.builder.doctypeToken.emit();

            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                context.SwitchToState(TokenizerStateName::BEFORE_DOCTYPE_NAME);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.ReconsumeIn(TokenizerStateName::BEFORE_DOCTYPE_NAME);
                break;
            default:
                context.ReportParseError(ParseError::MISSING_WHITESPACE_BEFORE_DOCTYPE_NAME);
                context.ReconsumeIn(TokenizerStateName::BEFORE_DOCTYPE_NAME);
                break;
        }
    }

} // namespace HTML::TokenizerStates
