/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#attribute-value-(double-quoted)-state
     */
    void AttributeValueDoubleQuoted(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_TAG);
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        auto &attribute = context.builder.tagToken->attributes.back();

        switch (character) {
            case Unicode::QUOTATION_MARK:
                context.SwitchToState(TokenizerStateName::AFTER_ATTRIBUTE_VALUE_QUOTED);
                break;
            case Unicode::AMPERSAND:
                context.returnState = TokenizerStateName::ATTRIBUTE_VALUE_DOUBLE_QUOTED;
                context.SwitchToState(TokenizerStateName::CHARACTER_REFERENCE);
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                attribute.value += Unicode::REPLACEMENT_CHARACTER;
                break;
            default:
                attribute.value += character;
                break;
        }
    }

} // namespace HTML::TokenizerStates
