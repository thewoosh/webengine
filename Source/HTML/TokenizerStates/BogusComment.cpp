/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#bogus-comment-state
     */
    void BogusCommentState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.builder.doctypeToken.emit();
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();
        switch (character) {
            case Unicode::GREATER_THAN_SIGN:
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.commentToken.emit();
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                context.builder.commentToken->data += Unicode::REPLACEMENT_CHARACTER;
                break;
            default:
                context.builder.commentToken->data += character;
                break;
        }
    }

} // namespace HTML::TokenizerStates
