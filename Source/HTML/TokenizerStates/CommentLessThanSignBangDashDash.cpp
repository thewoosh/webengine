/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#comment-less-than-sign-bang-dash-dash-state
     */
    void CommentLessThanSignBangDashDashState(TokenizerContext &context) {
        if (context.stream->IsEOF() || context.stream->Read() == Unicode::GREATER_THAN_SIGN) {
            context.ReconsumeIn(TokenizerStateName::COMMENT_END);
            return;
        }

        context.ReportParseError(ParseError::NESTED_COMMENT);
        context.ReconsumeIn(TokenizerStateName::COMMENT_END);
    }

} // namespace HTML::TokenizerStates
