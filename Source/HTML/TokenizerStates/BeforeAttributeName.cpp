/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#before-attribute-name-state
     */
    void BeforeAttributeNameState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.SwitchToState(TokenizerStateName::AFTER_ATTRIBUTE_NAME);
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                // Ignore the character.
                break;
            case Unicode::SOLIDUS:
            case Unicode::GREATER_THAN_SIGN:
                context.ReconsumeIn(TokenizerStateName::AFTER_ATTRIBUTE_NAME);
                break;
            case Unicode::EQUALS_SIGN:
                context.ReportParseError(ParseError::UNEXPECTED_EQUALS_SIGN_BEFORE_ATTRIBUTE_NAME);
                context.builder.tagToken->attributes.emplace_back(character);
                context.SwitchToState(TokenizerStateName::ATTRIBUTE_NAME);
                break;
            default:
                context.builder.tagToken->attributes.emplace_back();
                context.ReconsumeIn(TokenizerStateName::ATTRIBUTE_NAME);
                break;
        }
    }

} // namespace HTML::TokenizerStates
