/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#data-state
     */
    void DataState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::AMPERSAND:
                context.returnState = TokenizerStateName::DATA;
                context.SwitchToState(TokenizerStateName::CHARACTER_REFERENCE);
                break;
            case Unicode::LESS_THAN_SIGN:
                context.SwitchToState(TokenizerStateName::TAG_OPEN);
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                context.EmitCharacterToken(character);
                break;
            default:
                context.EmitCharacterToken(character);
                break;
        }
    }

} // namespace HTML::TokenizerStates
