/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#attribute-value-(unquoted)-state
     */
    void AttributeValueUnquoted(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.ReportParseError(ParseError::EOF_IN_TAG);
            context.EmitEOFToken();
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        auto &attribute = context.builder.tagToken->attributes.back();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                context.SwitchToState(TokenizerStateName::BEFORE_ATTRIBUTE_NAME);
                break;
            case Unicode::AMPERSAND:
                context.returnState = TokenizerStateName::ATTRIBUTE_VALUE_DOUBLE_QUOTED;
                context.SwitchToState(TokenizerStateName::CHARACTER_REFERENCE);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.tagToken.emit();
                break;
            case Unicode::NULL_CHARACTER:
                context.ReportParseError(ParseError::UNEXPECTED_NULL_CHARACTER);
                attribute.value += Unicode::REPLACEMENT_CHARACTER;
                break;
            case Unicode::QUOTATION_MARK:
            case Unicode::APOSTROPHE:
            case Unicode::LESS_THAN_SIGN:
            case Unicode::EQUALS_SIGN:
            case Unicode::GRAVE_ACCENT:
                context.ReportParseError(ParseError::UNEXPECTED_CHARACTER_IN_UNQUOTED_ATTRIBUTE_VALUE);
                [[fallthrough]];
            default:
                attribute.value += character;
                break;
        }
    }

} // namespace HTML::TokenizerStates
