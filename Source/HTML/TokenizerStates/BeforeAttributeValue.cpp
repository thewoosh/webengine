/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TokenizerStateName.hpp"
#include "Source/Text/UnicodeCommons.hpp"

namespace HTML::TokenizerStates {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#before-attribute-value-state
     */
    void BeforeAttributeValueState(TokenizerContext &context) {
        if (context.stream->IsEOF()) {
            context.SwitchToState(TokenizerStateName::ATTRIBUTE_VALUE_UNQUOTED);
            return;
        }

        Unicode::CodePoint character = context.stream->Read();

        switch (character) {
            case Unicode::CHARACTER_TABULATION:
            case Unicode::LINE_FEED:
            case Unicode::FORM_FEED:
            case Unicode::SPACE:
                // Ignore the character.
                break;
            case Unicode::QUOTATION_MARK:
                context.SwitchToState(TokenizerStateName::ATTRIBUTE_VALUE_DOUBLE_QUOTED);
                break;
            case Unicode::APOSTROPHE:
                context.SwitchToState(TokenizerStateName::ATTRIBUTE_VALUE_SINGLE_QUOTED);
                break;
            case Unicode::GREATER_THAN_SIGN:
                context.ReportParseError(ParseError::MISSING_ATTRIBUTE_VALUE);
                context.SwitchToState(TokenizerStateName::DATA);
                context.builder.tagToken.emit();
                break;
            default:
                context.ReconsumeIn(TokenizerStateName::ATTRIBUTE_VALUE_UNQUOTED);
                break;
        }
    }

} // namespace HTML::TokenizerStates
