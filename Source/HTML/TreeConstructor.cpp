/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Tokenizer.hpp"

#include "Source/HTML/InsertionModes/Index.hpp"
#include "Source/HTML/TokenizerContext.hpp"
#include "Source/HTML/TreeConstructor.hpp"

#define HTML_TREE_CONSTRUCTOR_LOG_INSERTION_MODE_INVOCATION

namespace HTML {

    void
    TreeConstructor::InvokeStateImplementation(InsertionModeName name, const Token &token) {
        auto function = implementationMap.byName(name);

        if (function) {
#ifdef HTML_TREE_CONSTRUCTOR_LOG_INSERTION_MODE_INVOCATION
            parserContext.tokenizerContext->logger.log<LogModule::DEBUG>("[TC] Invoking insertion mode: ", name, " for token with type: ", token.type);
#endif
            function(parserContext, token);
        } else {
            parserContext.tokenizerContext->logger.log<LogModule::PARSE_ABORT>(
                    "Tree constructor was tasked with invoking an unimplemented insertion mode",
                    ": ", name, " for processing token of type: ", token.type
            );
        }
    }

    void TreeConstructor::DispatchEmit(const Token &token) {
        InvokeStateImplementation(parserContext.currentInsertionMode, token);
    }

    TreeConstructor::ImplementationMap::ImplementationMap() noexcept {
        add(InsertionModeName::INITIAL, InsertionModeImplementations::InitialMode);
        add(InsertionModeName::BEFORE_HTML, InsertionModeImplementations::BeforeHTMLMode);
        add(InsertionModeName::BEFORE_HEAD, InsertionModeImplementations::BeforeHeadMode);
        add(InsertionModeName::IN_HEAD, InsertionModeImplementations::InHeadMode);
        add(InsertionModeName::AFTER_HEAD, InsertionModeImplementations::AfterHeadMode);
        add(InsertionModeName::IN_BODY, InsertionModeImplementations::InBodyMode);

        add(InsertionModeName::TEXT, InsertionModeImplementations::TextMode);
    }

} // namespace HTML
