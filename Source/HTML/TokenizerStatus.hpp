/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>

namespace HTML {

    enum class TokenizerStatus {
        /**
         * The tokenizer has been initialized, but not yet been started.
         */
        INITIAL,

        /**
         * The tokenizer is currently running.
         */
        RUNNING,

        /**
         * The tokenizer failed (see error logs).
         */
        FAILED,

        /**
         * The tokenizer has requested to invoke behavior that hasn't been
         * implemented yet.
         */
        FAILED_TODO,

        /**
         * The tokenizer was aborted.
         */
        ABORTED,

        /**
         * The tokenizer has finished without any critical errors.
         */
        SUCCESS,
    };

    [[nodiscard]] inline constexpr const char *
    TranslateTokenizerStatusToString(TokenizerStatus status) noexcept {
        switch (status) {
            case TokenizerStatus::INITIAL:      return "initial";
            case TokenizerStatus::RUNNING:      return "running";
            case TokenizerStatus::FAILED:       return "failed-generic";
            case TokenizerStatus::FAILED_TODO:  return "failed-todo";
            case TokenizerStatus::ABORTED:      return "aborted";
            case TokenizerStatus::SUCCESS:      return "success";
            default:                            return "(illegal value)";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, TokenizerStatus status)
            noexcept(noexcept(stream << "")) {
        stream << TranslateTokenizerStatusToString(status);
        return stream;
    }

} // namespace HTML
