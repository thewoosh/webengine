/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

namespace HTML {

    /**
     * This enum holds the names of the insertion modes of the tree
     * constructor.
     */
    enum class InsertionModeName {

        INITIAL,
        BEFORE_HTML,
        BEFORE_HEAD,
        IN_HEAD,
        IN_HEAD_NOSCRIPT,
        AFTER_HEAD,
        IN_BODY,
        TEXT,
        IN_TABLE,
        IN_TABLE_TEXT,
        IN_CAPTION,
        IN_COLUMN_GROUP,
        IN_TABLE_BODY,
        IN_ROW,
        IN_CELL,
        IN_SELECT,
        IN_SELECT_IN_TABLE,
        IN_TEMPLATE,
        AFTER_BODY,
        IN_FRAMESET,
        AFTER_FRAMESET,
        AFTER_AFTER_BODY,
        AFTER_AFTER_FRAMESET

    };

    constexpr const char *
    stringOfInsertionModeName(InsertionModeName name) {
        switch (name) {
            case InsertionModeName::INITIAL: return "INITIAL";
            case InsertionModeName::BEFORE_HTML: return "BEFORE_HTML";
            case InsertionModeName::BEFORE_HEAD: return "BEFORE_HEAD";
            case InsertionModeName::IN_HEAD: return "IN_HEAD";
            case InsertionModeName::IN_HEAD_NOSCRIPT: return "IN_HEAD_NOSCRIPT";
            case InsertionModeName::AFTER_HEAD: return "AFTER_HEAD";
            case InsertionModeName::IN_BODY: return "IN_BODY";
            case InsertionModeName::TEXT: return "TEXT";
            case InsertionModeName::IN_TABLE: return "IN_TABLE";
            case InsertionModeName::IN_TABLE_TEXT: return "IN_TABLE_TEXT";
            case InsertionModeName::IN_CAPTION: return "IN_CAPTION";
            case InsertionModeName::IN_COLUMN_GROUP: return "IN_COLUMN_GROUP";
            case InsertionModeName::IN_TABLE_BODY: return "IN_TABLE_BODY";
            case InsertionModeName::IN_ROW: return "IN_ROW";
            case InsertionModeName::IN_CELL: return "IN_CELL";
            case InsertionModeName::IN_SELECT: return "IN_SELECT";
            case InsertionModeName::IN_SELECT_IN_TABLE: return "IN_SELECT_IN_TABLE";
            case InsertionModeName::IN_TEMPLATE: return "IN_TEMPLATE";
            case InsertionModeName::AFTER_BODY: return "AFTER_BODY";
            case InsertionModeName::IN_FRAMESET: return "IN_FRAMESET";
            case InsertionModeName::AFTER_FRAMESET: return "AFTER_FRAMESET";
            case InsertionModeName::AFTER_AFTER_BODY: return "AFTER_AFTER_BODY";
            case InsertionModeName::AFTER_AFTER_FRAMESET: return "AFTER_AFTER_FRAMESET";
            default:
                // unreachable, but GCC complains.
                return NULL;
        }
    }

    constexpr auto &
    operator<<(auto &stream, InsertionModeName name) {
        stream << stringOfInsertionModeName(name);
        return stream;
    }

} // namespace HTML
