/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Source/HTML/ParserContext.hpp"
#include "Source/HTML/TokenizerContext.hpp"

namespace HTML {

    void ParserContext::ReportParseError(ParseError error, const char *file, std::size_t line) {
        // TODO currentInsertionMode may not reflect the name of the actual
        //      callee (i.e. insertion mode), since reprocessing of tokens is
        //      done by the parent insertion mode, which may not set the
        //      currentInsertionMode correctly.

        tokenizerContext->logger.log<LogModule::PARSE_ERROR>(
                "Parse error: '", error, "' was triggered, currentInsertionMode: ", currentInsertionMode
        );
        tokenizerContext->logger.log<LogModule::PARSE_ERROR>(
                file, ':', line
        );
    }

    void ParserContext::ProcessUsingOtherRules(InsertionModeName name, const Token &token) {
        treeConstructor->InvokeStateImplementation(name, token);
    }

} // namespace HTML
