/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <map>

#include "Source/Text/UString.hpp"

namespace DOM {

    using AttributeList = std::map<Unicode::UString, Unicode::UString>;

} // namespace DOM
