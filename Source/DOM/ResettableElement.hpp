/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

namespace DOM {

    /**
     * Some elements can be resettable elements. These elements are often input
     * elements, which have their own reset algorithm.
     *
     * This class should act as another parent, not the actual superclass
     * Element.
     *
     * https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#concept-form-reset-control
     */
    class ResettableElement {
    public:
        virtual void
        InvokeReset() = 0;
    };

} // namespace
