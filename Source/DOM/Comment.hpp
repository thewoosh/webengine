/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/CharacterData.hpp"

namespace DOM {

    /**
     * https://dom.spec.whatwg.org/#interface-comment
     */
    struct Comment : public CharacterData {

        explicit Comment(const Unicode::UString &data) : CharacterData(NodeType::COMMENT_NODE, data) {}

    };

} // namespace DOM
