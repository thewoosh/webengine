/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/Concept/QuirksMode.hpp"
#include "Source/DOM/DocumentType.hpp"
#include "Source/DOM/Node.hpp"

namespace DOM {

    /**
     * https://dom.spec.whatwg.org/#interface-document
     */
    struct Document : public Node {

        Document(): Node(NodeType::DOCUMENT_NODE) {}

        Concept::QuirksMode quirksMode{Concept::QuirksMode::UNDEFINED};

        std::shared_ptr<DocumentType> doctype{nullptr};

    };

} // namespace DOM
