/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <optional>

#include "Source/Text/UString.hpp"
#include "Source/DOM/AttributeList.hpp"
#include "Source/DOM/Node.hpp"

#include "Concept/CustomElementState.hpp"
#include "CommonElementType.hpp"

namespace DOM {

    struct Element : public Node {

        Element() : Node(NodeType::ELEMENT_NODE) {}
        virtual ~Element() = default;

        CommonElementType commonElementType{CommonElementType::UNCOMMON};

        AttributeList attributeList{};

        //
        // https://dom.spec.whatwg.org/#concept-element-local-name
        //
        Unicode::UString namespaceValue{};
        std::optional<Unicode::UString> namespacePrefix{};
        Unicode::UString localName{};
        Concept::CustomElementState customElementState{};
        // << Custom Element Definition >>
        Unicode::UString isValue{};

        /**
         * A post-constructor initialization function. This helps with finding
         * a definition for an element without actually initializing it at that
         * stage.
         */
        virtual inline void
        Initialize(const Unicode::UString &inNamespaceValue,
                   const Unicode::UString *inNamespacePrefix,
                   const Unicode::UString &inLocalName,
                   Concept::CustomElementState inCustomElementState,
                   const Unicode::UString *inIsValue) {
            this->namespaceValue = inNamespaceValue;
            if (inNamespacePrefix != nullptr)
                this->namespacePrefix = *inNamespacePrefix;
            this->localName = inLocalName;
            this->customElementState = inCustomElementState;
            if (inIsValue != nullptr)
                this->isValue = *inIsValue;
        }

        [[nodiscard]] virtual const Unicode::UString &
        GetTagName() const noexcept {
            return localName;
        }

    };

} // namespace DOM
