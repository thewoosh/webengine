/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/Node.hpp"
#include "Source/Text/UString.hpp"

namespace DOM {

    struct DocumentType : public Node {

        DocumentType(Unicode::UString &&name,
                     Unicode::UString &&publicId,
                     Unicode::UString &&systemId) :
                 Node(NodeType::DOCUMENT_TYPE_NODE),
                 name(std::move(name)),
                 publicId(std::move(publicId)),
                 systemId(std::move(systemId))
        {}

        Unicode::UString name{};
        Unicode::UString publicId{}; // unless explicitly given the empty string
        Unicode::UString systemId{}; // unless explicitly given the empty string

    };

} // namespace DOM
