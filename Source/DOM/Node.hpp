/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <memory>

#include "Source/CSS/ComputedStyle.hpp"
#include "Source/Text/UString.hpp"

namespace DOM {

    enum class NodeType {
        INVALID_TYPE,
        ELEMENT_NODE,
        ATTRIBUTE_NODE,
        TEXT_NODE,
        CDATA_SECTION_NODE,
        ENTITY_REFERENCE_NODE,
        ENTITY_NODE,
        PROCESSING_INSTRUCTION_NODE,
        COMMENT_NODE,
        DOCUMENT_NODE,
        DOCUMENT_TYPE_NODE,
        DOCUMENT_FRAGMENT_NODE,
        NOTATION_NODE
    };

    struct Node {

        NodeType nodeType;

        explicit Node(NodeType nodeType) noexcept : nodeType(nodeType) {}
        Node(Node &&) noexcept = default;
        Node(const Node &) noexcept = default;

        virtual ~Node() = default;

        std::vector<std::shared_ptr<Node>> childNodes{};

        CSS::ComputedStyle computedStyle{};

        // non-normative
        inline void
        append(std::shared_ptr<Node> node) {
            childNodes.push_back(node);
        }

    };

} // namespace DOM
