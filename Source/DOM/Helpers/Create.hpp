/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <memory>

#include "Source/DOM/Document.hpp"
#include "Source/DOM/Element.hpp"

#include "Source/HTML/Elements/HTMLBodyElement.hpp"
#include "Source/HTML/Elements/HTMLFormElement.hpp"
#include "Source/HTML/Elements/HTMLHeadElement.hpp"
#include "Source/HTML/Elements/HTMLHtmlElement.hpp"
#include "Source/HTML/Elements/HTMLTemplateElement.hpp"

#include "Source/Text/FastUStrings.hpp"
#include "Source/Text/Namespaces.hpp"

namespace DOM::Helpers {

    [[nodiscard]] inline std::shared_ptr<DOM::Element>
    FindElementInterface(const Unicode::UString &localName,
                         const Unicode::UString &theNamespace) {
        if (Unicode::FastUStrings::EqualsASCII(theNamespace, ::Text::Namespaces::HTML)) {
            if (Unicode::FastUStrings::EqualsASCII(localName, "body")) {
                return std::make_unique<HTML::HTMLBodyElement>();
            }
            if (Unicode::FastUStrings::EqualsASCII(localName, "form")) {
                return std::make_unique<HTML::HTMLFormElement>();
            }
            if (Unicode::FastUStrings::EqualsASCII(localName, "head")) {
                return std::make_unique<HTML::HTMLHeadElement>();
            }
            if (Unicode::FastUStrings::EqualsASCII(localName, "html")) {
                return std::make_unique<HTML::HTMLHtmlElement>();
            }
            if (Unicode::FastUStrings::EqualsASCII(localName, "template")) {
                return std::make_unique<HTML::HTMLTemplateElement>();
            }
        }

        return std::make_shared<DOM::Element>();
    }

    [[nodiscard]] inline std::shared_ptr<DOM::Element>
    CreateElement(std::shared_ptr<DOM::Document> document,
                  const Unicode::UString &localName,
                  const Unicode::UString &theNamespace,
                  const Unicode::UString *prefix=nullptr,
                  const Unicode::UString *is=nullptr,
                  bool synchronousCustomElementsFlag=false) {
        static_cast<void>(document);

        //
        // TODO custom element definitions
        //
        static_cast<void>(synchronousCustomElementsFlag);


        std::shared_ptr<DOM::Element> element = FindElementInterface(localName, theNamespace);
        element->Initialize(theNamespace, prefix, localName, Concept::CustomElementState::UNCUSTOMIZED, is);

        if (Unicode::FastUStrings::EqualsASCII(theNamespace, ::Text::Namespaces::HTML) && (
                /* TODO check if local name is a valid custom name || */
                is != nullptr
        )) {
            element->customElementState = Concept::CustomElementState::UNDEFINED;
        }

        return element;
    }

} // namespace DOM::Helpers
