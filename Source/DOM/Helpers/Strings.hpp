/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 *
 * This file adds stringification and stream support for common DOM objects
 * and types.
 */

#pragma once

#include <ostream>

#include "Source/Base/Terminate.hpp"
#include "Source/DOM/Node.hpp"

namespace DOM {

    namespace Helpers {

        [[nodiscard]] inline constexpr const char *
        stringOfNodeType(::DOM::NodeType nodeType) {
            switch (nodeType) {
                case NodeType::INVALID_TYPE: return "INVALID_TYPE";
                case NodeType::ELEMENT_NODE: return "ELEMENT_NODE";
                case NodeType::ATTRIBUTE_NODE: return "ATTRIBUTE_NODE";
                case NodeType::TEXT_NODE: return "TEXT_NODE";
                case NodeType::CDATA_SECTION_NODE: return "CDATA_SECTION_NODE";
                case NodeType::ENTITY_REFERENCE_NODE: return "ENTITY_REFERENCE_NODE";
                case NodeType::ENTITY_NODE: return "ENTITY_NODE";
                case NodeType::PROCESSING_INSTRUCTION_NODE: return "PROCESSING_INSTRUCTION_NODE";
                case NodeType::COMMENT_NODE: return "COMMENT_NODE";
                case NodeType::DOCUMENT_NODE: return "DOCUMENT_NODE";
                case NodeType::DOCUMENT_TYPE_NODE: return "DOCUMENT_TYPE_NODE";
                case NodeType::DOCUMENT_FRAGMENT_NODE: return "DOCUMENT_FRAGMENT_NODE";
                case NodeType::NOTATION_NODE: return "NOTATION_NODE";
                default: Base::InvokeTerminateForDefaultCase();
            }
        }

    } // namespace Helpers

    inline std::ostream &
    operator<<(std::ostream &stream, NodeType nodeType) {
        stream << Helpers::stringOfNodeType(nodeType);
        return stream;
    }

} // namespace DOM

