/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/DocumentType.hpp"
#include "Source/DOM/Node.hpp"

namespace DOM::Concept {

    /**
     * https://dom.spec.whatwg.org/#concept-document-quirks
     */
    enum class QuirksMode {

        UNDEFINED,
        NO_QUIRKS,
        QUIRKS,
        LIMITED_QUIRKS

    };

} // namespace DOM::Concept
