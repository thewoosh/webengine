/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/DocumentType.hpp"
#include "Source/DOM/Node.hpp"

namespace DOM::Concept {

    /**
     * https://dom.spec.whatwg.org/#concept-element-custom-element-state
     */
    enum class CustomElementState {

        UNDEFINED,
        FAILED,
        UNCUSTOMIZED,
        PRECUSTOMIZED,
        CUSTOM

    };

} // namespace DOM::Concept
