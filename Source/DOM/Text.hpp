/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/CharacterData.hpp"

namespace DOM {

    /**
     * https://dom.spec.whatwg.org/#interface-text
     */
    struct Text : public CharacterData {

        explicit Text(const Unicode::UString &data) : CharacterData(NodeType::TEXT_NODE, data) {}

    };

} // namespace DOM
