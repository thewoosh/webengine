/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include "Source/DOM/Node.hpp"
#include "Source/Text/UString.hpp"

namespace DOM {

    /**
     * https://dom.spec.whatwg.org/#characterdata
     */
    struct CharacterData : public Node {

        Unicode::UString data;

        CharacterData(NodeType nodeType, const Unicode::UString &data)
            : Node(nodeType), data(data)
        {}

    };

} // namespace DOM
