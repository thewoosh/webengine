/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <exception> // std::terminate
#include <iostream> // std::cerr

namespace Base {

    /**
     * Ultimately, we want to be able to warn asap (i.e. at compile-time). This
     * is only possible if the underlying function that has a switch statement
     * is constexpr.
     *
     * C++20's std::is_constant_evaluated helps with this, but I can't find a
     * way to notify the compiler that something is wrong.
     *
     * static_assert is evaluated at the parse-time of *this* function, not the
     * callee function. Otherwise, we could use std::is_constant_evaluated() to
     * detect whether or not the branch can be constexpr (compile-time) or not
     * (runtime std::terminate).
     *
     * Ultimately, I want to do something along the lines of:
     *
     * if (std::is_constant_evaluated()) {
     *     static_warning("Default switch case reached in constexpr context");
     * } else {
     *     std::terminate();
     * }
     *
     * Resources
     * (1) https://gist.github.com/Som1Lse/5309b114accc086d24b842fd803ba9d2
     * (2) https://en.cppreference.com/w/cpp/types/is_constant_evaluated
     */
    [[noreturn]] inline void
    InvokeTerminateForDefaultCase() {
        std::cerr << "switch statement shouldn't have reached default label\n";
        std::terminate();
    }

}

/**
 * When a switch statement provides control over an enum specifier, and all enum
 * identifiers are handled, in no case should the default case be used.
 *
 * In Clang we can omit the default case, but GCC has the -Werror=return-type
 * warning. This is less useful, since Clang tells us to add either a switch
 * label or a default case, in the event that a new enum identifier is added.
 */
#define SWITCH_DEFAULT_UNREACHABLE() Base::InvokeTerminateForDefaultCase();
