/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/CSS/PropertyMapper.hpp"
#include "Source/DOM/Forward.hpp"

namespace CSS {

    class Context {
        PropertyMapper m_propertyMapper;

        void
        addInheritedProperties(DOM::Node *, DOM::Node *parent) const noexcept;

        [[nodiscard]] bool
        applyToNode(DOM::Node *node, DOM::Node *parent) noexcept;

        [[nodiscard]] bool
        computeStyles(DOM::Node *) noexcept;

    public:
        [[nodiscard]] bool
        applyStyle(DOM::Document *) noexcept;
    };

} // namespace CSS
