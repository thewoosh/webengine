/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/CSS/Types/Display.hpp"
#include "Source/Paint/Color.hpp"

namespace CSS {

    struct ComputedStyle {
        Types::Display display{};
        Paint::Color backgroundColor{};
        Paint::Color textColor{Paint::BasicColors::BLACK};

        Paint::Color borderLeftColor{};
        Paint::Color borderRightColor{};
        Paint::Color borderTopColor{};
        Paint::Color borderBottomColor{};

        float paddingLeft{};
        float paddingRight{};
        float paddingTop{};
        float paddingBottom{};
    };

} // namespace CSS
