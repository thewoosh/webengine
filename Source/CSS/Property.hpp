/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Text/UString.hpp"

namespace CSS {

    struct Property {
        Unicode::UString name;
        Unicode::UString value;

        [[nodiscard]] Property() = default;
        [[nodiscard]] Property(Property &&) = default;
        [[nodiscard]] Property(const Property &) = default;

        Property &operator=(Property &&) = default;
        Property &operator=(const Property &) = default;

        [[nodiscard]] inline
        Property(const Unicode::UString &name,
                 const Unicode::UString &value) noexcept
                : name(name)
                , value(value) {
        }

        [[nodiscard]] inline
        Property(Unicode::UString &&name,
                 Unicode::UString &&value) noexcept
                : name(name)
                , value(value) {
        }
    };

} // namespace CSS
