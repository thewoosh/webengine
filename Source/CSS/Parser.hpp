/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/CSS/Property.hpp"
#include "Source/Text/UString.hpp"

namespace CSS {

    class Parser {
        using StringIterator = decltype(Unicode::UString().cbegin());

        [[nodiscard]] bool
        SkipWhitespace(StringIterator &, StringIterator end);

        [[nodiscard]] Unicode::UString
        ParsePropertyName(StringIterator &, StringIterator end);

        [[nodiscard]] Unicode::UString
        ParsePropertyValue(StringIterator &, StringIterator end);

    public:
        [[nodiscard]] std::vector<Property>
        ParseAttributeDeclarations(const Unicode::UString &);

        /**
         * https://www.w3.org/TR/CSS21/syndata.html#whitespace
         */
        [[nodiscard]] inline constexpr static bool
        IsCSSWhitespace(Unicode::CodePoint codePoint) noexcept {
            return codePoint == Unicode::SPACE ||
                    codePoint == Unicode::CHARACTER_TABULATION ||
                    codePoint == Unicode::LINE_FEED ||
                    codePoint == Unicode::CARRIAGE_RETURN ||
                    codePoint == Unicode::FORM_FEED;
        }
    };

} // namespace CSS
