/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/CSS/PropertyMapper.hpp"

#include <cstdlib>

#include "Source/CSS/ComputedStyle.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace CSS {

    PropertyMapper::PropertyMapper()
            : map{
                    {Unicode::UString("color"), &PropertyMapper::ApplyPropertyColor},
                    {Unicode::UString("background-color"), &PropertyMapper::ApplyPropertyBackgroundColor},
                    {Unicode::UString("padding"), &PropertyMapper::ApplyPropertyPadding}
            }
            , basicColorsMap{
                    {Unicode::UString("black"), Paint::BasicColors::BLACK},
                    {Unicode::UString("silver"), Paint::BasicColors::SILVER},
                    {Unicode::UString("gray"), Paint::BasicColors::GRAY},
                    {Unicode::UString("white"), Paint::BasicColors::WHITE},
                    {Unicode::UString("maroon"), Paint::BasicColors::MAROON},
                    {Unicode::UString("red"), Paint::BasicColors::RED},
                    {Unicode::UString("purple"), Paint::BasicColors::PURPLE},
                    {Unicode::UString("fuchsia"), Paint::BasicColors::FUCHSIA},
                    {Unicode::UString("green"), Paint::BasicColors::GREEN},
                    {Unicode::UString("lime"), Paint::BasicColors::LIME},
                    {Unicode::UString("olive"), Paint::BasicColors::OLIVE},
                    {Unicode::UString("yellow"), Paint::BasicColors::YELLOW},
                    {Unicode::UString("navy"), Paint::BasicColors::NAVY},
                    {Unicode::UString("blue"), Paint::BasicColors::BLUE},
                    {Unicode::UString("teal"), Paint::BasicColors::TEAL},
                    {Unicode::UString("aqua"), Paint::BasicColors::AQUA},
            }{
    }

    bool
    PropertyMapper::Apply(const Property &property, ComputedStyle *computedStyle) noexcept {
        const auto it = map[property.name];

        if (!it)
            return false;

        return it(this, property, computedStyle);
    }

    bool PropertyMapper::SetColorProperty(const Unicode::UString &value, Paint::Color *dest) {
        const auto basicColorIterator = basicColorsMap.find(value);

        if (basicColorIterator != std::cend(basicColorsMap)) {
            *dest = basicColorIterator->second;
            return true;
        }

        return false;
    }

    bool PropertyMapper::SetLengthProperty(const Unicode::UString &value, float *dest) {
        *dest = 0;
        auto it = std::cbegin(value);
        for (; it != std::cend(value); ++it) {
            if (!Unicode::IsDigit(*it))
                break;
            *dest *= 10;
            *dest += *it - Unicode::DIGIT_ZERO;
        }

        std::cout << __FUNCTION__ << " \"" << value << "\" to float is " << *dest << '\n';

        Unicode::UString unit(it, std::cend(value));
        if (Unicode::FastUStrings::EqualsASCII(unit, "px")) {
            return true;
        }

        // TODO support other units
        std::cout << __FUNCTION__ << " Unknown unit: " << unit << '\n';

        return false;
    }

    bool PropertyMapper::ApplyPropertyColor(const Property &property,
                                            ComputedStyle *computedStyle) {
        return SetColorProperty(property.value, &computedStyle->textColor);
    }

    bool PropertyMapper::ApplyPropertyBackgroundColor(const Property &property,
                                                      ComputedStyle *computedStyle) {
        return SetColorProperty(property.value, &computedStyle->backgroundColor);
    }

    bool PropertyMapper::ApplyPropertyPadding(const Property &property,
                                              ComputedStyle *computedStyle) {
        if (!SetLengthProperty(property.value, &computedStyle->paddingLeft))
            return false;

        computedStyle->paddingRight  = computedStyle->paddingLeft;
        computedStyle->paddingTop    = computedStyle->paddingLeft;
        computedStyle->paddingBottom = computedStyle->paddingLeft;

        return  true;
    }

} // namespace CSS
