/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/CSS/Context.hpp"

#include "Source/CSS/Parser.hpp"
#include "Source/DOM/Document.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/DOM/Node.hpp"
#include "Source/Text/FastUStrings.hpp"

namespace CSS {

    void
    Context::addInheritedProperties(DOM::Node *node, DOM::Node *parent) const noexcept {
        node->computedStyle.textColor = parent->computedStyle.textColor;
    }

    bool
    Context::applyStyle(DOM::Document *document) noexcept {
        return applyToNode(document, nullptr);
    }

    bool
    Context::computeStyles(DOM::Node *node) noexcept {
        node->computedStyle.display = CSS::Types::Display::INLINE;

        if (node->nodeType == DOM::NodeType::ELEMENT_NODE) {
            auto *element = static_cast<DOM::Element *>(node);

            if (element->localName == Unicode::UString("html") ||
                element->localName == Unicode::UString("body") ||
                element->localName == Unicode::UString("div")) {
                // TODO store these names in the UA stylesheet.
                node->computedStyle.display = CSS::Types::Display::BLOCK;
            }

            if (element->localName == Unicode::UString("head") ||
                element->localName == Unicode::UString("h2")) {
                node->computedStyle.display = CSS::Types::Display::NONE;
            }
        }

        return true;
    }

    bool
    Context::applyToNode(DOM::Node *node, DOM::Node *parent) noexcept {
        if (parent != nullptr)
            addInheritedProperties(node, parent);

        if (!computeStyles(node))
            return false;

        if (parent) {
            // copy inheritable properties here
            node->computedStyle.textColor = parent->computedStyle.textColor;
        }

        CSS::Parser parser{};
        if (node->nodeType == DOM::NodeType::ELEMENT_NODE) {
            auto *element = static_cast<DOM::Element *>(node);

            for (const auto &attribute : element->attributeList) {
                if (!Unicode::FastUStrings::EqualsASCII(attribute.first, "style"))
                    continue;

                auto properties = parser.ParseAttributeDeclarations(attribute.second);

                for (const auto &property : properties) {
                    bool success = m_propertyMapper.Apply(property, &element->computedStyle);

                    if (!success) {
                        std::cout << "[CSS] Couldn't apply property with name \"" << property.name << "\"\n";
                    }
                }
            }
        }

        for (auto &child : node->childNodes)
            if (!applyToNode(child.get(), node))
                return false;
        return true;
    }

} // namespace CSS
