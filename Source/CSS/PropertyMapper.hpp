/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <map>

#include "Source/CSS/Property.hpp"
#include "Source/Paint/Color.hpp"
#include "Source/Text/UString.hpp"

namespace CSS {

    struct ComputedStyle;

    class PropertyMapper {
        using FunctionSignature = std::function<bool(PropertyMapper *, const Property &, ComputedStyle *)>;
        std::map<Unicode::UString, FunctionSignature> map;

        /**
         * Colors can be identifiers, known as basic colors. This map maps the
         * identifiers to the Paint::BasicColors
         *
         * https://www.w3.org/TR/css-color-3/#html4
         */
        std::map<Unicode::UString, Paint::Color> basicColorsMap;

        [[nodiscard]] bool SetColorProperty(const Unicode::UString &, Paint::Color *);
        [[nodiscard]] bool SetLengthProperty(const Unicode::UString &, float *);

        [[nodiscard]] bool ApplyPropertyColor(const Property &, ComputedStyle *);
        [[nodiscard]] bool ApplyPropertyBackgroundColor(const Property &, ComputedStyle *);
        [[nodiscard]] bool ApplyPropertyPadding(const Property &, ComputedStyle *);

    public:
        PropertyMapper();

        [[nodiscard]] bool
        Apply(const Property &, ComputedStyle *) noexcept;
    };

} // namespace CSS
