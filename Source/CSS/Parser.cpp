/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/CSS/Parser.hpp"

#include <iostream>
#include <stdexcept>

#define PARSE_ERROR(func, text) \
        std::cout << "[CSSParser] Parse error in " << func << ": " << text << '\n';

namespace CSS {

    bool Parser::SkipWhitespace(StringIterator &iterator, StringIterator end) {
        for (; iterator != end; ++iterator) {
            if (!IsCSSWhitespace(*iterator)) {
                return true;
            }
        }

        return false;
    }

    Unicode::UString
    Parser::ParsePropertyName(StringIterator &iterator, StringIterator end) {
        const StringIterator begin = iterator;

        for (; iterator != end; ++iterator) {
            if (IsCSSWhitespace(*iterator) || *iterator == Unicode::COLON) {
                return Unicode::UString(begin, iterator);
            }
        }

        PARSE_ERROR("ParsePropertyName", "no name found")
        // TODO parse error
        return {};
    }

    Unicode::UString
    Parser::ParsePropertyValue(StringIterator &iterator, StringIterator end) {
        // NOTE: We don't have to check for a ; in a string "" or '', since it
        // isn't allowed there anyway.

        const StringIterator begin = iterator;

        for (; iterator != end; ++iterator) {
            if (*iterator == Unicode::SEMICOLON) {
                return Unicode::UString(begin, iterator);
            }
        }

        return Unicode::UString(begin, end);
    }

    std::vector<Property>
    Parser::ParseAttributeDeclarations(const Unicode::UString &input) {
        std::vector<Property> vector{};

        auto iterator = std::cbegin(input);
        const auto endIterator = std::cend(input);

        while (true) {
            if (!SkipWhitespace(iterator, endIterator)) {
                break;
            }

            auto name = ParsePropertyName(iterator, endIterator);
            if (name.length() == 0) {
                break;
            }

            if (!SkipWhitespace(iterator, endIterator)) {
                PARSE_ERROR("ParseAttributeDeclarations", "whitespace between name and colon EOF")
                break;
            }

            if (*iterator++ != Unicode::COLON) {
                PARSE_ERROR("ParseAttributeDeclarations", "no colon found")
                break;
            }

            if (!SkipWhitespace(iterator, endIterator)) {
                PARSE_ERROR("ParseAttributeDeclarations", "whitespace between colon and value EOF")
                break;
            }

            auto value = ParsePropertyValue(iterator, endIterator);
            if (value.length() == 0) {
                PARSE_ERROR("ParseAttributeDeclarations", "value is empty")
                break;
            }

            if (iterator != endIterator) {
                if (*iterator != Unicode::SEMICOLON) {
                    PARSE_ERROR("ParseAttributeDeclarations", (std::string("semicolon expected; got: ") + static_cast<char>(*iterator)));
                    break;
                }

                ++iterator;
            }

            vector.emplace_back(std::move(name), std::move(value));
        }

        return vector;
    }

} // namespace CSS
