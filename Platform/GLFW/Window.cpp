/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Graphics/Window.hpp"

#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace Graphics {

    Window::Window() noexcept {
        if (glfwInit() == GLFW_FALSE) {
            const char *description{nullptr};
            glfwGetError(&description);
            std::cerr << "[ERROR] GLFW(init): " << description << '\n';
            return;
        }

        window = glfwCreateWindow(1280, 720, "WebEngine", nullptr, nullptr);

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

        if (!window) {
            const char *description{nullptr};
            glfwGetError(&description);
            std::cerr << "[WARNING] GLFW(create-window): " << description << '\n';
        }

        glfwMakeContextCurrent(window);

        GLenum err = glewInit();
        if (err != GLEW_OK) {
            std::cerr << "[ERROR] GLEW (init): " << glewGetErrorString(err) << "\n";
            glfwTerminate();
            return;
        }

        //
        // GLEW sanity check
        // If GLEW is initialized correctly, these function pointers should be set.
        //

        if (glCreateProgram == nullptr) {
            std::cerr << "[SEVERE] GLEW sanity test failed!\n";
            glfwDestroyWindow(window);
            window = nullptr;
            glfwTerminate();
            return;
        }
    }

    [[nodiscard]]
    Window::operator bool() {
        return window != nullptr;
    }

    Window::~Window() noexcept {
        if (window) {
            glfwDestroyWindow(window);
        }

        glfwTerminate();
    }

    bool Window::IsCloseRequested() {
        glfwPollEvents();
        return glfwWindowShouldClose(window);
    }

    void Window::SwapBuffers() {
        glfwSwapBuffers(window);
    }

    IntDimensions
    Window::Size() const {
        IntDimensions size{};

        glfwGetWindowSize(window, &size.width, &size.height);

        return size;
    }

} // namespace Graphics