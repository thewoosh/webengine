# This module enables the compiler diagnostics.

# Warnings
add_compile_options(
        -Wall # Decent set of warnings
        -Wextra # Ditto
        -Wpedantic # Ensure standards compliance

        -Wformat=2 # std::printf safety

        # Warnings from lefticus
        -Wmisleading-indentation # warn if indentation implies blocks where blocks do not exist
        -Wduplicated-cond # warn if if / else chain has duplicated conditions
        -Wduplicated-branches # warn if if / else branches have duplicated code
        -Wlogical-op # warn about logical operations being used where bitwise were probably wanted
        -Wuseless-cast # warn if you perform a cast to the same type
        -Wshadow # warn the user if a variable declaration shadows one from a parent context
        -Wnon-virtual-dtor # warn the user if a class with virtual functions has a non-virtual destructor. This helps
        # catch hard to track down memory errors
        -Wold-style-cast # warn for c-style casts
        -Wcast-align # warn for potential performance problem casts
        -Wunused # warn on anything being unused
        -Woverloaded-virtual # warn if you overload (not override) a virtual function
        -Wpedantic # warn if non-standard C++ is used
        -Wconversion # warn on type conversions that may lose data
        -Wsign-conversion # warn on sign conversions
        -Wnull-dereference # warn if a null dereference is detected
        -Wdouble-promotion # warn if float is implicit promoted to double
)

# Set warnings as errors
add_compile_options(
        -Werror
)

# Disable some GTest warnings
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    target_compile_options(gtest PUBLIC
            -Wno-maybe-uninitialized
    )
endif()