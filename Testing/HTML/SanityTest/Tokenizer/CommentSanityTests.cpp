/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Testing/TestSymbols.hpp"
#include "Testing/HTML/SanityTest/SanityTokenizer.hpp"

template<typename...Types>
void runTest(const char *input,
             const char *output,
             std::vector<std::unique_ptr<HTML::Token>> tokens={},
             Types...additional) {
    auto comment = std::make_unique<HTML::CommentToken>();
    comment->data = Unicode::UString(output);

    tokens.push_back(std::move(comment));
    (tokens.push_back(std::move(additional)), ...);
    tokens.push_back(std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE));

    RunTokenizerSanity(input, tokens);
}

TEST(CommentSanityTests, AllowedTest) {
    runTest("<!-- Hello, world! -->",
               " Hello, world! ");
    runTest("<!--My favorite operators are > and <!-->",
            "My favorite operators are > and <!");
}

/**
 * Newlines inside the comment-ending-text ("-->") isn't allowed, but Firefox
 * used to produce weird behavior (@garethheyes)
 */
TEST(CommentSanityTests, NewLineTest) {
    runTest(R"(<!-- --!


>)", R"( --!


>)");
}

TEST(CommentSanityTests, WeirdEndingTest) {
    runTest(R"(<!-->ok-->)",
            "",
            {},
            std::make_unique<HTML::CharacterToken>('o'),
            std::make_unique<HTML::CharacterToken>('k'),
            std::make_unique<HTML::CharacterToken>('-'),
            std::make_unique<HTML::CharacterToken>('-'),
            std::make_unique<HTML::CharacterToken>('>')
    );
}

TEST(CommentSanityTests, AlternativeEndingTest) {
    runTest(R"(<!-- Hello! !--!>1)",
            " Hello! !",
            {},
            std::make_unique<HTML::CharacterToken>('1')
    );
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
