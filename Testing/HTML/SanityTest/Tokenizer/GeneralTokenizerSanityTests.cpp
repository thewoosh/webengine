/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Testing/TestSymbols.hpp"

#include "Source/HTML/Tokenizer.hpp"

constexpr auto firstStateName = static_cast<std::size_t>(HTML::TokenizerStateName::DATA);
constexpr auto lastStateName = static_cast<std::size_t>(HTML::TokenizerStateName::IN_FOREIGN_CONTENT);

TEST(HTMLTokenizerGeneral, ImplementationMapCheck) {
    HTML::Tokenizer::ImplementationMap map{};
    for (auto name = firstStateName; name <= lastStateName; name++) {
        const auto enumifiedName = static_cast<HTML::TokenizerStateName>(name);
        auto &func = map.byName(enumifiedName);


//        EXPECT_TRUE(func)
        if (!func) std::cout
            << "no function definition for: " << HTML::stringOfTokenizerStringName(enumifiedName)
            << std::endl
        ;
    }
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}