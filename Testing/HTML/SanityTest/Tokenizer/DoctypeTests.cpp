/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Testing/TestSymbols.hpp"
#include "Testing/HTML/SanityTest/SanityTokenizer.hpp"

void runTest(const char *input,
             bool forceQuirks,
             const char *name,
             const char *publicIdentifier=nullptr,
             const char *systemIdentifier=nullptr) {
    auto doctype = std::make_unique<HTML::DoctypeToken>();
    doctype->forceQuirks = forceQuirks;
    doctype->name = Unicode::UString(name);
    if (publicIdentifier)
        doctype->publicIdentifier = Unicode::UString(publicIdentifier);
    if (systemIdentifier)
        doctype->systemIdentifier = Unicode::UString(systemIdentifier);

    std::vector<std::unique_ptr<HTML::Token>> tokens;
    tokens.push_back(std::move(doctype));
    tokens.push_back(std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE));

    RunTokenizerSanity(input, tokens);
}

TEST(DoctypeTests, SimpleDoctype) {
    runTest("<!DOCTYPE html>",
            false,
            "html");
}

TEST(DoctypeTests, HTML4_01_Strict) {
    runTest(R"(<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
                       "http://www.w3.org/TR/html4/strict.dtd">)",
            false,
            "html",
            "-//W3C//DTD HTML 4.01//EN",
            "http://www.w3.org/TR/html4/strict.dtd"
    );
}

TEST(DoctypeTests, HTML4_01_Transitional) {
    runTest(R"(<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                       "http://www.w3.org/TR/html4/loose.dtd">)",
            false,
            "html",
            "-//W3C//DTD HTML 4.01 Transitional//EN",
            "http://www.w3.org/TR/html4/loose.dtd"
    );
}

TEST(DoctypeTests, HTML4_01_Frameset) {
    runTest(R"(<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
                       "http://www.w3.org/TR/html4/frameset.dtd">)",
            false,
            "html",
            "-//W3C//DTD HTML 4.01 Frameset//EN",
            "http://www.w3.org/TR/html4/frameset.dtd"
    );
}

TEST(DoctypeTests, SystemIdentifier) {
    runTest(R"(<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">)",
            false,
            "html",
            nullptr,
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
    );
}

TEST(DoctypeTests, FormalPublicIdentifier) {
    runTest(R"(<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">)",
            false,
            "html",
            "-//W3C//DTD HTML 4.01//EN"
    );
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
