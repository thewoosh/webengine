/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <fstream>
#include <memory>
#include <vector>

#include "Source/Data/IteratorStream.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/HTML/Tokenizer.hpp"
#include "Source/Text/Encoding/Type/UTF8.hpp"
#include "Source/Text/UString.hpp"

namespace std {
    inline std::ostream &
    operator<<(std::ostream &stream, const std::optional<Unicode::UString> &string) {
        if (string.has_value()) {
            stream << '"' << string.value() << '"';
        } else {
            stream << "missing";
        }
        return stream;
    }
} // namespace std

static inline HTML::HTMLLogger
ConstructDebugLogger() {
#ifdef TESTING_ENABLE_DEBUG_LOGGING
    return HTML::HTMLLogger(std::cout);
#else
    return HTML::HTMLLogger(std::make_unique<std::ofstream>(
#ifdef _MSC_VER
            "NUL"
#else
            "/dev/null"
#endif
    ));
#endif
}

template <std::size_t Size>
[[nodiscard]] inline std::shared_ptr<DOM::Document>
RunTreeConstructorSanity(const std::array<std::unique_ptr<HTML::Token>, Size> &tokens) {
    HTML::TokenizerContext tokenizerContext{
            {},
            ConstructDebugLogger()
    };

    HTML::TreeConstructor treeConstructor(&tokenizerContext);

    for (const auto &token : tokens) {
        treeConstructor.DispatchEmit(*token);
    }

    return treeConstructor.Context().document;
}
