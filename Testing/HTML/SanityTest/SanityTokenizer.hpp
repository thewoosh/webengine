/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

#include <fstream>
#include <memory>
#include <vector>

#include "Source/Data/IteratorStream.hpp"
#include "Source/HTML/Token.hpp"
#include "Source/HTML/Tokenizer.hpp"
#include "Source/Text/Encoding/Type/UTF8.hpp"
#include "Source/Text/UString.hpp"

namespace std {
    inline std::ostream &
    operator<<(std::ostream &stream, const std::optional<Unicode::UString> &string) {
        if (string.has_value()) {
            stream << '"' << string.value() << '"';
        } else {
            stream << "missing";
        }
        return stream;
    }
} // namespace std

static inline HTML::HTMLLogger
ConstructDebugLogger() {
#ifdef TESTING_ENABLE_DEBUG_LOGGING
    return HTML::HTMLLogger(std::cout);
#else
    return HTML::HTMLLogger(std::make_unique<std::ofstream>(
#ifdef _MSC_VER
            "NUL"
#else
            "/dev/null"
#endif
    ));
#endif
}

/**
 * This function will run a shallow sanity test for the HTML Tokenizer.
 *
 * Parameters:
 * 1. input
 *    The input string to tokenize.
 * 2. expectedOutput
 *    The list of expected tokens.
 *
 */
inline void
RunTokenizerSanity(std::string_view input,
                   const std::vector<std::unique_ptr<HTML::Token>> &expectedOutput) {
    Data::StringStream<std::uint8_t> rawTextStream(input);
    Encoding::UTF8<std::uint8_t> utf{};
    auto vec = utf.DecodeStream(&rawTextStream);
    std::shared_ptr<Data::Stream<Unicode::CodePoint>> tokenizerInputStream = std::make_shared<Data::IteratorStream<Unicode::CodePoint, decltype(vec)::const_iterator>>(
            std::cbegin(vec), std::cend(vec)
    );

    HTML::Tokenizer tokenizer(tokenizerInputStream, ConstructDebugLogger());
    tokenizer.Run();

    EXPECT_EQ(tokenizer.Context().tokenList.size(), expectedOutput.size()) << "token list size is incorrect";

    for (std::size_t i = 0; i < std::min(tokenizer.Context().tokenList.size(), expectedOutput.size()); i++) {
        const auto &outToken = tokenizer.Context().tokenList[i];
        const auto &expToken = expectedOutput[i];

        EXPECT_EQ(outToken->type, expToken->type) << "mismatched token types on token with id " << i;
        if (outToken->type != expToken->type)
            continue;

        if (outToken->type == HTML::TokenType::DOCTYPE) {
            const auto *outDoctype = reinterpret_cast<const HTML::DoctypeToken *>(outToken.get());
            const auto *expDoctype = reinterpret_cast<const HTML::DoctypeToken *>(expToken.get());

            EXPECT_EQ(outDoctype->forceQuirks, expDoctype->forceQuirks)
                << "mismatched force-quirks flag on DOCTYPE token with id " << i;
            EXPECT_EQ(outDoctype->name, expDoctype->name)
                    << "mismatched DOCTYPE name on token with id " << i;
            EXPECT_EQ(outDoctype->publicIdentifier, expDoctype->publicIdentifier)
                                << "mismatched DOCTYPE public identifier on token with id " << i;
            EXPECT_EQ(outDoctype->systemIdentifier, expDoctype->systemIdentifier)
                                << "mismatched DOCTYPE system identifier on token with id " << i;
        }

        if (outToken->type == HTML::TokenType::COMMENT) {
            EXPECT_EQ(reinterpret_cast<const HTML::CommentToken *>(outToken.get())->data,
                      reinterpret_cast<const HTML::CommentToken *>(expToken.get())->data)
                      << "mismatched data on comment token with id " << i;
        }
    }
}
