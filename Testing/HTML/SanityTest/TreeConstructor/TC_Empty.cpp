/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <array>
#include <memory>

#include "Testing/PrintDOMNode.hpp"
#include "Testing/TestSymbols.hpp"

#include "Testing/HTML/SanityTest/SanityTreeConstructor.hpp"

void VerifyAsIfEmpty(const std::shared_ptr<DOM::Document> &document,
                     bool haveDoctype,
                     const Unicode::UString &doctypeName = Unicode::UString()) {
    std::shared_ptr<HTML::HTMLElement> html;

    if (haveDoctype) {
        ASSERT_NE(document->doctype, nullptr);
        EXPECT_EQ(document->doctype->name, doctypeName);

        ASSERT_EQ(std::size(document->childNodes), 2);
        EXPECT_EQ(document->childNodes[0]->nodeType, DOM::NodeType::DOCUMENT_TYPE_NODE);
        EXPECT_EQ(document->childNodes[0], document->doctype);

        html = std::static_pointer_cast<HTML::HTMLElement>(document->childNodes[1]);
    } else {
        EXPECT_EQ(document->doctype, nullptr);
        ASSERT_EQ(std::size(document->childNodes), 1);

        html = std::static_pointer_cast<HTML::HTMLElement>(document->childNodes[0]);
    }

    ASSERT_GE(std::size(html->childNodes), 2);

    EXPECT_EQ(html->commonElementType, DOM::CommonElementType::HTML);

    ASSERT_EQ(html->childNodes[0]->nodeType, DOM::NodeType::ELEMENT_NODE);
    ASSERT_EQ(html->childNodes[1]->nodeType, DOM::NodeType::ELEMENT_NODE);

    auto head = std::static_pointer_cast<HTML::HTMLHeadElement>(html->childNodes[0]);
    auto body = std::static_pointer_cast<HTML::HTMLElement>(html->childNodes[1]);

    EXPECT_EQ(head->commonElementType, DOM::CommonElementType::HEAD);
    EXPECT_EQ(body->commonElementType, DOM::CommonElementType::BODY);
    EXPECT_TRUE(std::empty(head->childNodes));
    EXPECT_TRUE(std::empty(body->childNodes));
}

TEST(SimpleTest, Empty) {
    std::array<std::unique_ptr<HTML::Token>, 1> array {
        std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE)
    };
    std::shared_ptr<DOM::Document> document = RunTreeConstructorSanity(array);
    EXPECT_EQ(document->doctype, nullptr);
    VerifyAsIfEmpty(document, false);
}

TEST(SimpleTest, EmptyHTML) {
    std::array<std::unique_ptr<HTML::Token>, 3> array {
        std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("html")),
        std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("html")),
        std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE)
    };
    std::shared_ptr<DOM::Document> document = RunTreeConstructorSanity(array);
    EXPECT_EQ(document->doctype, nullptr);
    VerifyAsIfEmpty(document, false);
}

TEST(SimpleTest, EmptyHTMLHeadBody) {
    std::array<std::unique_ptr<HTML::Token>, 7> array {
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("html")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("head")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("head")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("body")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("body")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("html")),
            std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE)
    };
    std::shared_ptr<DOM::Document> document = RunTreeConstructorSanity(array);
    EXPECT_EQ(document->doctype, nullptr);
    VerifyAsIfEmpty(document, false);
}

TEST(SimpleTest, OnlyDoctype) {
    auto doctype = std::make_unique<HTML::DoctypeToken>(false);
    doctype->name = Unicode::UString("html");

    std::array<std::unique_ptr<HTML::Token>, 2> array {
            std::move(doctype),
            std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE)
    };

    std::shared_ptr<DOM::Document> document = RunTreeConstructorSanity(array);
    VerifyAsIfEmpty(document, true, static_cast<const HTML::DoctypeToken *>(array[0].get())->name.value());
}

TEST(SimpleTest, DoctypeHtml) {
    auto doctype = std::make_unique<HTML::DoctypeToken>(false);
    doctype->name = Unicode::UString("html");

    std::array<std::unique_ptr<HTML::Token>, 4> array {
            std::move(doctype),
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("html")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("html")),
            std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE)
    };

    std::shared_ptr<DOM::Document> document = RunTreeConstructorSanity(array);
    VerifyAsIfEmpty(document, true, static_cast<const HTML::DoctypeToken *>(array[0].get())->name.value());
}

TEST(SimpleTest, DoctypeHtmlHeadBody) {
    auto doctype = std::make_unique<HTML::DoctypeToken>(false);
    doctype->name = Unicode::UString("html");

    std::array<std::unique_ptr<HTML::Token>, 8> array {
            std::move(doctype),
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("html")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("head")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("head")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("body")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("body")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("html")),
            std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE)
    };

    std::shared_ptr<DOM::Document> document = RunTreeConstructorSanity(array);
    VerifyAsIfEmpty(document, true, static_cast<const HTML::DoctypeToken *>(array[0].get())->name.value());
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
