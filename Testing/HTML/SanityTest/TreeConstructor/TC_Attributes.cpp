/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <array>
#include <memory>

#include "Testing/PrintDOMNode.hpp"
#include "Testing/TestSymbols.hpp"

#include "Testing/HTML/SanityTest/SanityTreeConstructor.hpp"

void VerifyBasicStructure(const std::shared_ptr<DOM::Document> &document) {
    ASSERT_NE(document->doctype, nullptr);
    EXPECT_EQ(document->doctype->name, Unicode::UString("html"));

    ASSERT_EQ(std::size(document->childNodes), 2);
    EXPECT_EQ(document->childNodes[0]->nodeType, DOM::NodeType::DOCUMENT_TYPE_NODE);
    EXPECT_EQ(document->childNodes[0], document->doctype);

    auto html = std::static_pointer_cast<HTML::HTMLElement>(document->childNodes[1]);

    ASSERT_GE(std::size(html->childNodes), 2);

    EXPECT_EQ(html->commonElementType, DOM::CommonElementType::HTML);

    ASSERT_EQ(html->childNodes[0]->nodeType, DOM::NodeType::ELEMENT_NODE);
    ASSERT_EQ(html->childNodes[1]->nodeType, DOM::NodeType::ELEMENT_NODE);

    auto head = std::static_pointer_cast<HTML::HTMLHeadElement>(html->childNodes[0]);
    auto body = std::static_pointer_cast<HTML::HTMLElement>(html->childNodes[1]);

    EXPECT_EQ(head->commonElementType, DOM::CommonElementType::HEAD);
    EXPECT_EQ(body->commonElementType, DOM::CommonElementType::BODY);
}

[[nodiscard]] inline std::unique_ptr<HTML::DoctypeToken>
CreateDoctypeToken() {
    auto doctype = std::make_unique<HTML::DoctypeToken>(false);
    doctype->name = Unicode::UString("html");
    return doctype;
}

TEST(TC_Attributes, SimpleTest) {
    auto html = std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("html"));
    html->attributes.emplace_back(Unicode::UString("lang"), Unicode::UString("en"));

    std::array<std::unique_ptr<HTML::Token>, 8> array {
            CreateDoctypeToken(),
            std::move(html),
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("head")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("head")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::START_TAG, Unicode::UString("body")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("body")),
            std::make_unique<HTML::TagToken>(HTML::TokenType::END_TAG, Unicode::UString("html")),
            std::make_unique<HTML::Token>(HTML::TokenType::END_OF_FILE)
    };

    std::shared_ptr<DOM::Document> document = RunTreeConstructorSanity(array);
    VerifyBasicStructure(document);

    auto htmlElement = std::static_pointer_cast<HTML::HTMLElement>(document->childNodes[1]);

    ASSERT_EQ(std::size(htmlElement->attributeList), 1);
    auto it = htmlElement->attributeList.find(Unicode::UString("lang"));
    ASSERT_NE(it, std::end(htmlElement->attributeList));
    EXPECT_EQ(it->second, Unicode::UString("en"));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
