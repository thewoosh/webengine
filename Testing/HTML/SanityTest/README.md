# Sanity Tests for HTML
This directory contains shallow tests which only checks predefined behavior to
happen. More sophisticated tests (like fuzzers) should also be used, just not in
this unit.
