/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include <Source/Data/StringStream.hpp>
#include "Testing/TestSymbols.hpp"

#include "Source/Text/Encoding/Type/UTF8.hpp"

std::uniform_int_distribution<Unicode::CodePoint> unicodeDistribution(0x00, Unicode::LAST_ALLOWED_CODE_POINT);

namespace RulesOf {
    template<std::size_t OutputSize>
    void StringLiteralArray(const char literalInput[],
                            const std::array<Unicode::CodePoint, OutputSize> &expectedOutput) {
        using UnitType = std::uint8_t;

        Data::StringStream<UnitType> stream(literalInput);
        Encoding::UTF8<UnitType> encoder;

        auto output = encoder.DecodeStream(&stream);

        ASSERT_EQ(output.size(), expectedOutput.size());
        for (std::size_t i = 0; i < expectedOutput.size(); i++) {
            ASSERT_EQ(output[i], expectedOutput[i])
                << "differ on index " << i;
        }
    }
}

template<std::size_t Size, typename OutputType = Unicode::CodePoint, typename InputType = char>
constexpr std::array<OutputType, Size>
UpgradeArray(const std::array<InputType, Size> &in) noexcept {
    std::array<OutputType, Size> out{};
    for (std::size_t i = 0; i < Size; i++) {
        out[i] = static_cast<OutputType>(in[i]);
    }
    return out;
}

namespace Encoding {

    TEST(UTF_8_Decoder, DecodeASCII) {
        RulesOf::StringLiteralArray("Hello!", UpgradeArray<6>({'H', 'e', 'l', 'l', 'o', '!'}));
    }

    TEST(UTF_8_Decoder, DecodeEmoji) {
        RulesOf::StringLiteralArray<2>("👍❤", {0x1F44D, 0x2764});
    }

} // namespace Encoding

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
