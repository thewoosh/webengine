/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Testing/TestSymbols.hpp"

#include "Source/Text/FastUStrings.hpp"

// Big sample rate since a single sample is quite fast
constexpr std::size_t RANDOM_CHARACTERS_SAMPLES = 10000;

std::uniform_int_distribution<unsigned char> ucharDistribution(
        0x01,
        0x7F
);

std::uniform_int_distribution<unsigned char> ucharDistributionWithNull(
        0x00,
        0x7F
);

void InvokeASCIICaseSensitive(const char *text) {
    Unicode::UString string(text);
    ASSERT_TRUE(Unicode::FastUStrings::EqualsASCII(string, text)) << "String is: '" << text << "'";
    ASSERT_TRUE(Unicode::FastUStrings::EqualsASCIIIgnoreCase(string, text)) << "String is: '" << text << "'";
    ASSERT_TRUE(Unicode::FastUStrings::StartsWithASCIIIgnoreCase(string, text)) << "String is: '" << text << "'";
}

void InvokeInvalidASCIICaseSensitive(const char *text, std::size_t size) {
    Unicode::UString string(std::string_view(text, size));
    ASSERT_FALSE(Unicode::FastUStrings::EqualsASCII(string, text)) << "String is: '" << text << "'";
    ASSERT_FALSE(Unicode::FastUStrings::EqualsASCIIIgnoreCase(string, text)) << "String is: '" << text << "'";
    // starts with semantics not inherited when using invalid ASCII strings.
}

namespace Unicode::FastUStrings {

    TEST(FastUStrings, SimpleTest) {
        ::InvokeASCIICaseSensitive("lowercase");
        ::InvokeASCIICaseSensitive("Hello, world!");
    }

    TEST(FastUStrings, RandomValidTest) {
        for (std::size_t i = 0; i < RANDOM_CHARACTERS_SAMPLES; i++) {
            std::vector<char> characters{};
            characters.reserve(sizeDistribution(randomDevice));

            for (std::size_t j = 0; j < characters.capacity() - 1; j++) {
                characters.push_back(static_cast<char>(ucharDistribution(randomDevice)));
            }

            characters.push_back('\0');

            ::InvokeASCIICaseSensitive(characters.data());
        }
    }

    TEST(FastUStrings, RandomInvalidTest) {
        for (std::size_t i = 0; i < RANDOM_CHARACTERS_SAMPLES; i++) {
            std::vector<char> characters{};
            characters.reserve(sizeDistribution(randomDevice));

            for (std::size_t j = 0; j < characters.capacity() - 1; j++) {
                characters.push_back(static_cast<char>(ucharDistributionWithNull(randomDevice)));
            }

            characters.push_back('\0');

            ::InvokeInvalidASCIICaseSensitive(characters.data(), characters.size());
        }
    }

} // namespace Unicode::FastUStrings

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
