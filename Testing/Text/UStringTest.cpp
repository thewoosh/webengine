/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Testing/TestSymbols.hpp"

#include "Source/Text/UString.hpp"

std::uniform_int_distribution<Unicode::CodePoint> unicodeDistribution(0, Unicode::LAST_ALLOWED_CODE_POINT);
std::uniform_int_distribution<Unicode::CodePoint> charDistribution(
        std::numeric_limits<char>::min(),
        std::numeric_limits<char>::max()
);

namespace Unicode {

    TEST(UStringTest, StringASCIILiteral) {
        const char literal[] = "Hi!";
        Unicode::UString string(literal);
        ASSERT_EQ(string.length(), 3);
        for (std::size_t i = 0; i < string.length(); i++) {
            ASSERT_EQ(string[i], literal[i]);
        }
    }

    TEST(UStringTest, SingleCodePointTest) {
        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            Unicode::CodePoint codePoint = unicodeDistribution(randomDevice);
            Unicode::UString string(codePoint);
            ASSERT_EQ(string.length(), 1);
            ASSERT_EQ(string[0], codePoint);
        }
    }

    TEST(UStringTest, RandomStringTest) {
        for (std::size_t _ = 0; _ < RANDOM_SAMPLES; _++) {
            std::vector<Unicode::CodePoint> codePoints;
            codePoints.reserve(sizeDistribution(randomDevice));
            std::generate(std::begin(codePoints), std::end(codePoints), []() {
                return unicodeDistribution(randomDevice);
            });

            Unicode::UString string(codePoints);
            ASSERT_EQ(string.length(), codePoints.size());

            ASSERT_NE(string, codePoints[0]);

            for (std::size_t i = 0; i < codePoints.size(); i++) {
                ASSERT_EQ(string[i], codePoints[i]);
            }
        }
    }

    TEST(UStringTest, StartsWithTest) {
        for (std::size_t _ = 0; _ < RANDOM_SAMPLES; _++) {
            const std::size_t size = sizeDistribution(randomDevice);
            ASSERT_GT(size, 0);

            std::vector<Unicode::CodePoint> codePoints;
            codePoints.resize(size);
            std::generate(std::begin(codePoints), std::end(codePoints), []() {
                return unicodeDistribution(randomDevice);
            });

            UString string(codePoints);
            ASSERT_EQ(size, string.length());
            ASSERT_TRUE(string.startsWith(string[0]));
            for (std::size_t i = 1; i <= size; i++) {
                ASSERT_TRUE(string.startsWith({std::cbegin(codePoints), std::cbegin(codePoints) + i}));
            }
        }
    }

    TEST(USTringTest, CloneTest) {
        for (std::size_t _ = 0; _ < RANDOM_SAMPLES; _++) {
            std::vector<Unicode::CodePoint> codePoints;
            codePoints.reserve(sizeDistribution(randomDevice));
            std::generate(std::begin(codePoints), std::end(codePoints), []() {
                return unicodeDistribution(randomDevice);
            });

            UString string(codePoints);
            UString duplicate = string.clone();

            ASSERT_EQ(string, duplicate);
            ASSERT_EQ(string.length(), duplicate.length());
            ASSERT_TRUE(string.startsWith(duplicate));
            ASSERT_TRUE(duplicate.startsWith(string));
            ASSERT_TRUE(string.endsWith(duplicate));
            ASSERT_TRUE(duplicate.endsWith(string));
        }
    }

    TEST(UStringTest, EndsWithTest) {
        for (std::size_t _ = 0; _ < RANDOM_SAMPLES; _++) {
            const std::size_t size = sizeDistribution(randomDevice);
            ASSERT_GT(size, 0);

            std::vector<Unicode::CodePoint> codePoints;
            codePoints.resize(size);
            std::generate(std::begin(codePoints), std::end(codePoints), []() {
                return unicodeDistribution(randomDevice);
            });

            UString string(codePoints);
            ASSERT_EQ(size, string.length());
            ASSERT_TRUE(string.endsWith(codePoints.back()));
            for (std::size_t i = string.length() - 1; i > 0; i--) {
                ASSERT_TRUE(string.endsWith({ std::cbegin(codePoints) + i, std::cend(codePoints) }));
            }
        }
    }

    TEST(UStringTest, AppendTest) {
        for (std::size_t _ = 0; _ < RANDOM_SAMPLES; _++) {
            std::size_t size = sizeDistribution(randomDevice);

            std::vector<Unicode::CodePoint> codePoints;
            codePoints.resize(size);
            std::generate(std::begin(codePoints), std::end(codePoints), []() {
                return unicodeDistribution(randomDevice);
            });

            UString string(codePoints);

            // Add code point to the string
            Unicode::CodePoint codePoint = unicodeDistribution(randomDevice);
            string += codePoint;
            ASSERT_EQ(string.length(), size + 1);
            ASSERT_EQ(string[size], codePoint);
            size += 1;

            // Add C character to the string
            char character = charDistribution(randomDevice);
            string += character;
            ASSERT_EQ(string.length(), size + 1);
            ASSERT_EQ(string[size], character);
            size += 1;

            // Add NULL character to the string
            string += Unicode::NULL_CHARACTER;
            ASSERT_EQ(string.length(), size + 1);
            ASSERT_EQ(string[size], Unicode::NULL_CHARACTER);
            size += 1;

        }
    }

} // namespace Unicode

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
