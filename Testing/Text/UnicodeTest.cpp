/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Testing/TestSymbols.hpp"

#include "Source/Text/UnicodeTools.hpp"

std::uniform_int_distribution<Unicode::CodePoint> nonASCIIDistribution(0x100, Unicode::LAST_ALLOWED_CODE_POINT);

namespace Unicode {

    /**
     * Test for the Unicode::IsASCIIAlpha function for all ASCII code points.
     */
    TEST(Unicode, IsASCIIAlpha_ASCII) {
        for (Unicode::CodePoint cp = 0; cp <= 0xFF; cp++) {
            if ((cp >= Unicode::LATIN_CAPITAL_LETTER_A && cp <= Unicode::LATIN_CAPITAL_LETTER_Z) ||
                (cp >= Unicode::LATIN_SMALL_LETTER_A && cp <= Unicode::LATIN_SMALL_LETTER_Z)) {
                ASSERT_TRUE(Unicode::IsASCIIAlpha(cp));
            } else {
                ASSERT_FALSE(Unicode::IsASCIIAlpha(cp));
            }
        }
    }

    /**
     * Test for the Unicode::IsASCIIAlpha function for some non-ASCII code
     * points.
     */
    TEST(Unicode, IsASCIIAlpha_NonASCII) {
        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            ASSERT_FALSE(Unicode::IsASCIIAlpha(nonASCIIDistribution(randomDevice)));
        }
    }

    /**
     * Test for the Unicode:: IsASCIIAlphaNumeric function for all ASCII code\
     * points.
     */
    TEST(Unicode, IsASCIIAlphaNumeric_ASCII) {
        for (Unicode::CodePoint cp = 0; cp <= 0xFF; cp++) {
            if ((cp >= Unicode::LATIN_CAPITAL_LETTER_A && cp <= Unicode::LATIN_CAPITAL_LETTER_Z) ||
                (cp >= Unicode::LATIN_SMALL_LETTER_A && cp <= Unicode::LATIN_SMALL_LETTER_Z) ||
                (cp >= Unicode::DIGIT_ZERO && cp <= Unicode::DIGIT_NINE)) {
                ASSERT_TRUE(Unicode::IsASCIIAlphaNumeric(cp));
            } else {
                ASSERT_FALSE(Unicode::IsASCIIAlphaNumeric(cp));
            }
        }
    }

    /**
     * Test for the Unicode::IsASCIIAlphaNumeric function for some non-ASCII
     * code points.
     */
    TEST(Unicode, IsASCIIAlphaNumeric_NonASCII) {
        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            ASSERT_FALSE(Unicode::IsASCIIAlphaNumeric(nonASCIIDistribution(randomDevice)));
        }
    }

    /**
     * Test for the Unicode::IsDigit function for all ASCII code points.
     */
    TEST(Unicode, IsDigit_ASCII) {
        for (Unicode::CodePoint cp = 0; cp <= 0xFF; cp++) {
            if (cp >= Unicode::DIGIT_ZERO && cp <= Unicode::DIGIT_NINE) {
                ASSERT_TRUE(Unicode::IsDigit(cp));
            } else {
                ASSERT_FALSE(Unicode::IsDigit(cp));
            }
        }
    }

    /**
     * Test for the Unicode::IsDigit function for some non-ASCII code points.
     */
    TEST(Unicode, IsDigit_NonASCII) {
        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            ASSERT_FALSE(Unicode::IsDigit(nonASCIIDistribution(randomDevice)));
        }
    }

    /**
     * Test for the Unicode::IsSurrogate for code inside the surrogate bounds.
     */
    TEST(Unicode, IsSurrogate) {
        std::uniform_int_distribution<int> dist(0xD800, 0xDFFF);
        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            ASSERT_TRUE(Unicode::IsSurrogate(dist(randomDevice)));
        }
    }

    /**
     * Test for the Unicode::IsSurrogate for code points lower than the
     * surrogate lower-bounds.
     */
    TEST(Unicode, IsSurrogate_Lower) {
        std::uniform_int_distribution<int> dist(0, 0xD7FF);
        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            ASSERT_FALSE(Unicode::IsSurrogate(dist(randomDevice)));
        }
    }

    /**
     * Test for the Unicode::IsSurrogate for code points higher than the
     * surrogate upper-bounds.
     */
    TEST(Unicode, IsSurrogate_Upper) {
        std::uniform_int_distribution<int> dist(0xE000, Unicode::LAST_ALLOWED_CODE_POINT);
        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            ASSERT_FALSE(Unicode::IsSurrogate(dist(randomDevice)));
        }
    }

    /**
     * Test for the Unicode::ToLowerASCII with some predefined values.
     */
    TEST(Unicode, ToLowerASCII_Sanity) {
        std::array upperCased = {'A', 'Q', 'T', 'Z'};
        std::array lowerCased = {'a', 'q', 't', 'z'};
        static_assert(upperCased.size() == lowerCased.size());

        for (std::size_t i = 0; i < upperCased.size(); i++) {
            ASSERT_EQ(Unicode::ToLowerASCII(upperCased.at(i)), lowerCased.at(i));
        }
    }

    /**
     * Test for the Unicode::ToLowerASCII for all ASCII code points.
     */
    TEST(Unicode, ToLowerASCII_ASCII) {
        for (Unicode::CodePoint cp = 0; cp <= 0xFF; cp++) {
            if ((cp >= Unicode::LATIN_CAPITAL_LETTER_A && cp <= Unicode::LATIN_CAPITAL_LETTER_Z)) {
                ASSERT_EQ(Unicode::ToLowerASCII(cp), cp + 0x20)
                    << "Expected " << static_cast<char>(cp)
                    << " to return " << static_cast<char>(cp + 6)
                    << " but was " << static_cast<char>(Unicode::ToLowerASCII(cp));
            } else {
                ASSERT_EQ(Unicode::ToLowerASCII(cp), cp);
            }
        }
    }

    /**
     * Test for the Unicode::ToLowerASCII for all ASCII code points.
     */
    TEST(Unicode, ToLowerASCII_NonASCII) {
        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            Unicode::CodePoint codePoint = nonASCIIDistribution(randomDevice);
            ASSERT_EQ(Unicode::ToLowerASCII(codePoint), codePoint);
        }
    }

    /**
     * Test for the Unicode::IsC0Control
     */
    TEST(Unicode, IsC0Control) {
        for (Unicode::CodePoint codePoint = 0; codePoint < 0x100; codePoint++) {
            if (codePoint > Unicode::INFORMATION_SEPARATOR_ONE)
                ASSERT_FALSE(Unicode::IsC0Control(codePoint));
            else
                ASSERT_TRUE(Unicode::IsC0Control(codePoint));
        }

        for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
            Unicode::CodePoint codePoint = nonASCIIDistribution(randomDevice);
            ASSERT_FALSE(Unicode::IsC0Control(codePoint));
        }
    }


} // namespace Unicode

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
