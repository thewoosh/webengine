/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "gtest/gtest.h"

#include <random>

static std::random_device randomDevice;

std::uniform_int_distribution<std::size_t> sizeDistribution(2, 20);

/**
 * Defines how many samples we should take when using random input values.
 */
static constexpr std::size_t RANDOM_SAMPLES = 16;
