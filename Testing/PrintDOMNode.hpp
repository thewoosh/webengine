/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <iostream>
#include <memory>

#include "Source/DOM/Helpers/Strings.hpp"
#include "Source/DOM/CharacterData.hpp"
#include "Source/DOM/DocumentType.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/DOM/Node.hpp"

#include "Source/Text/Unicode.hpp"

namespace Testing {

    inline void
    PrintDOMNode(const std::shared_ptr<DOM::Node> &node, std::size_t depth) {
        const std::string spacing(depth * 4, ' ');

        std::cout << spacing << node->nodeType << ' ';

        switch (node->nodeType) {
            case DOM::NodeType::DOCUMENT_TYPE_NODE:
                std::cout << "name=" << std::static_pointer_cast<DOM::DocumentType>(node)->name;
                break;
            case DOM::NodeType::COMMENT_NODE:
            case DOM::NodeType::TEXT_NODE: {
                bool justWhitespace = true;
                const auto &data = std::static_pointer_cast<DOM::CharacterData>(node)->data;
                for (Unicode::CodePoint character : data) {
                    if (character != ' ' &&
                        character != '\t' &&
                        character != '\n' &&
                        character != '\r') {
                        justWhitespace = false;
                        break;
                    }
                }

                if (justWhitespace) {
                    std::cout << "as whitespace";
                } else {
                    std::cout << "with character data: " << std::static_pointer_cast<DOM::CharacterData>(node)->data;
                }
            } break;
            case DOM::NodeType::ELEMENT_NODE: {
                const auto &element = std::static_pointer_cast<DOM::Element>(node);
                std::cout << element->localName << " in " << element->namespaceValue;
            } break;
            default:
                break;
        }

        std::cout << '\n';

        // Print children
        for (const auto &childNode : node->childNodes) {
            PrintDOMNode(childNode, depth + 1);
        }
    }

} // namespace Testing
