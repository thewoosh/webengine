/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Testing/TestSymbols.hpp"

#include <string>

#include "Source/Data/StringStream.hpp"
#include "Source/Text/Unicode.hpp"

std::uniform_int_distribution<Unicode::CodePoint> asciiDistribution(0x00, 0x7F);

namespace RulesOf {
    template<typename Unit = std::uint8_t>
    void EmptyStream(Data::StringStream<Unit> &stream) {
        ASSERT_EQ(stream.Peek(), 0);
        ASSERT_EQ(stream.Read(), 0);
        ASSERT_TRUE(stream.IsEOF());
        ASSERT_FALSE(stream.IsReady());
    }
}

TEST(StringStream, EmptyStringLiteral) {
    Data::StringStream stream("");
    RulesOf::EmptyStream(stream);
}

TEST(StringStream, EmptyStdString) {
    std::string string;
    Data::StringStream stream(string);
    RulesOf::EmptyStream(stream);
}

TEST(StringStream, SanityStringLiteral) {
    Data::StringStream stream("Test");
    ASSERT_TRUE(stream.IsReady());
    ASSERT_FALSE(stream.IsEOF());
    ASSERT_EQ(stream.Read(), 'T');
    ASSERT_EQ(stream.Read(), 'e');
    ASSERT_EQ(stream.Read(), 's');
    ASSERT_EQ(stream.Read(), 't');
    ASSERT_TRUE(stream.IsEOF());
    ASSERT_FALSE(stream.IsReady());
}

TEST(StringStream, StringLiteral) {
    const char literal[] = "Hello, world!";

    Data::StringStream stream(literal);
    for (std::size_t i = 0; i < sizeof(literal) / sizeof(literal[0]) - 1; i++) {
        ASSERT_TRUE(stream.IsReady());
        ASSERT_FALSE(stream.IsEOF());
        ASSERT_EQ(stream.Peek(), literal[i]);
        ASSERT_EQ(stream.Read(), literal[i]);
    }

    RulesOf::EmptyStream(stream);
}

TEST(StringStream, StdString) {
    std::string string(sizeDistribution(randomDevice), 'A');
    std::generate(std::begin(string), std::end(string), []() {
        return asciiDistribution(randomDevice);
    });

    Data::StringStream stream(string);
    for (const auto &character : string) {
        ASSERT_TRUE(stream.IsReady());
        ASSERT_FALSE(stream.IsEOF());
        ASSERT_EQ(stream.Peek(), character);
        ASSERT_EQ(stream.Read(), character);
    }

    RulesOf::EmptyStream(stream);
}

TEST(StringStream, ReconsumeLiteralSanity) {
    const char literal[] = "123";
    Data::StringStream stream(literal);
    ASSERT_EQ(stream.Read(), '1');
    ASSERT_EQ(stream.Peek(), '2');
    stream.Reconsume();
    ASSERT_EQ(stream.Read(), '1');
    ASSERT_EQ(stream.Read(), '2');
    stream.Reconsume();
    ASSERT_EQ(stream.Read(), '2');
    ASSERT_EQ(stream.Read(), '3');
    stream.Reconsume();
    ASSERT_TRUE(stream.IsEOF()); // we can't reconsume at EOF
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
