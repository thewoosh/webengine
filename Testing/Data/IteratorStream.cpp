/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2020 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#include "Testing/TestSymbols.hpp"

#include "Source/Data/IteratorStream.hpp"
#include "Source/Text/Unicode.hpp"
#include "Source/Text/UnicodeCommons.hpp"

std::uniform_int_distribution<Unicode::CodePoint> codePointDistribution(0x100, Unicode::LAST_ALLOWED_CODE_POINT);

TEST(IteratorStream, EmptyStream) {
    std::array<Unicode::CodePoint, 0> emptyArray{};
    Data::IteratorStream<Unicode::CodePoint, decltype(emptyArray.cbegin())> iteratorStream{
        std::cbegin(emptyArray), std::cend(emptyArray)
    };

    ASSERT_TRUE(iteratorStream.IsEOF());
    ASSERT_FALSE(iteratorStream.IsReady());
    ASSERT_EQ(iteratorStream.Peek(), 0);
    ASSERT_EQ(iteratorStream.Read(), 0);
}

TEST(IteratorStream, SingleUnitStream) {
    const Unicode::CodePoint codePoint = codePointDistribution(randomDevice);

    std::array<Unicode::CodePoint, 1> singleUnitStream{codePoint};
    Data::IteratorStream<Unicode::CodePoint, decltype(singleUnitStream.cbegin())> iteratorStream{
            std::cbegin(singleUnitStream), std::cend(singleUnitStream)
    };

    ASSERT_FALSE(iteratorStream.IsEOF());
    ASSERT_TRUE(iteratorStream.IsReady());
    ASSERT_EQ(iteratorStream.Peek(), codePoint);
    ASSERT_EQ(iteratorStream.Read(), codePoint);
    ASSERT_TRUE(iteratorStream.IsEOF());
    ASSERT_FALSE(iteratorStream.IsReady());
    ASSERT_EQ(iteratorStream.Peek(), 0);
    ASSERT_EQ(iteratorStream.Read(), 0);
}

TEST(IteratorStream, GeneralStream) {
    std::vector<Unicode::CodePoint> vec;

    for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
        // Generate a random vector of code points
        vec.resize(sizeDistribution(randomDevice));

        std::generate(std::begin(vec), std::end(vec), []() -> auto {
            return codePointDistribution(randomDevice);
        });

        Data::IteratorStream<Unicode::CodePoint, decltype(vec.cbegin())> iteratorStream{
                std::cbegin(vec), std::cend(vec)
        };

        for (const auto &codePoint : vec) {
            ASSERT_FALSE(iteratorStream.IsEOF());
            ASSERT_TRUE(iteratorStream.IsReady());

            ASSERT_EQ(iteratorStream.Peek(), codePoint);
            ASSERT_EQ(iteratorStream.Read(), codePoint);
        }

        ASSERT_TRUE(iteratorStream.IsEOF());
        ASSERT_FALSE(iteratorStream.IsReady());
        ASSERT_EQ(iteratorStream.Peek(), 0);
        ASSERT_EQ(iteratorStream.Read(), 0);
    }
}

TEST(IteratorStream, PopulateTest) {
    std::vector<Unicode::CodePoint> vec;

    for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
        // Generate a random vector of code points
        vec.resize(sizeDistribution(randomDevice));

        std::generate(std::begin(vec), std::end(vec), []() -> auto {
            return codePointDistribution(randomDevice);
        });

        Data::IteratorStream<Unicode::CodePoint, decltype(vec.cbegin())> iteratorStream{
                std::cbegin(vec), std::cend(vec)
        };

        // Successful population
        std::uniform_int_distribution<std::size_t> distribution(0, vec.size());
        std::vector<Unicode::CodePoint> partialVector;
        partialVector.resize(distribution(randomDevice));
        std::cout << "sample is " << partialVector.size() << " of " << vec.size() << "\n";

        ASSERT_TRUE(iteratorStream.Populate(std::begin(partialVector), std::end(partialVector)));
        for (std::size_t k = 0; k < partialVector.size(); k++) {
            ASSERT_EQ(partialVector[k], vec[k])
                << "contents of populated vector isn't equal to the section"
                   " of the IteratorStreams underlying vector: index=" << k;
        }

    }
}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
